﻿using System;
using System.IO;
using Azure;
using Ennova.CategorizationService.Queue;
using Ennova.SentimentService.Service.Services;
using Microsoft.Extensions.Azure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Ennova.SentimentService.Functions
{
    public sealed class SentimentConfigureServices
    {
        private static readonly IServiceProvider _serviceProvider = Build();
        public static IServiceProvider Instance => _serviceProvider;

        static SentimentConfigureServices()
        {

        }

        private SentimentConfigureServices()
        {

        }

        private static IServiceProvider Build()
        {
            var services = new ServiceCollection();
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("local.settings.json", optional: true, reloadOnChange: true)
                .Build();
            services.AddAzureClients(builder =>
                builder.AddBlobServiceClient(configuration.GetSection("AzureWebJobsStorage")["ConnectionString"]));
            
            services.AddAzureClients(builder =>
                    builder.AddTextAnalyticsClient(new Uri(configuration.GetSection("AzureTextAnalytics")["Endpoint"]), new AzureKeyCredential(configuration.GetSection("AzureTextAnalytics")["Key"]))
                );
            
            services.AddTextQueue(configuration.GetSection("TextQueue")["ConnectionString"]);
            services.AddSentimentQueue(configuration.GetSection("SentimentQueue")["ConnectionString"]);

            services.AddTransient<ISentimentAnalysisService, SentimentAnalysisService>();

            return services.BuildServiceProvider();
        }
    }
}