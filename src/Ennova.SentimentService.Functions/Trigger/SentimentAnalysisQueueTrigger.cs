﻿using System;
using System.Threading.Tasks;
using Ennova.CategorizationService.Queue;
using Ennova.CategorizationService.Queue.Config;
using Ennova.CategorizationService.Queue.Message;
using Ennova.Comment.Dto;
using Ennova.Comment.Model;
using Ennova.SentimentService.Service.Services;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;

namespace Ennova.SentimentService.Functions.Trigger
{
    public class SentimentAnalysisQueueTrigger
    {
        [FunctionName("SentimentAnalysisQueueTrigger")]
        public static async Task Run([QueueTrigger(RouteConfig.RouteTextQueueName, Connection = "AzureWebJobsStorage")] string commentQueueItem, ILogger log)
        {
            try
            {
                var queueCommunicatorText = SentimentConfigureServices.Instance.GetService<IQueueCommunicator>();
                var queueCommunicatorSentiment = SentimentConfigureServices.Instance.GetService<IQueueCommunicator>();
                var textMessage = queueCommunicatorText.Read<CommentMessageCommand>(commentQueueItem);

                var service = SentimentConfigureServices.Instance.GetService<ISentimentAnalysisService>();
                var simpleComment = new SimpleComment {Id = textMessage.Id, Texts = textMessage.Comment};
                var comment =  await service.AnalyzeSentiment(new SimpleCommentMessageDto() {SimpleComment = simpleComment,ServiceBusReceivedMessageId = Guid.NewGuid().ToString()});

                await queueCommunicatorSentiment.SendAsync(new SentimentMessageCommand{ Comment = comment.Comment});
            }
            catch(Exception ex)
            {
                var error = ex;
                throw;
            }


        }
    }
}