﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Ennova.Comment.Dto;
using Ennova.Comment.Model;

namespace Ennova.SentimentService.Service.Services
{
    public interface ISentimentAnalysisService
    {
        Task<CommentMessageDto> AnalyzeSentiment(SimpleCommentMessageDto messageDto);
        Task<List<CommentMessageDto>> AnalyzeSentimentBatch(List<SimpleCommentMessageDto> listOfSimpleComment);
    }
}