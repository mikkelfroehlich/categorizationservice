﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Azure;
using Azure.AI.TextAnalytics;
using Ennova.CategorizationService.Service.Services;
using Ennova.Comment.Dto;
using Ennova.Comment.Model;

namespace Ennova.SentimentService.Service.Services
{
    public class SentimentAnalysisService : ISentimentAnalysisService
    {
        private readonly TextAnalyticsClient _textAnalyticsClient;
        private readonly ITableService _tableService;

        public SentimentAnalysisService(TextAnalyticsClient textAnalyticsClient, ITableService tableService)
        {
            _textAnalyticsClient = textAnalyticsClient;
            _tableService = tableService;
        }

        public async Task<CommentMessageDto> AnalyzeSentiment(SimpleCommentMessageDto messageDto)
        {
            int maxRuns = 10;
            int count = 0;
            DocumentSentiment documentSentiment = null;
            
            while (documentSentiment == null)
            {
                count++;
                documentSentiment = _textAnalyticsClient.AnalyzeSentiment(messageDto.SimpleComment.Texts);
                if (maxRuns == count)
                {
                    break;
                }
            }

            if (documentSentiment != null)
            {
                //Log that it cant contact service
                CommentMessageDto commentDto = MapComment(documentSentiment, messageDto);

                return commentDto;
            }

            //try
            //{
            //    documentSentiment = _textAnalyticsClient.AnalyzeSentiment(messageDto.SimpleComment.Texts);
            //}
            //catch (Exception e)
            //{
            //    Console.WriteLine("error: " + e);
            //    throw;
            //}

            //if (documentSentiment == null)
            //{
            //    documentSentiment = _textAnalyticsClient.AnalyzeSentiment(messageDto.SimpleComment.Texts);
            //}

            return null;
        }

        public async Task <List<CommentMessageDto>> AnalyzeSentimentBatch(List<SimpleCommentMessageDto> listOfSimpleComment)
        {
            DocumentSentiment documentSentiment = null;
            List<string> textList = new List<string>();
            Response<AnalyzeSentimentResultCollection> response = null;
            foreach (SimpleCommentMessageDto simpleComment in listOfSimpleComment)
            {
                textList.Add(simpleComment.SimpleComment.Texts);
            }

            try
            {
               
            }
            catch (Exception e)
            {
                Console.WriteLine("error: " + e);
                throw;
            }

            if (documentSentiment == null)
            {
                bool hasNotExecuted = true;
                int count = 0;
                while (hasNotExecuted)
                {
                    count++;
                    if (CanDoRequestAnalycisService())
                    {
                        response = await _textAnalyticsClient.AnalyzeSentimentBatchAsync(textList);
                        hasNotExecuted = false;
                        break;
                    }

                    if (count == 30)
                    {
                        break;
                        //log som error that it cant execute analysic
                    }

                    Thread.Sleep(1000);
                }
            }

            List<CommentMessageDto> list = AlignIdWithResultList(response.Value , listOfSimpleComment);

            return list;
        }

        private List<CommentMessageDto> AlignIdWithResultList(AnalyzeSentimentResultCollection responseValue, List<SimpleCommentMessageDto> listOfSimpleComment)
        {
            List<CommentMessageDto> list = new List<CommentMessageDto>();
            int i = 0;
            foreach (AnalyzeSentimentResult result in responseValue)
            {
                CommentMessageDto commentDto = MapComment(result.DocumentSentiment, listOfSimpleComment[i]);
                list.Add(commentDto);
                i++;
            }

            return list;
        }

        private CommentMessageDto MapComment(DocumentSentiment document, SimpleCommentMessageDto messageDto)
        {
            CommentMessageDto dto = new CommentMessageDto();

            Comment.Model.Comment comment = new Comment.Model.Comment();
            comment.Id = messageDto.SimpleComment.Id;
            comment.Texts = messageDto.SimpleComment.Texts;
            foreach (var sentence in document.Sentences)
            {
                var sentenceItem = new SentenceText()
                {
                    Text = sentence.Text,
                    Score = new SentenceScore()
                    {
                        OverAllSentiment = sentence.Sentiment.ToString(),
                        PositiveScore = sentence.ConfidenceScores.Positive,
                        NeutralScore = sentence.ConfidenceScores.Neutral,
                        NegativeScore = sentence.ConfidenceScores.Negative
                    }
                };
                comment.Sentence.Add(sentenceItem);
            }

            dto.Comment = comment;
            dto.ServiceBusReceivedMessageId = messageDto.ServiceBusReceivedMessageId;
            return dto;
        }

        private bool CanDoRequestAnalycisService()
        {
            bool canExecute = false;

            int requestAmount = _tableService.GetAmountOfRequestToTextAnalyseService();

            if (requestAmount < 300)
            {
                canExecute =  true;
                _tableService.AddRequestThatTextAnalyseServiceHasBeenUsed();
            }

            return canExecute;
        }
        

        //Rate limits
        //Tier	                Requests per second	    Requests per minute
        //  S / Multi-service	1000	                1000
        //  S0 / F0	            100	                    300
        //  S1	                200	                    300
        //  S2	                300	                    300
        //  S3	                500	                    500
        //  S4	                1000	                1000


        //  The following limits are for the current v3 API.Exceeding the limits below will generate an HTTP 400 error code.
        //  Feature Max Documents Per Request
        //  Language Detection          1000
        //  Sentiment Analysis          10
        //  Opinion Mining              10
        //  Key Phrase Extraction       10
        //  Named Entity Recognition    5
        //  Entity Linking	            5
        //  Text Analytics for health   10 for the web-based API, 1000 for the container.
        //  Analyze endpoint	        25 for all operations.
    }
}