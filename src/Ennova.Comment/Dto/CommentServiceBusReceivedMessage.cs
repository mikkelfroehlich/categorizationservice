﻿using System;
using Ennova.Comment.Model;

namespace Ennova.Comment.Dto
{
    public class CommentMessageDto
    {
        public CommentMessageDto()
        {
            Comment = new Model.Comment();
        }
        public string ServiceBusReceivedMessageId { get; set; }
        public Model.Comment Comment { get; set; }

        public Guid GetIdAsGuid => new Guid(ServiceBusReceivedMessageId);
    }


    public class SimpleCommentMessageDto
    {
        public SimpleCommentMessageDto()
        {
            SimpleComment = new SimpleComment();
        }
        public string ServiceBusReceivedMessageId { get; set; }
        public SimpleComment SimpleComment { get; set; }

        public Guid GetIdAsGuid => new Guid(ServiceBusReceivedMessageId);
    }
}