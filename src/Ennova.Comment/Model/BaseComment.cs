﻿using System;

namespace Ennova.Comment.Model
{
    public class BaseComment
    {
        public Guid Id { get; set; }
        public string Texts { get; set; }
    }
}