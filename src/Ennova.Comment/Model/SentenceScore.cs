﻿namespace Ennova.Comment.Model

{
    public class SentenceScore
    {
        public string OverAllSentiment { get; set; }
        public double PositiveScore { get; set; }
        public double NeutralScore { get; set; }
        public double NegativeScore { get; set; }
    }
}