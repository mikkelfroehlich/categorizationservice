﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Ennova.Comment.Model
{
    public class SentenceText
    {
        public SentenceText()
        {
            Categories = new List<string>();
        }
        public string Text { get; set; }
        public SentenceScore Score { get; set; }
        public List<string> Categories { get; set; }

        public bool IsPhraseOfWords => HasTextSpacesInBetweenWords();
        public string ClearText => CleanTextForSpecialCharacters();



        private bool HasTextSpacesInBetweenWords()
        {
            if (string.IsNullOrEmpty(Text))
            {
                return false;
            }

            if (string.IsNullOrWhiteSpace(Text))
            {
                return false;
            }

            if (Text.Trim().Contains(" "))
            {
                return true;
            }

            return false;
        }

        private string CleanTextForSpecialCharacters()
        {
            string noneWantedChars = Regex.Replace(Text, @"[a-zA-Z\s']", " ");
            noneWantedChars = noneWantedChars.Replace(" ", "");

            List<string> listOfNoneWantedChars = new List<string>();
            for (int i = 0; i < noneWantedChars.Length; i++)
            {
                listOfNoneWantedChars.Add(noneWantedChars.Substring(i, 1));
            }

            listOfNoneWantedChars = listOfNoneWantedChars.Distinct().ToList();

            string comment = Text;

            foreach (string text in listOfNoneWantedChars)
            {
                comment = comment.Replace(text, " ");
            }

            return comment;
        }
    }
}