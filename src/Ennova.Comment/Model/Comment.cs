﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Ennova.Comment.Model
{
    public class Comment : BaseComment
    {

        public Comment()
        {
            Sentence = new List<SentenceText>();
        }

        //public Guid Id { get; set; }
        //public string Texts { get; set; }
        public List<SentenceText> Sentence { get; set; }

        public bool IsPhraseOfWords => HasTextSpacesInBetweenWords();
        public string ClearText => CleanTextForSpecialCharacters();
        public bool IsNotEmptyNullOrWhiteSpaces => IsNotEmpty();

        private bool IsNotEmpty()
        {
            if (!string.IsNullOrWhiteSpace(Texts) && !string.IsNullOrEmpty(Texts))
            {
                return true;
            }

            return false;
        }


        private bool HasTextSpacesInBetweenWords()
        {
            if (string.IsNullOrEmpty(Texts))
            {
                return false;
            }

            if (string.IsNullOrWhiteSpace(Texts))
            {
                return false;
            }

            if (Texts.Trim().Contains(" "))
            {
                return true;
            }

            return false;
        }

        private string CleanTextForSpecialCharacters()
        {
            string noneWantedChars = Regex.Replace(Texts, @"[a-zA-Z\s']", " ");
            noneWantedChars = noneWantedChars.Replace(" ", "");

            List<string> listOfNoneWantedChars = new List<string>();
            for (int i = 0; i < noneWantedChars.Length; i++)
            {
                listOfNoneWantedChars.Add(noneWantedChars.Substring(i, 1));
            }

            listOfNoneWantedChars = listOfNoneWantedChars.Distinct().ToList();

            string comment = Texts;

            foreach (string text in listOfNoneWantedChars)
            {
                comment = comment.Replace(text, " ");
            }

            return comment;
        }

    }
}