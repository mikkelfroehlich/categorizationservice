﻿using System;
using System.IO;
using Ennova.CategorizationService.Functions.Categorization;
using Ennova.CategorizationService.Queue;
using Ennova.CategorizationService.Queue.Factory;
using Ennova.CategorizationService.Queue.Message.Serializer;
using Ennova.CategorizationService.Service.Services;
using Microsoft.Extensions.Azure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Azure.Data.Tables;

namespace Ennova.CategorizationService.Functions
{
    public sealed class CategorizationServiceContainer
    {
        private static readonly IServiceProvider _serviceProvider = Build();
        public static IServiceProvider Instance => _serviceProvider;

        static CategorizationServiceContainer()
        {

        }

        private CategorizationServiceContainer()
        {

        }

        private static IServiceProvider Build()
        {
            var services = new ServiceCollection();

            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("local.settings.json", optional: true, reloadOnChange: true)
                .Build();
            services.AddSentimentQueue(configuration.GetSection("SentimentQueue")["ConnectionString"]);
            services.AddAzureClients(builder => 
                builder.AddBlobServiceClient(configuration.GetSection("AzureWebJobsStorage")["ConnectionString"])
            );
           
            services.AddSingleton<ICategorizationHandler, CategorizationHandler>();
            services.AddSingleton<ICommentsService,CommentsService> ();
            services.AddTransient<IQueueCommunicator, QueueCommunicator>();
            services.AddTransient<IQueueMessageSerializer, JsonSerializer> ();
            services.AddSingleton<IQueueClientFactory, QueueClientFactory> ();
            services.AddSingleton<ICategorySyntax, CategorySyntax>();
            services.AddSingleton<TableClient>(new TableClient(configuration.GetSection("AzureTablesService")["ConnectionString"], configuration.GetSection("AzureTablesService")["TablesName"]));
            services.AddSingleton<ITableService,TableService>();

            return services.BuildServiceProvider();
        }
    }
}