﻿using System.Threading.Tasks;
using Ennova.CategorizationService.Queue.Message;

namespace Ennova.CategorizationService.Functions.Categorization
{
    public interface ICategorizationHandler
    {
        Task FindTheCategories(SentimentMessageCommand comment);
    }
}