﻿using System.Threading.Tasks;
using Ennova.CategorizationService.Queue.Message;
using Ennova.CategorizationService.Service.Services;

namespace Ennova.CategorizationService.Functions.Categorization
{
    public class CategorizationHandler : ICategorizationHandler
    {
        private readonly ICommentsService _commentsService;
        private readonly ITableService _tableService;

        public CategorizationHandler(ICommentsService commentsService,  ITableService tableService)
        {
            _commentsService = commentsService;
            _tableService = tableService;
        }

        public async Task FindTheCategories(SentimentMessageCommand commentMessage)
        {
            Comment.Model.Comment comment = commentMessage.Comment;

            if (comment.IsNotEmptyNullOrWhiteSpaces)
            {
                comment = _commentsService.Categorize(comment);
                _tableService.Insert(comment);
            }
        }
    }
}