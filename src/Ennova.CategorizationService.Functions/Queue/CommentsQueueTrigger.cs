﻿using System;
using System.Threading.Tasks;
using Ennova.CategorizationService.Functions.Categorization;
using Ennova.CategorizationService.Queue;
using Ennova.CategorizationService.Queue.Config;
using Ennova.CategorizationService.Queue.Message;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Ennova.CategorizationService.Functions.Queue
{
    public class CommentsQueueTrigger
    {
        [FunctionName("CommentsQueueTrigger")]
        public static async Task Run([QueueTrigger(RouteConfig.RouteSentimentQueueName, Connection = "AzureWebJobsStorage")] string commentQueueItem, ILogger log)
        {
            try
            {
                var queueCommunicatorComment = CategorizationServiceContainer.Instance.GetService<IQueueCommunicator>();
                var commentMessage = queueCommunicatorComment.Read<SentimentMessageCommand>(commentQueueItem); 
                
                var handler = CategorizationServiceContainer.Instance.GetService<ICategorizationHandler>();
                await handler.FindTheCategories(commentMessage);
                log.LogInformation("comment ID :" + commentMessage.Comment.Id +" is completed:");
            }
            catch (Exception ex) 
            {
                log.LogError($"Error in CommentsQueueTrigger {commentQueueItem} Error:" + ex); 
                throw;
            }
        }
    }
}