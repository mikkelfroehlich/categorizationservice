﻿using Ennova.CategorizationService.Service.Model;

namespace Ennova.CategorizationService.Service.Data.Categories
{
    public class Remuneration
    {
        public Category GetCategory()
        {
            var cat = new Category();
            cat.Name = "Remuneration";
            cat.Id = "c119541";
            cat.MinimumMatches = 2;

			var batch = new SearchBatch();

            string[] wordWithInWordsZero =
            {
                "compensate", "compensated", "compensations", "compensates", "income", "wage", "wages", "payment",
                "more money", "remuneration", "salary", "salaries", "hourly rate", "bonus*", "overtime", "earning",
                "earnings", "commissions", "commission", "my discount", "social activities", "recovery days",
                "recovery day", "welfare", "well being", "empath*", "sympath*", "compassion*", "impersonal",
                "disinterest*", "attentive", "comforting", "compassion", "compassionate", "look after", "paycheck",
                "paychecks", "laid off", "lay off", "lay offs", "layoffs", "layoff", "secure future", "unemplo*",
                "out of work", "downsize", "downsizing", "changed", "reorganis*", "reorganiz*", "restruc*",
                "uncertainty", "certainty", "retain employees", "retaining employees", "job safety",
                "process ambiguity", "benefits", "benifits", "insurance", "healthcare", "retirement", "pension",
                "vacation", "vacations", "flextime", "personal leave days", "personal time off", "pto", "paid time off",
                "personal days", "tuition reimbursement", "dental", "medical", "maternity", "paternity", "day off",
                "days off", "time off", "car", "sports", "loans", "allowance*"
            };
            string[] wordWithInWordsTwo =
            {
                "compensation", "little money", "care employee", "cares employee", "care employees", "cares employees",
                "show concern", "shows concern", "show concerns", "shows concerns", "well health", "interest employees",
                "interest employee", "interest staff", "my health", "physical health", "reliability job",
                "reliable job", "stable job", "stability job", "dependable job", "retain job", "retain jobs",
                "still have job", "jobs created", "lose job", "losing job", "replace employees", "replaced employees"
            };
            string[] wordWithInWordsThree =
            {
                "employee discount", "care workforce", "cares workforce", "care individual", "cares individual",
                "care individuals", "cares individuals", "care staff", "cares staff", "care people", "cares people",
                "care me", "cares me", "care us", "cares us", "concern us", "concerns us", "concerned us", "concern me",
                "concerns me", "concerned me", "concern staff", "concerns staff", "concerned staff", "concern employee",
                "concerns employee", "concerned employee", "concern employees", "concerns employees",
                "concerned employees", "consideration staff", "considerations staff", "consideration employee",
                "considerations employee", "consideration employees", "considerations employees", "consideration team",
                "considerations team", "regard for staff", "regard for employee", "regard for employees",
                "employee health", "employees health", "health safety", "pays more", "pays less", "pays small",
                "pays fair", "pays fairly", "pays employees", "pays employee", "security job", "security employment",
                "security role", "security future", "occupational security", "secure job", "secure jobs",
                "secure career", "reliable employer", "stable work", "unstable work", "job safe", "job secured",
                "company stability", "stable company", "stop changing", "stop altering", "moving around", "move around",
                "moves around", "moved around", "stabiliy orders","stable work", "unstable work"
			};
            string[] wordWithInWordsFive =
            {
                "security employees", "Secure employment", "reliable employment", "stability work", "less change",
                "less changing", "changing work", "changing process", "changing processes", "stable order",
                "stability job", "stable job", "stability work", "stability job",
            };

			batch.AddRangeSearchWords(wordWithInWordsZero,0);
			batch.AddRangeSearchWords(wordWithInWordsTwo,2);
			batch.AddRangeSearchWords(wordWithInWordsThree,3);
			batch.AddRangeSearchWords(wordWithInWordsFive,5);
			

			var orBatch = new SearchBatch();
            orBatch.AddSearchWord("Christmas",0 );
            var notInBatch = new SearchBatch();

            string[] notInWordWithInWordsZero =
            {
                "christmas period","christmas season","during christmas","for christmas",
			};

			notInBatch.AddRangeSearchWords(notInWordWithInWordsZero,0);
            orBatch.NotInWords.Add(notInBatch);
            batch.OrSubSearchBatch.Add(orBatch);

                
            orBatch = new SearchBatch();
    		orBatch.AddSearchWord("trip", 0);
            notInBatch = new SearchBatch();
    		notInBatch.AddSearchWord("business trip", 0 );
            orBatch.NotInWords.Add(notInBatch);
            batch.OrSubSearchBatch.Add(orBatch);

            orBatch = new SearchBatch();
            string[] orWordWithInWordsZero =
            {
				"earn","earns","earned",
            };

			orBatch.AddRangeSearchWords(orWordWithInWordsZero, 0);
			
            notInBatch = new SearchBatch();
            string[] orWordWithInWordsOne =
            {
				"earn respect","earns respect","earned respect",
            };
			notInBatch.AddRangeSearchWords(orWordWithInWordsOne,1);
            orBatch.NotInWords.Add(notInBatch);
            batch.OrSubSearchBatch.Add(orBatch);


            orBatch = new SearchBatch();
            orBatch.AddSearchWord("pay", 0 );
            notInBatch = new SearchBatch();
            notInBatch.AddSearchWord("pay attention",2 );
			notInBatch.AddSearchWord("pay gas",  2 );
            orBatch.NotInWords.Add(notInBatch);
            batch.OrSubSearchBatch.Add(orBatch);

                
            orBatch = new SearchBatch();
		    orBatch.AddSearchWord("paid",0 );
            notInBatch = new SearchBatch();
            notInBatch.AddSearchWord("paid attention",4 );
			notInBatch.AddSearchWord("paid bills",  3 );
            orBatch.NotInWords.Add(notInBatch);
            batch.OrSubSearchBatch.Add(orBatch);

            orBatch = new SearchBatch();
            orBatch.AddSearchWord("fire", 0 );
            notInBatch = new SearchBatch();
            string[] notInWordWithInWordsZero2nd =
            {
				"fire fighting", "fire drills", "fire drill", "the fire", "fighting fire", "light a fire", "a fire", "fire safety",
			};

			notInBatch.AddRangeSearchWords(notInWordWithInWordsZero2nd, 0 );
			 
            orBatch.NotInWords.Add(notInBatch);
            batch.OrSubSearchBatch.Add(orBatch);


            orBatch = new SearchBatch();
		    orBatch.BatchSearchWords.Add(new SearchWord() { Word = "health", WithinWords = 0 });
            var andBatch = new SearchBatch();

			string[] andWordWithInWordsZero =
            {
                "package", "benefit", "benefits", "benifits", "benfits", "premium", "premiums", "coverage", "ins", "insur", "insurace", "plan", "care"

            };

			andBatch.AddRangeSearchWords(andWordWithInWordsZero,0);
            orBatch.AndMustMatchWords.Add(andBatch);
            batch.OrSubSearchBatch.Add(orBatch);

            orBatch = new SearchBatch();
		    orBatch.AddSearchWord("sick",0 );
            andBatch = new SearchBatch();

			string[] andWordWithInWordsZero2nd =
            {
                "time","day", "days", "hours", "benefits", "leave", "policy","pay",
            };

			andBatch.AddRangeSearchWords(andWordWithInWordsZero2nd, 0 );
			
            orBatch.AndMustMatchWords.Add(andBatch);
            batch.OrSubSearchBatch.Add(orBatch);

            orBatch = new SearchBatch();
            orBatch.AddSearchWord("holiday",0 );
            orBatch.AddSearchWord("holidays", 0 ); 
            notInBatch = new SearchBatch();

            string[] notInWordWithInWordsZero3nd =
            {
				"during holiday","during holidays","holiday season"
			};

			notInBatch.AddRangeSearchWords(notInWordWithInWordsZero3nd, 0);
            orBatch.NotInWords.Add(notInBatch);
            batch.OrSubSearchBatch.Add(orBatch);

                
            orBatch = new SearchBatch();
            orBatch.AddSearchWord("insurance",0 );
            notInBatch = new SearchBatch();
            notInBatch.AddSearchWord("insurance company",0 );
            orBatch.NotInWords.Add(notInBatch);
            batch.OrSubSearchBatch.Add(orBatch);

            cat.Batches.Add(batch);
            return cat;

        }

		//public Category GetCategory()
		//{
		//	var cat = new Category();
		//	cat.Name = "Remuneration";
		//	cat.Id = "c119541";
		//	cat.MinimumMatches = 2;

		//	var batch = new SearchBatch();
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "compensation", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "compensate", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "compensated", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "compensations", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "compensates", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "income", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "wage", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "wages", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "payment", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "little money", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "more money", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "remuneration", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "salary", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "salaries", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "hourly rate", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "bonus*", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "overtime", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "earning", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "earnings", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "commissions", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "commission", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "employee discount", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "my discount", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "social activities", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "recovery days", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "recovery day", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "welfare", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "well being", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "care employee", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "cares employee", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "care employees", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "cares employees", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "care workforce", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "cares workforce", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "care individual", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "cares individual", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "care individuals", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "cares individuals", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "care staff", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "cares staff", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "care people", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "cares people", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "care me", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "cares me", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "care us", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "cares us", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "show concern", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "shows concern", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "show concerns", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "shows concerns", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "concern us", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "concerns us", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "concerned us", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "concern me", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "concerns me", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "concerned me", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "concern staff", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "concerns staff", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "concerned staff", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "concern employee", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "concerns employee", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "concerned employee", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "concern employees", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "concerns employees", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "concerned employees", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "consideration staff", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "considerations staff", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "consideration employee", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "considerations employee", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "consideration employees", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "considerations employees", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "consideration team", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "considerations team", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "empath*", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "sympath*", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "compassion*", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "impersonal", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "disinterest*", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "attentive", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "comforting", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "compassion", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "compassionate", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "regard for staff", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "regard for employee", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "regard for employees", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "look after", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "well health", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "interest employees", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "interest employee", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "interest staff", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "employee health", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "employees health", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "my health", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "physical health", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "health safety", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "pays more", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "pays less", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "pays small", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "pays fair", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "pays fairly", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "pays employees", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "pays employee", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "paycheck", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "paychecks", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "security job", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "security employment", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "security employees", WithinWords = 5 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "security role", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "security future", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "occupational security", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "reliability job", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "reliable job", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "stable job", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "stability job", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "secure job", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "secure jobs", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "laid off", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "lay off", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "lay offs", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "layoffs", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "layoff", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "secure career", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "secure future", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "dependable job", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "retain job", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "retain jobs", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "still have job", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "jobs created", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "lose job", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "losing job", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "unemplo*", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "out of work", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "downsize", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "downsizing", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "Secure employment", WithinWords = 5 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "reliable employment", WithinWords = 5 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "reliable employer", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "stable work", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "unstable work", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "stability work", WithinWords = 5 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "stability job", WithinWords = 5 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "stable job", WithinWords = 5 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "job safe", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "job secured", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "company stability", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "stable company", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "less change", WithinWords = 5 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "less changing", WithinWords = 5 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "changed", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "changing work", WithinWords = 5 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "changing process", WithinWords = 5 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "changing processes", WithinWords = 5 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "stop changing", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "stop altering", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "reorganis*", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "reorganiz*", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "restruc*", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "uncertainty", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "certainty", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "retain employees", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "retaining employees", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "replace employees", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "replaced employees", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "job safety", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "process ambiguity", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "moving around", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "move around", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "moves around", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "moved around", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "stable work", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "unstable work", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "stable order", WithinWords = 5 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "stability work", WithinWords = 5 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "stability job", WithinWords = 5 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "stabiliy orders", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "benefits", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "benifits", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "insurance", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "healthcare", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "retirement", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "pension", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "vacation", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "vacations", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "flextime", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "personal leave days", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "personal time off", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "pto", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "paid time off", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "personal days", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "tuition reimbursement", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "dental", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "medical", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "maternity", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "paternity", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "day off", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "days off", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "time off", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "car", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "sports", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "loans", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "allowance*", WithinWords = 0 });




		//	var orBatch = new SearchBatch();
		//	orBatch.BatchSearchWords.Add(new SearchWord() { Word = "Christmas", WithinWords = 0 });
		//	var notInBatch = new SearchBatch();
		//	notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "christmas period", WithinWords = 0 });
		//	notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "christmas season", WithinWords = 0 });
		//	notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "during christmas", WithinWords = 0 });
		//	notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "for christmas", WithinWords = 0 });
		//	orBatch.NotInWords.Add(notInBatch);
		//	batch.OrSubSearchBatch.Add(orBatch);


		//	orBatch = new SearchBatch();
		//	orBatch.BatchSearchWords.Add(new SearchWord() { Word = "trip", WithinWords = 0 });
		//	notInBatch = new SearchBatch();
		//	notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "business trip", WithinWords = 0 });
		//	orBatch.NotInWords.Add(notInBatch);
		//	batch.OrSubSearchBatch.Add(orBatch);

		//	orBatch = new SearchBatch();
		//	orBatch.BatchSearchWords.Add(new SearchWord() { Word = "earn", WithinWords = 0 });
		//	orBatch.BatchSearchWords.Add(new SearchWord() { Word = "earns", WithinWords = 0 });
		//	orBatch.BatchSearchWords.Add(new SearchWord() { Word = "earned", WithinWords = 0 });
		//	notInBatch = new SearchBatch();
		//	notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "earn respect", WithinWords = 1 });
		//	notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "earns respect", WithinWords = 1 });
		//	notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "earned respect", WithinWords = 1 });
		//	orBatch.NotInWords.Add(notInBatch);
		//	batch.OrSubSearchBatch.Add(orBatch);


		//	orBatch = new SearchBatch();
		//	orBatch.BatchSearchWords.Add(new SearchWord() { Word = "pay", WithinWords = 0 });
		//	notInBatch = new SearchBatch();
		//	notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "pay attention", WithinWords = 2 });
		//	notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "pay gas", WithinWords = 2 });
		//	orBatch.NotInWords.Add(notInBatch);
		//	batch.OrSubSearchBatch.Add(orBatch);


		//	orBatch = new SearchBatch();
		//	orBatch.BatchSearchWords.Add(new SearchWord() { Word = "paid", WithinWords = 0 });
		//	notInBatch = new SearchBatch();
		//	notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "paid attention", WithinWords = 4 });
		//	notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "paid bills", WithinWords = 3 });
		//	orBatch.NotInWords.Add(notInBatch);
		//	batch.OrSubSearchBatch.Add(orBatch);

		//	orBatch = new SearchBatch();
		//	orBatch.BatchSearchWords.Add(new SearchWord() { Word = "fire", WithinWords = 0 });
		//	notInBatch = new SearchBatch();
		//	notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "fire fighting", WithinWords = 0 });
		//	notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "fire drills", WithinWords = 0 });
		//	notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "fire drill", WithinWords = 0 });
		//	notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "the fire", WithinWords = 0 });
		//	notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "fighting fire", WithinWords = 0 });
		//	notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "light a fire", WithinWords = 0 });
		//	notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "a fire", WithinWords = 0 });
		//	notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "fire safety", WithinWords = 0 });
		//	orBatch.NotInWords.Add(notInBatch);
		//	batch.OrSubSearchBatch.Add(orBatch);


		//	orBatch = new SearchBatch();
		//	orBatch.BatchSearchWords.Add(new SearchWord() { Word = "health", WithinWords = 0 });
		//	var andBatch = new SearchBatch();
		//	andBatch.BatchSearchWords.Add(new SearchWord() { Word = "package", WithinWords = 0 });
		//	andBatch.BatchSearchWords.Add(new SearchWord() { Word = "benefit", WithinWords = 0 });
		//	andBatch.BatchSearchWords.Add(new SearchWord() { Word = "benefits", WithinWords = 0 });
		//	andBatch.BatchSearchWords.Add(new SearchWord() { Word = "benifits", WithinWords = 0 });
		//	andBatch.BatchSearchWords.Add(new SearchWord() { Word = "benfits", WithinWords = 0 });
		//	andBatch.BatchSearchWords.Add(new SearchWord() { Word = "premium", WithinWords = 0 });
		//	andBatch.BatchSearchWords.Add(new SearchWord() { Word = "premiums", WithinWords = 0 });
		//	andBatch.BatchSearchWords.Add(new SearchWord() { Word = "coverage", WithinWords = 0 });
		//	andBatch.BatchSearchWords.Add(new SearchWord() { Word = "ins", WithinWords = 0 });
		//	andBatch.BatchSearchWords.Add(new SearchWord() { Word = "insur", WithinWords = 0 });
		//	andBatch.BatchSearchWords.Add(new SearchWord() { Word = "insurace", WithinWords = 0 });
		//	andBatch.BatchSearchWords.Add(new SearchWord() { Word = "plan", WithinWords = 0 });
		//	andBatch.BatchSearchWords.Add(new SearchWord() { Word = "care", WithinWords = 0 });
		//	orBatch.AndMustMatchWords.Add(andBatch);
		//	batch.OrSubSearchBatch.Add(orBatch);

		//	orBatch = new SearchBatch();
		//	orBatch.BatchSearchWords.Add(new SearchWord() { Word = "sick", WithinWords = 0 });
		//	andBatch = new SearchBatch();
		//	andBatch.BatchSearchWords.Add(new SearchWord() { Word = "time", WithinWords = 0 });
		//	andBatch.BatchSearchWords.Add(new SearchWord() { Word = "day", WithinWords = 0 });
		//	andBatch.BatchSearchWords.Add(new SearchWord() { Word = "days", WithinWords = 0 });
		//	andBatch.BatchSearchWords.Add(new SearchWord() { Word = "hours", WithinWords = 0 });
		//	andBatch.BatchSearchWords.Add(new SearchWord() { Word = "benefits", WithinWords = 0 });
		//	andBatch.BatchSearchWords.Add(new SearchWord() { Word = "leave", WithinWords = 0 });
		//	andBatch.BatchSearchWords.Add(new SearchWord() { Word = "policy", WithinWords = 0 });
		//	andBatch.BatchSearchWords.Add(new SearchWord() { Word = "pay", WithinWords = 0 });
		//	orBatch.AndMustMatchWords.Add(andBatch);
		//	batch.OrSubSearchBatch.Add(orBatch);

		//	orBatch = new SearchBatch();
		//	orBatch.BatchSearchWords.Add(new SearchWord() { Word = "holiday", WithinWords = 0 });
		//	orBatch.BatchSearchWords.Add(new SearchWord() { Word = "holidays", WithinWords = 0 });
		//	notInBatch = new SearchBatch();
		//	notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "during holiday", WithinWords = 0 });
		//	notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "during holidays", WithinWords = 0 });
		//	notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "holiday season", WithinWords = 0 });
		//	orBatch.NotInWords.Add(notInBatch);
		//	batch.OrSubSearchBatch.Add(orBatch);


		//	orBatch = new SearchBatch();
		//	orBatch.BatchSearchWords.Add(new SearchWord() { Word = "insurance", WithinWords = 0 });
		//	notInBatch = new SearchBatch();
		//	notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "insurance company", WithinWords = 0 });
		//	orBatch.NotInWords.Add(notInBatch);
		//	batch.OrSubSearchBatch.Add(orBatch);

		//	cat.Batches.Add(batch);
		//	return cat;

		//}
	}
}
