﻿using Ennova.CategorizationService.Service.Model;

namespace Ennova.CategorizationService.Service.Data.Categories
{
    public class TheJob
    {
        public Category GetCategory()
        {
            var cat = new Category();
            cat.Name = "TheJob";
            cat.Id = "c119540";
            cat.MinimumMatches = 2;

			var batch = new SearchBatch();
            string[] wordWithInWordsZero =
            {
                "job development", "job opportunities", "job rotation", "job rotations", "job termination", "job roles", "work task"
			};
            
            string[] wordWithInWordsTwo =
            {
                "job categories", "job important", "perfect job", "tough job", "boring job", "hard job", "work tasks"
			};
            string[] wordWithInWordsThree =
            {
                "job excellent", "job guidelines", "good job", "job responsibility", "job responsibilities", "job role",
                "varied job", "easy tasks", "easy task", "hard tasks", "hard task", "less tasks", "streamlining tasks",
                "streamline tasks", "streamlining task", "streamline task", "respect task", "new tasks", "many tasks",
                "heavy tasks", "heavy task", "lack tasks", "task overload", "tasks overload", "jobs tasks", "job task",
                "job tasks", "enough tasks", "varied tasks", "varied work"
            };
            
            string[] wordWithInWordsFive =
            {
                "job interesting", "job interest", "job interested", "job great", "job best", "job engaging",
                "job engaged", "job engage", "job exciting", "job excited", "job excites", "job excite",
                "job challenges", "job challenged", "job challenging", "personal challenge", "job challenge",
                "job love", "job happy", "job enjoy", "satisfied job", "like job", "grateful job", "pleased job",
                "value job", "quality job", "task interesting", "task interest", "task interested", "task great",
                "task best", "task engaging", "task engaged", "task engage", "task exciting", "task excited",
                "task excites", "task excite", "task challenges", "task challenged", "task challenging",
                "task challenge", "task love", "task happy", "task enjoy", "satisfied task", "like task",
                "grateful task", "pleased task", "value task", "more tasks", "routine tasks", "more task",
                "routine task", "prioritization tasks", "clear tasks", "clear task", "respect tasks", "good tasks",
                "good task", "tasks interesting", "tasks interest", "tasks interested", "tasks great", "tasks best",
                "tasks engaging", "tasks engaged", "tasks engage", "tasks exciting", "tasks excited", "tasks excites",
                "tasks excite", "tasks challenges", "tasks challenged", "tasks challenging", "tasks challenge",
                "tasks love", "tasks happy", "tasks enjoy", "satisfied tasks", "like tasks", "grateful tasks",
                "pleased tasks", "value tasks", "work interesting", "work interest", "work interested", "work great",
                "work best", "work engaging", "work engaged", "work engage", "work exciting", "work excited",
                "work excites", "work excite", "work challenges", "work challenged", "work challenging",
                "work challenge", "work love", "job work", "work enjoy", "satisfied work", "like work", "grateful work",
                "pleased work", "value work", "happy work"
            };

            batch.AddRangeSearchWords(wordWithInWordsZero,0);
            batch.AddRangeSearchWords(wordWithInWordsTwo,2);
            batch.AddRangeSearchWords(wordWithInWordsThree,3);
            batch.AddSearchWord("skills my", 4);
            batch.AddRangeSearchWords(wordWithInWordsFive,5);


			cat.Batches.Add(batch);
			return cat;

        }
        
   //     public Category GetCategory()
   //     {
   //         var cat = new Category();
   //         cat.Name = "TheJob";
   //         cat.Id = "c119540";
   //         cat.MinimumMatches = 2;

			//var batch = new SearchBatch();

			//batch.BatchSearchWords.Add(new SearchWord() { Word = "job interesting", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "job interest", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "job interested", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "job great", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "job best", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "job engaging", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "job engaged", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "job engage", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "job exciting", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "job excited", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "job excites", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "job excite", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "job excellent", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "job challenges", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "job development", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "job opportunities", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "job categories", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "job challenged", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "job guidelines", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "job challenging", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "personal challenge", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "job challenge", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "job love", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "job happy", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "job enjoy", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "satisfied job", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "like job", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "grateful job", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "pleased job", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "value job", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "quality job", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "job rotation", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "job rotations", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "job termination", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "good job", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "job responsibility", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "job responsibilities", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "job role", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "job important", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "perfect job", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "job roles", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "tough job", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "boring job", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "hard job", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "varied job", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "task interesting", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "task interest", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "task interested", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "task great", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "task best", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "task engaging", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "task engaged", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "task engage", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "task exciting", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "task excited", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "task excites", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "task excite", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "task challenges", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "task challenged", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "task challenging", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "task challenge", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "task love", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "task happy", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "task enjoy", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "satisfied task", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "like task", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "grateful task", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "pleased task", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "value task", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "more tasks", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "routine tasks", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "more task", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "routine task", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "prioritization tasks", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "clear tasks", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "clear task", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "work tasks", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "work task", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "easy tasks", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "easy task", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "hard tasks", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "hard task", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "less tasks", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "streamlining tasks", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "streamline tasks", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "streamlining task", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "streamline task", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "respect tasks", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "respect task", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "good tasks", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "good task", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "new tasks", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "many tasks", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "heavy tasks", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "heavy task", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "lack tasks", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "task overload", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "tasks overload", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "jobs tasks", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "job task", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "job tasks", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "enough tasks", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "tasks interesting", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "tasks interest", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "tasks interested", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "tasks great", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "tasks best", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "tasks engaging", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "tasks engaged", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "tasks engage", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "tasks exciting", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "tasks excited", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "tasks excites", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "tasks excite", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "tasks challenges", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "tasks challenged", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "tasks challenging", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "tasks challenge", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "tasks love", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "tasks happy", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "tasks enjoy", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "satisfied tasks", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "like tasks", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "grateful tasks", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "pleased tasks", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "value tasks", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "varied tasks", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "work interesting", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "work interest", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "work interested", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "work great", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "work best", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "work engaging", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "work engaged", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "work engage", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "work exciting", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "work excited", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "work excites", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "work excite", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "work challenges", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "work challenged", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "work challenging", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "work challenge", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "work love", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "job work", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "work enjoy", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "satisfied work", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "like work", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "grateful work", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "pleased work", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "value work", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "happy work", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "skills my", WithinWords = 4 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "varied work", WithinWords = 3 });

   //         cat.Batches.Add(batch);
			//return cat;

   //     }
    }
}
