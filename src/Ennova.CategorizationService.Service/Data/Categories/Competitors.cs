﻿using System.Linq;
using Ennova.CategorizationService.Service.Model;

namespace Ennova.CategorizationService.Service.Data.Categories
{
    public class Competitors
    {
        public Category GetCategory()
        {
            var cat = new Category();
            cat.Name = "Competitors";
            cat.Id = "c132891";
            cat.MinimumMatches = 2;

            var batch = new SearchBatch();

            string[] wordWithZero = { "competitor*", "competing companies", "compete", "competes", "rival", "rivals", "opponent" };

            batch.AddRangeSearchWords(wordWithZero.ToList(), 0);
            
            var orBatch = new SearchBatch();
            orBatch.AddSearchWord("competition", 0 );

            var notInBatch = new SearchBatch();

            string[] wordWith3 = { "competition team", "competition teams", "competition departments", "competition interdepartmental" };
            notInBatch.AddRangeSearchWords(wordWith3.ToList(), 3);
            
            orBatch.NotInWords.Add(notInBatch);
            batch.OrSubSearchBatch.Add(orBatch);
            
            orBatch = new SearchBatch();
            orBatch.AddSearchWord("competing", 0);

            notInBatch = new SearchBatch();
            notInBatch.AddSearchWord("competing positions", 2);
            notInBatch.AddSearchWord("competing each other", 3);
            orBatch.NotInWords.Add(notInBatch);
            batch.OrSubSearchBatch.Add(orBatch);

            orBatch = new SearchBatch();
            orBatch.AddSearchWord("opposition", 0);

            notInBatch = new SearchBatch();
            notInBatch.AddSearchWord("in direct opposition", 0);
            orBatch.NotInWords.Add(notInBatch);
            batch.OrSubSearchBatch.Add(orBatch);

            cat.Batches.Add(batch);
            return cat;
        }

        //public Category GetCategory()
        //{
        //    var cat = new Category();
        //    cat.Name = "Competitors";
        //    cat.Id = "c132891";
        //    cat.MinimumMatches = 2;

        //    var batch = new SearchBatch();
        //    batch.BatchSearchWords.Add(new SearchWord() { Word = "competitor*", WithinWords = 0 });
        //    batch.BatchSearchWords.Add(new SearchWord() { Word = "competing companies", WithinWords = 0 });
        //    batch.BatchSearchWords.Add(new SearchWord() { Word = "compete", WithinWords = 0 });
        //    batch.BatchSearchWords.Add(new SearchWord() { Word = "competes", WithinWords = 0 });
        //    batch.BatchSearchWords.Add(new SearchWord() { Word = "rival", WithinWords = 0 });
        //    batch.BatchSearchWords.Add(new SearchWord() { Word = "rivals", WithinWords = 0 });
        //    batch.BatchSearchWords.Add(new SearchWord() { Word = "opponent", WithinWords = 0 });

            
        //    var orBatch = new SearchBatch();
        //    orBatch.BatchSearchWords.Add(new SearchWord() { Word = "competition", WithinWords = 0 });
        //    var notInBatch = new SearchBatch();
        //    notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "competition team", WithinWords = 3 });
        //    notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "competition teams", WithinWords = 3 });
        //    notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "competition departments", WithinWords = 3 });
        //    notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "competition interdepartmental", WithinWords = 3 });
        //    orBatch.NotInWords.Add(notInBatch);
        //    batch.OrSubSearchBatch.Add(orBatch);

                
        //    orBatch = new SearchBatch();
        //    orBatch.BatchSearchWords.Add(new SearchWord() { Word = "competing", WithinWords = 0 });
        //    notInBatch = new SearchBatch();
        //    notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "competing positions", WithinWords = 2 });
        //    notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "competing each other", WithinWords = 3 });
        //    orBatch.NotInWords.Add(notInBatch);
        //    batch.OrSubSearchBatch.Add(orBatch);

        //    orBatch = new SearchBatch();
        //    orBatch.BatchSearchWords.Add(new SearchWord() { Word = "opposition", WithinWords = 0 });
        //    notInBatch = new SearchBatch();
        //    notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "in direct opposition", WithinWords = 0 });
        //    orBatch.NotInWords.Add(notInBatch);
        //    batch.OrSubSearchBatch.Add(orBatch);

        //    cat.Batches.Add(batch);
        //    return cat;
        //}
    }
}
