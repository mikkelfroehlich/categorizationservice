﻿using Ennova.CategorizationService.Service.Model;

namespace Ennova.CategorizationService.Service.Data.Categories
{
    public class Faithfulness
    {
        public Category GetCategory()
        {
            var cat = new Category();
            cat.Name = "Faithfulness";
            cat.Id = "c119547";
            cat.MinimumMatches = 2;

			var batch = new SearchBatch();

            string[] wordWithInWordsZero =
            {
                "seeking employment", "regular employee", "permanent employee", "regular employees",
                "permanent employees", "trustworthy companies", "trustworthy company", "trust in the future of",
                "can be trusted", "satisfied with", "highly of the company", "position elsewhere",
                "positions elsewhere", "earn more elsewhere", "opportunities elsewhere", "better deal elsewhere"
            };
            string[] wordWithInWordsTwo =
            {
				"recommend to other", "recommend to others", "no future", "employment outside of"
			};

            string[] wordWithInWordsThree =
            {
                "trusting company", "believe in company", "believe in companies", "believe in product",
                "believe in products", "believe in team", "believe in teams", "believe in management",
                "believe in managements", "believe in organisation", "believe in organisations", "believe in brand",
                "dependable product", "dependable products", "dependable quality", "dependable performance",
                "dependable team", "dependable teams", "dependable company", "dependable organisation",
                "dependable brand", "reliable product", "reliable products", "reliable quality", "reliable performance",
                "reliable team", "reliable teams", "reliable company", "reliable organisation", "reliable brand",
                "stable product", "stable products", "stable quality", "stable performance", "stable team",
                "stable teams", "stable company", "stable organisation", "stable brand", "highly of supervisors",
                "employment elsewhere", "employment somewhere else", "searching somewhere else", "searching elsewhere",
                "jobs somewhere else", "jobs elsewhere", "job somewhere else", "job elsewhere", "work somewhere else",
                "work elsewhere", "looking for somewhere else", "looking for elsewhere", "seeking somewhere else",
                "seeking elsewhere", "seek somewhere else", "seek elsewhere", "look somewhere else", "look elsewhere",
                "looking somewhere else", "looking elsewhere", "look for outside company"
            };

            string[] wordWithInWordsFive =
            {
                "recommend job", "recommend to work", "recommend company", "recommended job", "recommended to work",
                "recommended company", "recommending job", "recommending to work", "recommending company",
                "recommendation job", "recommendation to work", "recommendation company", "recommend as place work"
            };

			batch.AddRangeSearchWords(wordWithInWordsZero, 0);
			batch.AddSearchWord("job outside",  1);
			batch.AddRangeSearchWords(wordWithInWordsTwo, 2);
			batch.AddRangeSearchWords(wordWithInWordsThree, 3);
			batch.AddRangeSearchWords(wordWithInWordsFive, 5);

            var orBatch = new SearchBatch();
		    orBatch.AddSearchWord("loyal",0 );
			orBatch.AddSearchWord("loyalty",0 );
            var notInBatch = new SearchBatch();
            string[] notInordWithInWordsZero =
            {
                "loyal clients", "loyal client", "loyal customers", "loyal customer", "loyal costumers",
                "loyal custumers", "loyal custumer", "customer loyalty", "customers loyalty", "client loyalty",
                "clients loyalty"
            };
			notInBatch.AddRangeSearchWords(notInordWithInWordsZero,0);
			 
            orBatch.NotInWords.Add(notInBatch);
            batch.OrSubSearchBatch.Add(orBatch);



			orBatch = new SearchBatch();
			orBatch.AddSearchWord("trust company", 3);
            notInBatch = new SearchBatch();
		    notInBatch.AddSearchWord("company trust", 2);
            orBatch.NotInWords.Add(notInBatch);
            batch.OrSubSearchBatch.Add(orBatch);


            orBatch = new SearchBatch();
            orBatch.AddSearchWord("faith*", 0 );
            notInBatch = new SearchBatch();
			notInBatch.AddSearchWord("faith in employees",3 );
			notInBatch.AddSearchWord("faithful employees", 0 );
			notInBatch.AddSearchWord("faith in me", 0 );
            orBatch.NotInWords.Add(notInBatch);
            batch.OrSubSearchBatch.Add(orBatch);


            cat.Batches.Add(batch);
			return cat;

        }
        
   //     public Category GetCategory()
   //     {
   //         var cat = new Category();
   //         cat.Name = "Faithfulness";
   //         cat.Id = "c119547";
   //         cat.MinimumMatches = 2;

			//var batch = new SearchBatch();

			//batch.BatchSearchWords.Add(new SearchWord() { Word = "recommend job", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "recommend to work", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "recommend company", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "recommended job", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "recommended to work", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "recommended company", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "recommending job", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "recommending to work", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "recommending company", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "recommendation job", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "recommendation to work", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "recommendation company", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "recommend to other", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "recommend to others", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "recommend as place work", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "seeking employment", WithinWords = 0 });
   //         batch.BatchSearchWords.Add(new SearchWord() { Word = "regular employee", WithinWords = 0 });
   //         batch.BatchSearchWords.Add(new SearchWord() { Word = "permanent employee", WithinWords = 0 });
   //         batch.BatchSearchWords.Add(new SearchWord() { Word = "regular employees", WithinWords = 0 });
   //         batch.BatchSearchWords.Add(new SearchWord() { Word = "permanent employees", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "trusting company", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "trustworthy companies", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "trustworthy company", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "trust in the future of", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "can be trusted", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "believe in company", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "believe in companies", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "believe in product", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "believe in products", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "believe in team", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "believe in teams", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "believe in management", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "believe in managements", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "believe in organisation", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "believe in organisations", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "believe in brand", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "dependable product", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "dependable products", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "dependable quality", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "dependable performance", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "dependable team", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "dependable teams", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "dependable company", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "dependable organisation", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "dependable brand", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "reliable product", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "reliable products", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "reliable quality", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "reliable performance", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "reliable team", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "reliable teams", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "reliable company", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "reliable organisation", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "reliable brand", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "stable product", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "stable products", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "stable quality", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "stable performance", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "stable team", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "stable teams", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "stable company", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "stable organisation", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "stable brand", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "satisfied with", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "highly of supervisors", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "highly of the company", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "job outside", WithinWords = 1 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "no future", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "employment elsewhere", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "employment somewhere else", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "searching somewhere else", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "searching elsewhere", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "jobs somewhere else", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "jobs elsewhere", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "job somewhere else", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "job elsewhere", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "work somewhere else", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "work elsewhere", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "looking for somewhere else", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "looking for elsewhere", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "seeking somewhere else", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "seeking elsewhere", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "seek somewhere else", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "seek elsewhere", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "look somewhere else", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "look elsewhere", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "looking somewhere else", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "looking elsewhere", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "position elsewhere", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "positions elsewhere", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "earn more elsewhere", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "opportunities elsewhere", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "better deal elsewhere", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "look for outside company", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "employment outside of", WithinWords = 2 });

   //         var orBatch = new SearchBatch();
		 //   orBatch.BatchSearchWords.Add(new SearchWord() { Word = "loyal", WithinWords = 0 });
			//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "loyalty", WithinWords = 0 });
   //         var notInBatch = new SearchBatch();
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "loyal clients", WithinWords = 0 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "loyal client", WithinWords = 0 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "loyal customers", WithinWords = 0 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "loyal customer", WithinWords = 0 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "loyal costumers", WithinWords = 0 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "loyal custumers", WithinWords = 0 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "loyal custumer", WithinWords = 0 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "customer loyalty", WithinWords = 0 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "customers loyalty", WithinWords = 0 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "client loyalty", WithinWords = 0 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "clients loyalty", WithinWords = 0 });
   //         orBatch.NotInWords.Add(notInBatch);
   //         batch.OrSubSearchBatch.Add(orBatch);



			//orBatch = new SearchBatch();
			//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "trust company", WithinWords = 3 });
   //         notInBatch = new SearchBatch();
		 //   notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "company trust", WithinWords = 2 });
   //         orBatch.NotInWords.Add(notInBatch);
   //         batch.OrSubSearchBatch.Add(orBatch);


   //         orBatch = new SearchBatch();
   //         orBatch.BatchSearchWords.Add(new SearchWord() { Word = "faith*", WithinWords = 0 });
   //         notInBatch = new SearchBatch();
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "faith in employees", WithinWords = 3 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "faithful employees", WithinWords = 0 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "faith in me", WithinWords = 0 });
   //         orBatch.NotInWords.Add(notInBatch);
   //         batch.OrSubSearchBatch.Add(orBatch);


   //         cat.Batches.Add(batch);
			//return cat;

   //     }
    }
}
