﻿using System.Linq;
using Ennova.CategorizationService.Service.Model;

namespace Ennova.CategorizationService.Service.Data.Categories
{
    public class Customers
    {

		public Category GetCategory()
		{
			var cat = new Category();
			cat.Name = "Customers";
			cat.Id = "c132892";
			cat.MinimumMatches = 2;

			var batch = new SearchBatch();

			string[] wordWithZero = {
				"customer friendly", "customer oriented", "customers oriented", "customer driven", "customer centric", "customer centricity", "our customers",
				"customer satisfaction", "customers satisfaction", "customers satisfactions", "customer satisfactions", "customer expectation", "customer expectations",
				"customers expectation", "customers expectations", "customers requirements", "customers needs", "customers need", "customers requirement", "lost customers",
				"lost customer", "to the customer", "for the customer", "to our customer", "for our customer", "end user", "end users", "to the customers",
				"for the customers", "to our customers", "for our customers"
			};
			batch.AddRangeSearchWords(wordWithZero.ToList(), 0);

			string[] wordWithOne = { "customer first", "customers first", "loose customer", "loose customers", "close to customer", "close to customers" };
			batch.AddRangeSearchWords(wordWithZero.ToList(), 1);

			string[] wordWithTwo = {
				"serve customer", "serve customers", "serves customer", "service support", "customer support", "customer communication",
				"customer view", "customer views", "customers view", "customer perspective", "customers perspective", "towards customers", "satisfied customer",
				"satisfy customer", "satisfy customers", "satisfying customer", "satisfying customers", "customer requirement", "customer requirements",
				"customer need", "customer needs", "value customers", "value customer", "losing customers", "losing customer"
			};
			batch.AddRangeSearchWords(wordWithZero.ToList(), 2);

			string[] wordWithThree =
			{
				"focus customer", "focus customers", "focused customer", "focussed customer", "customers focused", "customers focussed", "important customers",
				"customer service", "customer relationship", "customer relation", "satisfied customers", "unsatisfied customer", "unsatisfied customers", "dissatisfied customer",
				"dissatisfied customers", "supporting customer", "help customer", "help customers", "helping customer", "helping customers", "deliver customer",
				"assist customer", "listen customers", "listen customer", "complaint customer", "customers complaint", "customer complaints", "assist customers",
				"assisting customer", "assisting customers", "deliver customers", "delivering customer", "delivering customers", "customer requirements", "support customer",
				"support customers", "supporting customers",
			};
			batch.AddRangeSearchWords(wordWithZero.ToList(), 3);
			
			cat.Batches.Add(batch);
			return cat;

		}


		//public Category GetCategory()
  //      {
  //          var cat = new Category();
  //          cat.Name = "Customers";
  //          cat.Id = "c132892";
  //          cat.MinimumMatches = 2;

		//	var batch = new SearchBatch();
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "focus customer", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "focus customers", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "focused customer", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "focussed customer", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "customers focused", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "customers focussed", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "important customers", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "customer friendly", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "customer oriented", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "customers oriented", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "serve customer", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "serve customers", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "serves customer", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "customer first", WithinWords = 1 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "customers first", WithinWords = 1 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "customer service", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "service support", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "customer support", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "customer driven", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "customer centric", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "customer centricity", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "customer communication", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "our customers", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "customer view", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "customer views", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "customers view", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "customer perspective", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "customers perspective", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "towards customers", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "customer relationship", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "customer relation", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "customer happy", WithinWords = 1 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "customers happy", WithinWords = 1 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "satisfied customer", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "satisfied customers", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "unsatisfied customer", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "unsatisfied customers", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "dissatisfied customer", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "dissatisfied customers", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "customer satisfaction", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "customers satisfaction", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "customers satisfactions", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "customer satisfactions", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "satisfy customer", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "satisfy customers", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "satisfying customer", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "satisfying customers", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "customer expectation", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "customer expectations", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "customers expectation", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "customers expectations", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "customer requirement", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "customer requirements", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "customers requirements", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "customer need", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "customer needs", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "customers needs", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "customers need", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "customers requirement", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "customer requirements", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "losing customers", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "losing customer", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "loose customer", WithinWords = 1 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "loose customers", WithinWords = 1 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "lost customers", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "lost customer", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "value customers", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "value customer", WithinWords = 2 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "support customer", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "supporting customer", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "help customer", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "help customers", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "helping customer", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "helping customers", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "deliver customer", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "assist customer", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "listen customers", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "listen customer", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "complaint customer", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "customers complaint", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "customer complaints", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "assist customers", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "assisting customer", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "assisting customers", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "deliver customers", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "delivering customer", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "delivering customers", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "close to customer", WithinWords = 1 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "to the customer", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "for the customer", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "to our customer", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "for our customer", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "end user", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "end users", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "support customers", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "supporting customers", WithinWords = 3 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "close to customers", WithinWords = 1 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "to the customers", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "for the customers", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "to our customers", WithinWords = 0 });
		//	batch.BatchSearchWords.Add(new SearchWord() { Word = "for our customers", WithinWords = 0 }); 


  //          cat.Batches.Add(batch);            
  //          return cat;

  //      }
    }
}
