﻿using Ennova.CategorizationService.Service.Model;

namespace Ennova.CategorizationService.Service.Data.Categories
{
    public class Other
    {
    
        public Category GetCategory()
        {
            var cat = new Category();
            cat.Name = "Other";
            cat.Id = "uc1787";
            cat.MinimumMatches = 2;
            return cat;
        }
    }
}