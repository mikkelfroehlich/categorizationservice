﻿using Ennova.CategorizationService.Service.Model;

namespace Ennova.CategorizationService.Service.Data.Categories
{
    public class SeniorManagement
    {
        public Category GetCategory()
        {
            var cat = new Category();
            cat.Name = "SeniorManagement";
            cat.Id = "c119536";
            cat.MinimumMatches = 2;

			var batch = new SearchBatch();

            string[] wordWithInWordsZero =
            {
				"leadership*", "goal*", "vision*", "envision*", "strateg*", "value*", "blueprint*", "mission*", "ambition*", "target*", "objective*", "direction*",
                "high level decisions", "long term decisions", "LT decisions"
			};

            string[] wordWithInWordsTwo =
            {
				 "decisions above","executive decision", "executive decisions", "managers decisions","management decisions", "company decisions", "top down decisions",
                 "decisions top down"
			};


            batch.AddRangeSearchWords(wordWithInWordsZero, 0);
            batch.AddRangeSearchWords(wordWithInWordsTwo, 2);

			batch.AddSearchWord("leadership decisions", 3 );
			batch.AddSearchWord("business decisions", 3 );
			
			var orbatch = new SearchBatch();

			string [] orWordWithInWordsZero = {
                    "senior leader", "senior leaders", "senior officials", "senior managers", "senior manager", "senior mgmt", "senior managements", "senior management",
                    "sr leader", "sr leaders", "sr officials", "sr managers", "sr manager", "sr mgmt", "sr managements", "sr management", "upper leader", "upper leaders",
                    "upper officials", "upper managers", "upper manager", "upper mgmt", "upper managements", "upper management", "top leader", "top leaders", "top officials",
                    "top managers", "top manager", "top mgmt", "top managements", "top management", "corporate leader", "corporate leaders", "corporate officials",
                    "corporate managers", "corporate manager", "corporate mgmt", "corporate managements", "corporate management", "corp leader",
                    "corp leaders", "corp officials", "corp managers", "corp manager", "corp mgmt", "corp managements", "corp management", "higher leader",
                    "higher leaders", "higher officials", "higher managers", "higher manager", "higher mgmt", "higher managements", "higher management", "company leader",
                    "company leaders", "company officials", "company managers", "company manager", "company mgmt", "company managements", "company management",
                    "exec", "president", "vp", "vice president", "svp", "evp", "senior level", "executive*", "C?O", "CM",
			};
            
            orbatch.AddRangeSearchWords(orWordWithInWordsZero, 0);


			var notinBatch = new SearchBatch();
            string[] notInWordWithInWordsZero =
            {
                "below vp","executive capabilities","below director", "area director", "regional director", "department director"
			};
			notinBatch.AddRangeSearchWords(notInWordWithInWordsZero, 0);
			

			orbatch.NotInWords.Add(notinBatch);
            batch.OrSubSearchBatch.Add(orbatch);
                
            cat.Batches.Add(batch);
            return cat;

        }
        //public Category GetCategory()
   //     {
   //         var cat = new Category();
   //         cat.Name = "SeniorManagement";
   //         cat.Id = "c119536";
   //         cat.MinimumMatches = 2;

			//var batch = new SearchBatch();

			//batch.BatchSearchWords.Add(new SearchWord() { Word = "leadership*", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "goal*", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "vision*", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "envision*", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "strateg*", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "value*", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "blueprint*", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "mission*", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "ambition*", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "target*", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "objective*", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "direction*", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "high level decisions", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "managers decisions", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "management decisions", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "company decisions", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "top down decisions", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "decisions top down", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "leadership decisions", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "business decisions", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "long term decisions", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "LT decisions", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "decisions above", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "executive decision", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "executive decisions", WithinWords = 2 });


			//var orbatch = new SearchBatch();
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "senior leader", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "senior leaders", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "senior officials", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "senior managers", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "senior manager", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "senior mgmt", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "senior managements", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "senior management", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "sr leader", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "sr leaders", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "sr officials", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "sr managers", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "sr manager", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "sr mgmt", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "sr managements", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "sr management", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "upper leader", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "upper leaders", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "upper officials", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "upper managers", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "upper manager", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "upper mgmt", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "upper managements", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "upper management", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "top leader", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "top leaders", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "top officials", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "top managers", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "top manager", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "top mgmt", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "top managements", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "top management", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "corporate leader", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "corporate leaders", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "corporate officials", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "corporate managers", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "corporate manager", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "corporate mgmt", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "corporate managements", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "corporate management", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "corp leader", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "corp leaders", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "corp officials", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "corp managers", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "corp manager", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "corp mgmt", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "corp managements", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "corp management", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "higher leader", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "higher leaders", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "higher officials", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "higher managers", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "higher manager", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "higher mgmt", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "higher managements", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "higher management", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "company leader", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "company leaders", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "company officials", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "company managers", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "company manager", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "company mgmt", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "company managements", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "company management", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "exec", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "president", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "vp", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "vice president", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "svp", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "evp", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "senior level", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "executive*", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "C?O", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "CM", WithinWords = 0 });


			//var notinBatch = new SearchBatch();
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "below vp", WithinWords = 0 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "executive capabilities", WithinWords = 0 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "below director", WithinWords = 0 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "area director", WithinWords = 0 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "regional director", WithinWords = 0 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "department director", WithinWords = 0 });

			//orbatch.NotInWords.Add(notinBatch);
   //         batch.OrSubSearchBatch.Add(orbatch);
                
   //         cat.Batches.Add(batch);
   //         return cat;

   //     }
    }
}
