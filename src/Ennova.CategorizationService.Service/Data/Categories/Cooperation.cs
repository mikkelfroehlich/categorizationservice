﻿using Ennova.CategorizationService.Service.Model;

namespace Ennova.CategorizationService.Service.Data.Categories
{
    public class Cooperation
    {
		public Category GetCategory()
		{
			var cat = new Category();
			cat.Name = "Cooperation";
			cat.Id = "c119538";
			cat.MinimumMatches = 2;

			var batch = new SearchBatch();

            string[] wordWithZero =
            {
                "more collaboration", "honest collaboration", "no collaboration", "collaborate more",
                "collaborate well", "collaborate better", "global collaboration", "collaboration between",
                "more cooperation", "honest cooperation", "cooperation between", "no cooperation", "cooperate more",
                "cooperate well", "cooperate better", "more coordination", "honest coordination", "no coordination",
                "coordination between", "coordinate more", "coordinate well", "coordinate better", "disagreement*",
                "misunderstanding*", "miscommunication*", "work together", "inter department", "inter departmental",
                "inter departmentally", "silo*", "brainstorm*", "brain storm", "brain storms", "brain storming",
                "more communication", "honest communication", "communication between", "no communication",
                "mutual communication", "communications", "communicate more", "communicate well", "communicate better",
                "knowledge sharing", "communicated", "more information", "info", "open conversation",
                "conversations between", "conversation between", "meaningful conversations", "meaningful conversation",
                "conversation with staff", "no conversations on", "transparency", "transparent", "relationship",
                "relationships", "togetherness", "teamwork", "team work", "team working", "working team", "work team",
                "build team", "team build", "team player", "team players", "team building", "teambuilding",
                "help other", "help others", "co worker", "co workers", "coworker*", "workmate*", "teammate*",
                "team mate", "team mates", "teammembers", "teammember", "with expertise", "well trained",
                "respect other", "respect others", "trust others", "trust other", "HR", "sales department",
                "marketing department", "market department", "finance department", "tax department",
                "profit & loss department", "P&L departments", "tech department", "technical department",
                "IT department", "R&D department", "Design department", "QC department", "documents departments",
                "service department", "services department", "maintenance department", "Canteen Stores Department",
                "CSD Department", "sales team", "marketing team", "market team", "finance team", "tax team",
                "profit & loss team", "P&L teams", "tech team", "technical team", "IT team", "R&D team", "Design team",
                "QC team", "documents teams", "service team", "services team", "maintenance team",
                "Canteen Stores team", "CSD team", "sales dptmt", "marketing dptmt", "market dptmt", "finance dptmt",
                "tax dptmt", "profit & loss dptmt", "P&L dptmts", "tech dptmt", "technical dptmt", "IT dptmt",
                "R&D dptmt", "Design dptmt", "QC dptmt", "documents dptmts", "service dptmt", "services dptmt",
                "maintenance dptmt", "Canteen Stores dptmt", "CSD dptmt", "them and us", "us and them", "work together","team members","team member"
			};

            string[] wordWithOne = {"team spirit", "colleagues"};
            string[] wordWithTwo =
            {
                "information sharing", "sharing information", "verbal communication", "verbal communications",
                "information shared", "sharing knowledge", "knowledges sharing", "sharing knowledges",
                "lack of conversations", "has conversations", "more conversation", "transparent information",
                "collaboration employees", "working together", "works together", "worked together", "cooperation staff",
                "cooperation employees", "developing cooperation", "develop cooperation", "communication",
                "experienced people", "professional people", "skilled people", "knowledgeable people",
                "knowledgable people", "capable people", "talent people", "trained people", "motivated people",
                "systematic people", "unskilled people", "valuable people", "reliable people", "dedicated people",
                "frustrated people", "excellent people", "talented people", "skill people", "skills people",
                "capability people", "capabilities people", "hard working people", "good people", "great people",
                "experienced worker", "experienced workers", "professional worker", "professional workers",
                "skilled worker", "skilled workers", "knowledgeable worker", "knowledgeable workers",
                "knowledgable worker", "knowledgable workers", "capable worker", "capable workers", "talent worker",
                "talent workers", "trained worker", "trained workers", "motivated worker", "motivated workers",
                "systematic worker", "systematic workers", "unskilled worker", "unskilled workers", "valuable worker",
                "valuable workers", "reliable worker", "reliable workers", "dedicated worker", "dedicated workers",
                "frustrated worker", "frustrated workers", "excellent worker", "excellent workers", "talented worker",
                "talented workers", "skill worker", "skill workers", "skills worker", "skills workers",
                "capability worker", "capability workers", "capabilities worker", "capabilities workers",
                "hard working worker", "hard working workers", "good worker", "good workers", "great worker",
                "great workers", "experienced employee", "experienced employees", "professional employee",
                "professional employees", "skilled employee", "skilled employees", "knowledgeable employee",
                "knowledgeable employees", "knowledgable employee", "knowledgable employees", "capable employee",
                "capable employees", "talent employee", "talent employees", "trained employee", "trained employees",
                "motivated employee", "motivated employees", "systematic employee", "systematic employees",
                "unskilled employee", "unskilled employees", "valuable employee", "valuable employees",
                "reliable employee", "reliable employees", "dedicated employee", "dedicated employees",
                "frustrated employee", "frustrated employees", "excellent employee", "excellent employees",
                "talented employee", "talented employees", "skill employee", "skill employees", "skills employee",
                "skills employees", "capability employee", "capability employees", "capabilities employee",
                "capabilities employees", "hard working employee", "hard working employees", "good employee",
                "good employees", "great employee", "great employees", "experienced engineer", "experienced engineers",
                "professional engineer", "professional engineers", "skilled engineer", "skilled engineers",
                "knowledgeable engineer", "knowledgeable engineers", "knowledgable engineer", "knowledgable engineers",
                "capable engineer", "capable engineers", "talent engineer", "talent engineers", "trained engineer",
                "trained engineers", "motivated engineer", "motivated engineers", "systematic engineer",
                "systematic engineers", "unskilled engineer", "unskilled engineers", "valuable engineer",
                "valuable engineers", "reliable engineer", "reliable engineers", "dedicated engineer",
                "dedicated engineers", "frustrated engineer", "frustrated engineers", "excellent engineer",
                "excellent engineers", "talented engineer", "talented engineers", "skill engineer", "skill engineers",
                "skills engineer", "skills engineers", "capability engineer", "capability engineers",
                "capabilities engineer", "capabilities engineers", "hard working engineer", "hard working engineers",
                "good engineer", "good engineers", "great engineer", "great engineers", "experienced workforce",
                "professional workforce", "skilled workforce", "knowledgeable workforce", "knowledgable workforce",
                "capable workforce", "talent workforce", "trained workforce", "motivated workforce",
                "systematic workforce", "unskilled workforce", "valuable workforce", "reliable workforce",
                "dedicated workforce", "frustrated workforce", "excellent workforce", "talented workforce",
                "skill workforce", "skills workforce", "capability workforce", "capabilities workforce",
                "hard working workforce", "good workforce", "great workforce", "experienced individual",
                "experienced individuals", "professional individual", "professional individuals", "skilled individual",
                "skilled individuals", "knowledgeable individual", "knowledgeable individuals",
                "knowledgable individual", "knowledgable individuals", "capable individual", "capable individuals",
                "talent individual", "talent individuals", "trained individual", "trained individuals",
                "motivated individual", "motivated individuals", "systematic individual", "systematic individuals",
                "unskilled individual", "unskilled individuals", "valuable individual", "valuable individuals",
                "reliable individual", "reliable individuals", "dedicated individual", "dedicated individuals",
                "frustrated individual", "frustrated individuals", "excellent individual", "excellent individuals",
                "talented individual", "talented individuals", "skill individual", "skill individuals",
                "skills individual", "skills individuals", "capability individual", "capability individuals",
                "capabilities individual", "capabilities individuals", "hard working individual",
                "hard working individuals", "good individual", "good individuals", "great individual",
                "great individuals", "experienced staff", "experienced staffs", "professional staff",
                "professional staffs", "skilled staff", "skilled staffs", "knowledgeable staff", "knowledgeable staffs",
                "knowledgable staff", "knowledgable staffs", "capable staff", "capable staffs", "talent staff",
                "talent staffs", "trained staff", "trained staffs", "motivated staff", "motivated staffs",
                "systematic staff", "systematic staffs", "unskilled staff", "unskilled staffs", "valuable staff",
                "valuable staffs", "reliable staff", "reliable staffs", "dedicated staff", "dedicated staffs",
                "frustrated staff", "frustrated staffs", "excellent staff", "excellent staffs", "talented staff",
                "talented staffs", "skill staff", "skill staffs", "skills staff", "skills staffs", "capability staff",
                "capability staffs", "capabilities staff", "capabilities staffs", "hard working staff",
                "hard working staffs", "good staff", "good staffs", "great staff", "great staffs",
                "experienced technician", "experienced technicians", "professional technician",
                "professional technicians", "skilled technician", "skilled technicians", "knowledgeable technician",
                "knowledgeable technicians", "knowledgable technician", "knowledgable technicians",
                "capable technician", "capable technicians", "talent technician", "talent technicians",
                "trained technician", "trained technicians", "motivated technician", "motivated technicians",
                "systematic technician", "systematic technicians", "unskilled technician", "unskilled technicians",
                "valuable technician", "valuable technicians", "reliable technician", "reliable technicians",
                "dedicated technician", "dedicated technicians", "frustrated technician", "frustrated technicians",
                "excellent technician", "excellent technicians", "talented technician", "talented technicians",
                "skill technician", "skill technicians", "skills technician", "skills technicians",
                "capability technician", "capability technicians", "capabilities technician",
                "capabilities technicians", "hard working technician", "hard working technicians", "good technician",
                "good technicians", "great technician", "great technicians", "perfect team","knowledge sharing","cooperation staff","develop cooperation"
			};

            string[] wordWithThree =
            {
                "improve collaboration", "better collaboration", "improvement collaboration", "greater collaboration",
                "open collaboration", "poor collaboration", "lack collaboration", "weak collaboration",
                "clear collaboration", "good collaboration", "clearer collaboration", "direct collaboration",
                "acceptable collaboration", "enough collaboration", "sufficient collaboration", "enhance collaboration",
                "inadequate collaboration", "little collaboration", "frequent collaboration", "proper collaboration",
                "ensure collaboration", "encourage collaboration", "internal collaboration", "stronger collaboration",
                "level collaboration", "necessary collaboration", "develop collaboration", "employees collaboration",
                "employee collaboration", "missing collaboration", "lacking collaboration", "effective collaboration",
                "focus collaboration", "improved collaboration", "active collaboration", "collaboration improving",
                "problem collaboration", "problems collaboration", "topdown collaboration", "increase collaboration",
                "increased collaboration", "collaborate employee", "collaborate employees", "collaboration bu",
                "collaboration units", "collaboration business unit", "collaboration team", "collaboration department",
                "collaboration division", "collaboration function", "collaboration site", "collaboration group",
                "collaboration workers", "collaboration entity", "collaboration bus", "collaboration business units",
                "collaboration teams", "collaboration departments", "collaboration divisions",
                "collaboration functions", "collaboration sites", "collaboration groups", "collaboration entities",
                "collaborate bu", "collaborate business unit", "collaborate team", "collaborate department",
                "collaborate division", "collaborate function", "collaborate site", "collaborate group",
                "collaborate entity", "collaborate bus", "collaborate business units", "collaborate teams",
                "collaborate departments", "collaborate divisions", "collaborate functions", "collaborate sites",
                "collaborate groups", "collaborate entities", "collaboration staff", "improve cooperation",
                "better cooperation", "improvement cooperation", "open cooperation", "good cooperation",
                "poor cooperation", "closer cooperation", "lack cooperation", "weak cooperation", "clear cooperation",
                "greater cooperation", "acceptable cooperation", "encourage cooperation", "proper cooperation",
                "ensure cooperation", "global cooperation", "stronger cooperation", "clearer cooperation",
                "direct cooperation", "enough cooperation", "sufficient cooperation", "strong cooperation",
                "enhance cooperation", "inadequate cooperation", "little cooperation", "frequent cooperation",
                "internal cooperation", "level cooperation", "necessary cooperation", "employees cooperation",
                "employee cooperation", "missing cooperation", "lacking cooperation", "effective cooperation",
                "focus cooperation", "improved cooperation", "active cooperation", "cooperation improving",
                "problem cooperation", "problems cooperation", "topdown cooperation", "increase cooperation",
                "increased cooperation", "cooperate employee", "cooperate workers", "cooperation workers",
                "cooperate employees", "cooperation bu", "cooperation business unit", "cooperation team",
                "cooperation department", "cooperation division", "cooperation function", "cooperation site",
                "cooperation group", "cooperation entity", "cooperation bus", "cooperation business units",
                "cooperation teams", "cooperation departments", "cooperation divisions", "cooperation functions",
                "cooperation sites", "cooperation groups", "cooperation entities", "cooperate bu",
                "cooperate business unit", "cooperate team", "cooperate department", "cooperate division",
                "cooperate function", "cooperate site", "cooperate group", "cooperate entity", "cooperate bus",
                "cooperate business units", "cooperate teams", "cooperate departments", "cooperate divisions",
                "cooperate functions", "cooperate sites", "cooperate groups", "cooperate entities",
                "improve coordination", "better coordination", "improvement coordination", "open coordination",
                "poor coordination", "lack coordination", "weak coordination", "clear coordination",
                "good coordination", "clearer coordination", "direct coordination", "enough coordination",
                "sufficient coordination", "global coordination", "enhance coordination", "inadequate coordination",
                "little coordination", "frequent coordination", "internal coordination", "level coordination",
                "necessary coordination", "develop coordination", "employees coordination", "employee coordination",
                "missing coordination", "lacking coordination", "effective coordination", "focus coordination",
                "improved coordination", "active coordination", "coordination improving", "problem coordination",
                "problems coordination", "topdown coordination", "increase coordination", "increased coordination",
                "coordinate employee", "coordinate employees", "coordination staff", "coordination bu",
                "coordination business unit", "coordination team", "coordination department", "coordination division",
                "coordination function", "coordination site", "coordination group", "coordination entity",
                "coordination bus", "coordination business units", "coordination teams", "coordination departments",
                "coordination divisions", "coordination functions", "coordination sites", "coordination groups",
                "coordination entities", "coordinate bu", "coordinate business unit", "coordinate team",
                "coordinate department", "coordinate division", "coordinate function", "coordinate site",
                "coordinate group", "coordinate entity", "coordinate bus", "coordinate business units",
                "coordinate teams", "coordinate departments", "coordinate divisions", "coordinate functions",
                "coordinate sites", "coordinate groups", "coordinate entities", "conflicts department",
                "conflict department", "conflicts team", "conflict team", "conflicts group", "conflict group",
                "work together bu", "work together business unit", "work together team", "work together department",
                "work together division", "work together function", "work together site", "work together group",
                "work together entity", "work together bus", "work together business units", "work together teams",
                "work together departments", "work together divisions", "work together functions",
                "work together sites", "work together groups", "work together entities", "working together bu",
                "working together business unit", "working together team", "working together department",
                "working together division", "working together function", "working together site",
                "working together group", "working together entity", "working together bus",
                "working together business units", "working together teams", "working together departments",
                "working together divisions", "working together functions", "working together sites",
                "working together groups", "working together entities", "cross bu", "cross business unit", "cross team",
                "cross department", "cross division", "cross function", "cross site", "cross group", "cross entity",
                "cross bus", "cross business units", "cross teams", "cross departments", "cross divisions",
                "cross functions", "cross sites", "cross groups", "cross entities", "with other bus",
                "with other business units", "with other teams", "with other departments", "with other divisions",
                "with other functions", "with other sites", "with other groups", "with other entities",
                "with another bu", "with another business unit", "with another team", "with another department",
                "with another division", "with another function", "with another site", "with another group",
                "with another entity", "improve communication", "better communication", "improvement communication",
                "greater communication", "acceptable communication", "lateral communication", "encourage communication",
                "proper communication", "ensure communication", "stronger communication", "open communication",
                "poor communication", "global communication", "bilateral communication", "lack communication",
                "weak communication", "clear communication", "good communication", "clearer communication",
                "direct communication", "enough communication", "sufficient communication", "enhance communication",
                "inadequate communication", "little communication", "frequent communication", "internal communication",
                "level communication", "necessary communication", "develop communication", "employees communication",
                "employee communication", "communication improve", "missing communication", "lacking communication",
                "effective communication", "focus communication", "improved communication", "active communication",
                "communication improving", "problem communication", "problems communication", "topdown communication",
                "increase communication", "increased communication", "communication bu", "communication business unit",
                "communication team", "communication department", "communication division", "communication function",
                "communication site", "communication group", "communication entity", "communication bus",
                "communication business units", "communication teams", "communication departments",
                "communication divisions", "communication functions", "communication sites", "communication groups",
                "communication entities", "communicate bu", "communicate business unit", "communicate team",
                "communicate department", "communicate division", "communicate function", "communicate site",
                "communicate group", "communicate entity", "communicate bus", "communicate business units",
                "communicate teams", "communicate departments", "communicate divisions", "communicate functions",
                "communicate sites", "communicate groups", "communicate entities", "communication staff",
                "communicate employee", "communicate employees", "communicates bu", "communicates business unit",
                "communicates team", "communicates department", "communicates division", "communicates function",
                "communicates site", "communicates group", "communicates entity", "communicates bus",
                "communicates business units", "communicates teams", "communicates departments",
                "communicates divisions", "communicates functions", "communicates sites", "communicates groups",
                "communicates entities", "information everyone", "share information", "shares information",
                "share knowledge", "shares knowledge", "share knowledges", "shares knowledges", "together team",
                "team member", "team members", "team playing", "team effort", "help each other", "better dialogue",
                "respect each other", "respect team", "respect department", "respect group", "respect employees",
                "trust each other", "trust team", "trust department", "trust group", "team improvements",
                "department improvements", "dptmt improvements","good cooperation","more collaboration","more cooperation", "more communication","more coordination"
			};
            string[] wordWithFour = { "communication better", "Team building"};
            string[] wordWithFive =
            {
                "colleagues to help", "colleagues helpful", "colleagues to support", "colleagues supportive",
                "colleague to help", "colleague helpful", "colleague to support", "colleague supportive",
                "coworkers to help", "coworkers helpful", "coworkers to support", "coworkers supportive",
                "coworker to help", "coworker helpful", "coworker to support", "coworker supportive",
            };

			batch.AddRangeSearchWords(wordWithZero,0);
			batch.AddRangeSearchWords(wordWithOne,1);
			batch.AddRangeSearchWords(wordWithTwo,2);
			batch.AddRangeSearchWords(wordWithThree,3);
			batch.AddRangeSearchWords(wordWithFour,4);
			batch.AddRangeSearchWords(wordWithFive,5);


            string[] orWordWithZero = {"collaborating", "collaborative", "collaborate", "collaborates" };

			var orBatch = new SearchBatch();
            orBatch.AddRangeSearchWords(orWordWithZero, 0);

            orBatch.AddSearchWord("collaboration",2 );

			var notInBatch = new SearchBatch();
            string[] notInWordWithZero = { "robot", "robots", "collaborative applications" };
            notInBatch.AddRangeSearchWords(notInWordWithZero, 0);

			orBatch.NotInWords.Add(notInBatch);
			batch.OrSubSearchBatch.Add(orBatch);



			orBatch = new SearchBatch();
			orBatch.AddSearchWord("Lets talk",0 );
			notInBatch = new SearchBatch();
			notInBatch.AddSearchWord("lets talk about",0 );
			notInBatch.AddSearchWord("lets talk more about",0 );
			orBatch.NotInWords.Add(notInBatch);
			batch.OrSubSearchBatch.Add(orBatch);



			orBatch = new SearchBatch();
			batch.BatchSearchWords.Add(new SearchWord() { Word = "together", WithinWords = 0 });
			var andBatch = new SearchBatch();

            string[] andWordWithZero = 
            {
                "bu", "business unit", "unit", "department", "division", "function", "site", "group", "entity", "bus", "business units", "units", "teams",
                "departments", "divisions", "functions", "sites", "groups", "entities"
            };
            andBatch.AddRangeSearchWords(andWordWithZero, 0);
			orBatch.AndMustMatchWords.Add(andBatch);
			batch.OrSubSearchBatch.Add(orBatch);

			orBatch = new SearchBatch();
			orBatch.AddSearchWord("cooperate", 0);
			orBatch.AddSearchWord("cooperates", 0 );
			orBatch.AddSearchWord("cooperating", 0);
			orBatch.AddSearchWord("cooperation", 1);

			notInBatch = new SearchBatch();
            andBatch.AddSearchWord("teams we co-operate with", 0);
            andBatch.AddSearchWord("people who cooperate", 0);
            andBatch.AddSearchWord("the cooperating local leader", 0);
			orBatch.NotInWords.Add(notInBatch);
			batch.OrSubSearchBatch.Add(orBatch);


			orBatch = new SearchBatch();
			orBatch.AddSearchWord("coordinate", 0 );
			orBatch.AddSearchWord("coordinates",  0 );
			orBatch.AddSearchWord("coordinating", 0 );
			orBatch.AddSearchWord("coordination", 1 );

			andBatch = new SearchBatch();
			andBatch.AddSearchWord("with", 0 );
			andBatch.AddSearchWord("together", 0 );
			orBatch.AndMustMatchWords.Add(andBatch);
			batch.OrSubSearchBatch.Add(orBatch);
			


			
            string[] orWordWithThree2orBatch = {
                    "between bu", "between business unit", "between team", "between department", "between division", "between function", "between site",
                    "between group", "between entity", "between bus", "between business units", "between teams", "between departments", "between divisions",
                    "between functions", "between sites", "between groups", "between entities",
			};
            orBatch = new SearchBatch();
			orBatch.AddRangeSearchWords(orWordWithThree2orBatch, 3);

			
			notInBatch = new SearchBatch();
			notInBatch.AddSearchWord("move between",0 );
			orBatch.NotInWords.Add(notInBatch);
			batch.OrSubSearchBatch.Add(orBatch);


			orBatch = new SearchBatch();
			orBatch.AddSearchWord("between and us",2 );
			notInBatch = new SearchBatch();
			notInBatch.AddSearchWord("between us and competition",0 );
			notInBatch.AddSearchWord("between us and our competitors",0);
			orBatch.NotInWords.Add(notInBatch);
			batch.OrSubSearchBatch.Add(orBatch);


            string[] orWordWithThree2orBatch2nd =
            {
                "across bu","across business unit", "across team", "across department", "across division", "across function", "across site", "across group", "across entity",
                "across bus", "across business units", "across teams", "across departments", "across divisions", "across functions", "across sites", "across groups", "across entities",
			};

			orBatch = new SearchBatch();
            orBatch.AddRangeSearchWords(orWordWithThree2orBatch2nd,3);
			
			notInBatch = new SearchBatch();
			notInBatch.AddSearchWord("move across",0 );
			orBatch.NotInWords.Add(notInBatch);
			batch.OrSubSearchBatch.Add(orBatch);



			
            string[] orWordWithThree2orBatch3nd =
            {
                "avoid isolation", "creates isolation", "working in isolation", "isolated from"
			};
            orBatch = new SearchBatch();
			orBatch.AddRangeSearchWords(orWordWithThree2orBatch3nd,0);
			
			var ororBatch = new SearchBatch();
			ororBatch.AddSearchWord("feel", 0);
			var orAndBatch = new SearchBatch();
			orAndBatch.AddSearchWord("isolated", 0 );
			ororBatch.AndMustMatchWords.Add(orAndBatch);
			orBatch.OrSubSearchBatch.Add(ororBatch);
			notInBatch = new SearchBatch();

            string[] notInWordWithZeroNotInBatch =
            {
				"isolated issues", "isolated incident", "single isolated department", "isolate the sounds", "isolated place", "Looking isolated at",
			};

			notInBatch.AddRangeSearchWords(notInWordWithZeroNotInBatch,0);
			 
			orBatch.NotInWords.Add(notInBatch);
			batch.OrSubSearchBatch.Add(orBatch);

            string[] orWordWithInThree =
            {
                "improve relations", "improvement relations", "improved relations", "relations improving",
                "increase relations", "increased relations", "encourage relations",
                "enhance relations", "develop relations", "greater relations", "good relations", "better relations",
                "proper relations", "stronger relations",
                "open relations", "global relations", "bilateral relations", "effective relations", "poor relations",
                "lack relations", "lacking relations",
                "weak relations", "inadequate relations", "little relations", "employees relations",
                "employee relations", "problem relations", "problems relations",
                "topdown relations", "relations bu", "relations business unit", "relations team",
                "relations department", "relations division",
                "relations group", "relations entity", "relations bus", "relations business units", "relations teams",
                "relations departments", "relations divisions",
                "relations functions", "relations groups", "relations entities", "relations staff"
            };

			orBatch = new SearchBatch();
			orBatch.AddSearchWord("relations between", 0 );
            orBatch.AddRangeSearchWords(orWordWithInThree,3);


			notInBatch = new SearchBatch();
			notInBatch.AddSearchWord("public relations department",0 );
			notInBatch.AddSearchWord("better family relations", 0);
			orBatch.NotInWords.Add(notInBatch);
			batch.OrSubSearchBatch.Add(orBatch);


			orBatch = new SearchBatch();
			orBatch.AddSearchWord("no help from",  0 );
			orBatch.AddSearchWord("no support from", 0 );
			notInBatch = new SearchBatch();
			notInBatch.AddSearchWord("manager", 0);
			notInBatch.AddSearchWord("management",0 );
			notInBatch.AddSearchWord("leader", 0 );
			orBatch.NotInWords.Add(notInBatch);
			batch.OrSubSearchBatch.Add(orBatch);


			orBatch = new SearchBatch();
            string[] orWordWithInzero3nd =
            {
                "my team", "great team", "our team", "positive team", "negative team", "team driven", "more team"
			};

            orBatch.AddRangeSearchWords(orWordWithInzero3nd,0);

            orBatch.AddSearchWord("team player",2);

            string[] orWordWithInThree3nd =
            {
				"good team", "fantastic team", "grow team", "strong team", "amazing team", "knowledgeable team",
			};
			orBatch.AddRangeSearchWords(orWordWithInThree3nd,3);
			
			
            orBatch.AddSearchWord("committed team", 5);
			notInBatch = new SearchBatch();
			notInBatch.AddSearchWord( "our team leader",0 );
			orBatch.NotInWords.Add(notInBatch);
			batch.OrSubSearchBatch.Add(orBatch);

			cat.Batches.Add(batch);
			return cat;
		}



		//     public Category GetCategory()
		//     {
		//         var cat = new Category();
		//         cat.Name = "Cooperation";
		//         cat.Id = "c119538";
		//         cat.MinimumMatches = 2;

		//var batch = new SearchBatch();

		//batch.BatchSearchWords.Add(new SearchWord() { Word = "working together", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "work together", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "works together", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "worked together", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "improve collaboration", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "better collaboration", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "improvement collaboration", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "more collaboration", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "honest collaboration", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "greater collaboration", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "open collaboration", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "poor collaboration", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "lack collaboration", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "weak collaboration", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "clear collaboration", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "good collaboration", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "clearer collaboration", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "direct collaboration", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "acceptable collaboration", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "enough collaboration", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "sufficient collaboration", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "enhance collaboration", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "inadequate collaboration", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "more collaboration", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "little collaboration", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "frequent collaboration", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "proper collaboration", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "ensure collaboration", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "encourage collaboration", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "internal collaboration", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "stronger collaboration", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "level collaboration", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "necessary collaboration", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "develop collaboration", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "employees collaboration", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "employee collaboration", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "missing collaboration", WithinWords = 3 }); 
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "lacking collaboration", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "effective collaboration", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "focus collaboration", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "improved collaboration", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "active collaboration", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "no collaboration", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "collaboration improving", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "problem collaboration", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "problems collaboration", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "topdown collaboration", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "increase collaboration", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "increased collaboration", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "collaborate more", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "collaborate employee", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "collaborate employees", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "collaborate well", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "collaborate better", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "collaboration bu", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "global collaboration", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "collaboration units", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "collaboration business unit", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "collaboration team", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "collaboration department", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "collaboration division", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "collaboration function", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "collaboration site", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "collaboration group", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "collaboration workers", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "collaboration entity", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "collaboration bus", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "collaboration business units", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "collaboration teams", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "collaboration between", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "collaboration departments", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "collaboration divisions", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "collaboration functions", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "collaboration sites", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "collaboration groups", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "collaboration entities", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "collaborate bu", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "collaborate business unit", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "collaborate team", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "collaborate department", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "collaborate division", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "collaborate function", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "collaborate site", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "collaborate group", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "collaborate entity", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "collaborate bus", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "collaborate business units", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "collaborate teams", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "collaborate departments", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "collaborate divisions", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "collaborate functions", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "collaborate sites", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "collaborate groups", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "collaborate entities", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "collaboration staff", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "collaboration employees", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "improve cooperation", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "better cooperation", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "improvement cooperation", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "more cooperation", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "honest cooperation", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "open cooperation", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "good cooperation", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "poor cooperation", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "closer cooperation", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "lack cooperation", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "weak cooperation", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "clear cooperation", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "good cooperation", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "greater cooperation", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "acceptable cooperation", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "encourage cooperation", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "proper cooperation", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "ensure cooperation", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "global cooperation", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "stronger cooperation", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "clearer cooperation", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "direct cooperation", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "enough cooperation", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "sufficient cooperation", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "strong cooperation", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "enhance cooperation", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "inadequate cooperation", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "more cooperation", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "little cooperation", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "frequent cooperation", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "internal cooperation", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "level cooperation", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "necessary cooperation", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "develop cooperation", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "employees cooperation", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "employee cooperation", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "missing cooperation", WithinWords = 3 }); 
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "lacking cooperation", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "effective cooperation", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "focus cooperation", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "improved cooperation", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "active cooperation", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cooperation between", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "no cooperation", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cooperation improving", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "problem cooperation", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "problems cooperation", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "topdown cooperation", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "increase cooperation", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "increased cooperation", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cooperate more", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cooperate employee", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cooperate workers", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cooperation workers", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cooperate employees", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cooperate well", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cooperate better", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cooperation staff", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cooperation bu", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cooperation business unit", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cooperation team", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cooperation department", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cooperation division", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cooperation function", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cooperation site", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cooperation group", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cooperation entity", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cooperation bus", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cooperation business units", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cooperation teams", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cooperation departments", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cooperation divisions", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cooperation functions", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cooperation sites", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cooperation groups", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cooperation entities", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cooperate bu", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cooperate business unit", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cooperate team", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cooperate department", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cooperate division", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cooperate function", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cooperate site", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cooperate group", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cooperate entity", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cooperate bus", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cooperate business units", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cooperate teams", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cooperate departments", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cooperate divisions", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cooperate functions", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cooperate sites", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cooperate groups", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cooperate entities", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cooperation staff", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cooperation employees", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "developing cooperation", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "develop cooperation", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "improve coordination", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "better coordination", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "improvement coordination", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "more coordination", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "honest coordination", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "open coordination", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "poor coordination", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "lack coordination", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "weak coordination", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "clear coordination", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "good coordination", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "clearer coordination", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "direct coordination", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "enough coordination", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "sufficient coordination", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "global coordination", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "enhance coordination", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "inadequate coordination", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "more coordination", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "little coordination", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "frequent coordination", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "internal coordination", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "level coordination", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "necessary coordination", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "develop coordination", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "employees coordination", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "employee coordination", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "missing coordination", WithinWords = 3 }); 
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "lacking coordination", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "effective coordination", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "focus coordination", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "improved coordination", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "active coordination", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "no coordination", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coordination improving", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coordination between", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "problem coordination", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "problems coordination", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "topdown coordination", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "increase coordination", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "increased coordination", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coordinate more", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coordinate employee", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coordinate employees", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coordinate well", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coordinate better", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coordination staff", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coordination bu", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coordination business unit", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coordination team", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coordination department", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coordination division", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coordination function", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coordination site", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coordination group", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coordination entity", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coordination bus", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coordination business units", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coordination teams", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coordination departments", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coordination divisions", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coordination functions", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coordination sites", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coordination groups", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coordination entities", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coordinate bu", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coordinate business unit", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coordinate team", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coordinate department", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coordinate division", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coordinate function", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coordinate site", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coordinate group", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coordinate entity", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coordinate bus", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coordinate business units", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coordinate teams", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coordinate departments", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coordinate divisions", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coordinate functions", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coordinate sites", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coordinate groups", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coordinate entities", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "conflicts department", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "conflict department", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "conflicts team", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "conflict team", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "conflicts group", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "conflict group", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "disagreement*", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "misunderstanding*", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "miscommunication*", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "work together bu", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "work together business unit", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "work together team", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "work together department", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "work together division", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "work together function", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "work together site", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "work together group", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "work together entity", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "work together bus", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "work together business units", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "work together teams", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "work together departments", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "work together divisions", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "work together functions", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "work together sites", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "work together groups", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "work together entities", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "work together", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "working together bu", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "working together business unit", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "working together team", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "working together department", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "working together division", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "working together function", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "working together site", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "working together group", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "working together entity", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "working together bus", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "working together business units", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "working together teams", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "working together departments", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "working together divisions", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "working together functions", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "working together sites", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "working together groups", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "working together entities", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cross bu", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cross business unit", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cross team", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cross department", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cross division", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cross function", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cross site", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cross group", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cross entity", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cross bus", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cross business units", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cross teams", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cross departments", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cross divisions", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cross functions", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cross sites", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cross groups", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "cross entities", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "inter department", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "inter departmental", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "inter departmentally", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "with other bus", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "with other business units", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "with other teams", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "with other departments", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "with other divisions", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "with other functions", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "with other sites", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "with other groups", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "with other entities", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "with another bu", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "with another business unit", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "with another team", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "with another department", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "with another division", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "with another function", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "with another site", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "with another group", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "with another entity", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "silo*", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "brainstorm*", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "brain storm", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "brain storms", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "brain storming", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communication", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "improve communication", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "better communication", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "improvement communication", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "more communication", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "honest communication", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "greater communication", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "acceptable communication", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communication better", WithinWords = 4 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "lateral communication", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "encourage communication", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "proper communication", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "ensure communication", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "stronger communication", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "open communication", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "poor communication", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "global communication", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "bilateral communication", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "lack communication", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "weak communication", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "clear communication", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "good communication", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "clearer communication", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "direct communication", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "enough communication", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "sufficient communication", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "enhance communication", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "inadequate communication", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "more communication", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "little communication", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "frequent communication", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "internal communication", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "level communication", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "necessary communication", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "develop communication", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "employees communication", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "employee communication", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communication improve", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "missing communication", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "lacking communication", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "effective communication", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communication between", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "focus communication", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "improved communication", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "active communication", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "no communication", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communication improving", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "problem communication", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "problems communication", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "topdown communication", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "increase communication", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "increased communication", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "mutual communication", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communication bu", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communication business unit", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communication team", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communication department", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communication division", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communication function", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communication site", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communication group", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communication entity", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communication bus", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communication business units", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communication teams", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communication departments", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communication divisions", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communication functions", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communication sites", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communication groups", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communication entities", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communicate bu", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communicate business unit", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communicate team", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communicate department", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communicate division", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communicate function", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communicate site", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communicate group", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communicate entity", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communicate bus", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communicate business units", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communicate teams", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communicate departments", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communicate divisions", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communicate functions", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communications", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communicate sites", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communicate groups", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communicate entities", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communication staff", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "verbal communication", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "verbal communications", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communicate more", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communicate employee", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communicate employees", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communicate well", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communicate better", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communicates bu", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communicates business unit", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communicates team", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communicates department", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communicates division", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communicates function", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communicates site", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communicates group", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communicates entity", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communicates bus", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communicates business units", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communicates teams", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communicates departments", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communicates divisions", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communicates functions", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communicates sites", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communicates groups", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communicates entities", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communicated", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "information everyone", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "more information", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "info", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "share information", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "information sharing", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "sharing information", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "shares information", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "information shared", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "knowledge sharing", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "share knowledge", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "knowledge sharing", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "sharing knowledge", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "shares knowledge", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "share knowledges", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "knowledges sharing", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "sharing knowledges", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "shares knowledges", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "open conversation", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "conversations between", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "conversation between", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "meaningful conversations", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "meaningful conversation", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "conversation with staff", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "no conversations on", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "lack of conversations", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "has conversations", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "more conversation", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "transparency", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "transparent", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "transparent information", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "relationship", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "relationships", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "team spirit", WithinWords = 1 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "together team", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "togetherness", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "teamwork", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "team work", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "team working", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "working team", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "work team", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "Team building", WithinWords = 4 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "build team", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "team build", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "perfect team", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "team member", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "team members", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "team playing", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "team player", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "team players", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "team effort", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "team building", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "teambuilding", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "help each other", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "help other", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "help others", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "better dialogue", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "colleagues to help", WithinWords = 5 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "colleagues helpful", WithinWords = 5 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "colleagues to support", WithinWords = 5 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "colleagues supportive", WithinWords = 5 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "colleague to help", WithinWords = 5 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "colleague helpful", WithinWords = 5 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "colleague to support", WithinWords = 5 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "colleague supportive", WithinWords = 5 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coworkers to help", WithinWords = 5 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coworkers helpful", WithinWords = 5 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coworkers to support", WithinWords = 5 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coworkers supportive", WithinWords = 5 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coworker to help", WithinWords = 5 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coworker helpful", WithinWords = 5 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coworker to support", WithinWords = 5 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coworker supportive", WithinWords = 5 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "colleagues", WithinWords = 1 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "co worker", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "co workers", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "coworker*", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "workmate*", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "teammate*", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "team mate", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "team mates", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "team members", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "team member", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "teammembers", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "teammember", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "experienced people", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "professional people", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "skilled people", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "knowledgeable people", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "knowledgable people", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "capable people", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "talent people", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "trained people", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "motivated people", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "systematic people", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "unskilled people", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "valuable people", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "reliable people", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "dedicated people", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "frustrated people", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "excellent people", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "talented people", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "skill people", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "skills people", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "capability people", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "capabilities people", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "hard working people", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "good people", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "great people", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "experienced worker", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "experienced workers", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "professional worker", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "professional workers", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "skilled worker", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "skilled workers", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "knowledgeable worker", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "knowledgeable workers", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "knowledgable worker", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "knowledgable workers", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "capable worker", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "capable workers", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "talent worker", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "talent workers", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "trained worker", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "trained workers", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "motivated worker", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "motivated workers", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "systematic worker", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "systematic workers", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "unskilled worker", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "unskilled workers", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "valuable worker", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "valuable workers", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "reliable worker", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "reliable workers", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "dedicated worker", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "dedicated workers", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "frustrated worker", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "frustrated workers", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "excellent worker", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "excellent workers", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "talented worker", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "talented workers", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "skill worker", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "skill workers", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "skills worker", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "skills workers", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "capability worker", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "capability workers", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "capabilities worker", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "capabilities workers", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "hard working worker", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "hard working workers", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "good worker", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "good workers", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "great worker", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "great workers", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "experienced employee", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "experienced employees", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "professional employee", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "professional employees", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "skilled employee", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "skilled employees", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "knowledgeable employee", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "knowledgeable employees", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "knowledgable employee", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "knowledgable employees", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "capable employee", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "capable employees", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "talent employee", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "talent employees", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "trained employee", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "trained employees", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "motivated employee", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "motivated employees", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "systematic employee", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "systematic employees", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "unskilled employee", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "unskilled employees", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "valuable employee", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "valuable employees", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "reliable employee", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "reliable employees", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "dedicated employee", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "dedicated employees", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "frustrated employee", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "frustrated employees", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "excellent employee", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "excellent employees", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "talented employee", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "talented employees", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "skill employee", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "skill employees", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "skills employee", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "skills employees", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "capability employee", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "capability employees", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "capabilities employee", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "capabilities employees", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "hard working employee", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "hard working employees", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "good employee", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "good employees", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "great employee", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "great employees", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "experienced engineer", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "experienced engineers", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "professional engineer", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "professional engineers", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "skilled engineer", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "skilled engineers", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "knowledgeable engineer", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "knowledgeable engineers", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "knowledgable engineer", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "knowledgable engineers", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "capable engineer", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "capable engineers", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "talent engineer", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "talent engineers", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "trained engineer", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "trained engineers", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "motivated engineer", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "motivated engineers", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "systematic engineer", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "systematic engineers", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "unskilled engineer", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "unskilled engineers", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "valuable engineer", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "valuable engineers", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "reliable engineer", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "reliable engineers", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "dedicated engineer", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "dedicated engineers", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "frustrated engineer", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "frustrated engineers", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "excellent engineer", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "excellent engineers", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "talented engineer", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "talented engineers", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "skill engineer", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "skill engineers", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "skills engineer", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "skills engineers", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "capability engineer", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "capability engineers", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "capabilities engineer", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "capabilities engineers", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "hard working engineer", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "hard working engineers", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "good engineer", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "good engineers", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "great engineer", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "great engineers", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "experienced workforce", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "professional workforce", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "skilled workforce", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "knowledgeable workforce", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "knowledgable workforce", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "capable workforce", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "talent workforce", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "trained workforce", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "motivated workforce", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "systematic workforce", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "unskilled workforce", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "valuable workforce", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "reliable workforce", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "dedicated workforce", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "frustrated workforce", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "excellent workforce", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "talented workforce", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "skill workforce", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "skills workforce", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "capability workforce", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "capabilities workforce", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "hard working workforce", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "good workforce", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "great workforce", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "experienced individual", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "experienced individuals", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "professional individual", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "professional individuals", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "skilled individual", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "skilled individuals", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "knowledgeable individual", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "knowledgeable individuals", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "knowledgable individual", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "knowledgable individuals", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "capable individual", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "capable individuals", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "talent individual", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "talent individuals", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "trained individual", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "trained individuals", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "motivated individual", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "motivated individuals", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "systematic individual", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "systematic individuals", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "unskilled individual", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "unskilled individuals", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "valuable individual", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "valuable individuals", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "reliable individual", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "reliable individuals", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "dedicated individual", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "dedicated individuals", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "frustrated individual", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "frustrated individuals", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "excellent individual", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "excellent individuals", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "talented individual", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "talented individuals", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "skill individual", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "skill individuals", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "skills individual", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "skills individuals", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "capability individual", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "capability individuals", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "capabilities individual", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "capabilities individuals", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "hard working individual", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "hard working individuals", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "good individual", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "good individuals", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "great individual", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "great individuals", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "experienced staff", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "experienced staffs", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "professional staff", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "professional staffs", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "skilled staff", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "skilled staffs", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "knowledgeable staff", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "knowledgeable staffs", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "knowledgable staff", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "knowledgable staffs", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "capable staff", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "capable staffs", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "talent staff", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "talent staffs", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "trained staff", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "trained staffs", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "motivated staff", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "motivated staffs", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "systematic staff", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "systematic staffs", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "unskilled staff", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "unskilled staffs", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "valuable staff", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "valuable staffs", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "reliable staff", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "reliable staffs", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "dedicated staff", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "dedicated staffs", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "frustrated staff", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "frustrated staffs", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "excellent staff", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "excellent staffs", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "talented staff", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "talented staffs", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "skill staff", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "skill staffs", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "skills staff", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "skills staffs", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "capability staff", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "capability staffs", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "capabilities staff", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "capabilities staffs", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "hard working staff", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "hard working staffs", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "good staff", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "good staffs", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "great staff", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "great staffs", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "experienced technician", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "experienced technicians", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "professional technician", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "professional technicians", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "skilled technician", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "skilled technicians", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "knowledgeable technician", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "knowledgeable technicians", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "knowledgable technician", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "knowledgable technicians", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "capable technician", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "capable technicians", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "talent technician", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "talent technicians", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "trained technician", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "trained technicians", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "motivated technician", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "motivated technicians", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "systematic technician", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "systematic technicians", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "unskilled technician", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "unskilled technicians", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "valuable technician", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "valuable technicians", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "reliable technician", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "reliable technicians", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "dedicated technician", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "dedicated technicians", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "frustrated technician", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "frustrated technicians", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "excellent technician", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "excellent technicians", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "talented technician", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "talented technicians", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "skill technician", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "skill technicians", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "skills technician", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "skills technicians", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "capability technician", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "capability technicians", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "capabilities technician", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "capabilities technicians", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "hard working technician", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "hard working technicians", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "good technician", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "good technicians", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "great technician", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "great technicians", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "with expertise", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "well trained", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "respect each other", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "respect other", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "respect others", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "respect team", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "respect department", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "respect group", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "respect employees", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "trust others", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "trust each other", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "trust other", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "trust team", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "trust department", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "trust group", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "HR", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "sales department", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "marketing department", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "market department", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "finance department", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "tax department", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "profit & loss department", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "P&L departments", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "tech department", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "technical department", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "IT department", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "R&D department", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "Design department", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "QC department", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "documents departments", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "service department", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "services department", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "maintenance department", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "Canteen Stores Department", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "CSD Department", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "sales team", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "marketing team", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "market team", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "finance team", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "tax team", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "profit & loss team", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "P&L teams", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "tech team", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "technical team", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "IT team", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "R&D team", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "Design team", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "QC team", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "documents teams", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "service team", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "services team", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "maintenance team", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "Canteen Stores team", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "CSD team", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "sales dptmt", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "marketing dptmt", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "market dptmt", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "finance dptmt", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "tax dptmt", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "profit & loss dptmt", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "P&L dptmts", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "tech dptmt", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "technical dptmt", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "IT dptmt", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "R&D dptmt", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "Design dptmt", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "QC dptmt", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "documents dptmts", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "service dptmt", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "services dptmt", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "maintenance dptmt", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "Canteen Stores dptmt", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "CSD dptmt", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "team improvements", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "department improvements", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "dptmt improvements", WithinWords = 3 });
		//         batch.BatchSearchWords.Add(new SearchWord() { Word = "them and us", WithinWords = 0 });
		//         batch.BatchSearchWords.Add(new SearchWord() { Word = "us and them", WithinWords = 0 });


		//var orBatch = new SearchBatch();
		//         orBatch.BatchSearchWords.Add(new SearchWord() { Word = "collaborating", WithinWords = 0 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "collaboration", WithinWords = 2 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "collaborative", WithinWords = 0 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "collaborate", WithinWords = 0 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "collaborates", WithinWords = 0 }); 
		//         var notInBatch = new SearchBatch();
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "robot", WithinWords = 0 });
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "robots", WithinWords = 0 });
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "collaborative applications", WithinWords = 0 });
		//orBatch.NotInWords.Add(notInBatch);
		//batch.OrSubSearchBatch.Add(orBatch);



		//         orBatch = new SearchBatch();
		//      orBatch.BatchSearchWords.Add(new SearchWord() { Word = "Lets talk", WithinWords = 0 });
		//         notInBatch = new SearchBatch();
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "lets talk about", WithinWords = 0 });
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "lets talk more about", WithinWords = 0 });
		//         orBatch.NotInWords.Add(notInBatch);
		//         batch.OrSubSearchBatch.Add(orBatch);



		//         orBatch = new SearchBatch();
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "together", WithinWords = 0 });
		//         var andBatch = new SearchBatch();
		//andBatch.BatchSearchWords.Add(new SearchWord() { Word = "bu", WithinWords = 0 });
		//andBatch.BatchSearchWords.Add(new SearchWord() { Word = "business unit", WithinWords = 0 });
		//andBatch.BatchSearchWords.Add(new SearchWord() { Word = "unit", WithinWords = 0 });
		//andBatch.BatchSearchWords.Add(new SearchWord() { Word = "department", WithinWords = 0 });
		//andBatch.BatchSearchWords.Add(new SearchWord() { Word = "division", WithinWords = 0 });
		//andBatch.BatchSearchWords.Add(new SearchWord() { Word = "function", WithinWords = 0 });
		//andBatch.BatchSearchWords.Add(new SearchWord() { Word = "site", WithinWords = 0 });
		//andBatch.BatchSearchWords.Add(new SearchWord() { Word = "group", WithinWords = 0 });
		//andBatch.BatchSearchWords.Add(new SearchWord() { Word = "entity", WithinWords = 0 });
		//andBatch.BatchSearchWords.Add(new SearchWord() { Word = "bus", WithinWords = 0 });
		//andBatch.BatchSearchWords.Add(new SearchWord() { Word = "business units", WithinWords = 0 });
		//andBatch.BatchSearchWords.Add(new SearchWord() { Word = "units", WithinWords = 0 });
		//andBatch.BatchSearchWords.Add(new SearchWord() { Word = "teams", WithinWords = 0 });
		//andBatch.BatchSearchWords.Add(new SearchWord() { Word = "departments", WithinWords = 0 });
		//andBatch.BatchSearchWords.Add(new SearchWord() { Word = "divisions", WithinWords = 0 });
		//andBatch.BatchSearchWords.Add(new SearchWord() { Word = "functions", WithinWords = 0 });
		//andBatch.BatchSearchWords.Add(new SearchWord() { Word = "sites", WithinWords = 0 });
		//andBatch.BatchSearchWords.Add(new SearchWord() { Word = "groups", WithinWords = 0 });
		//andBatch.BatchSearchWords.Add(new SearchWord() { Word = "entities", WithinWords = 0 });
		//         orBatch.AndMustMatchWords.Add(andBatch);
		//         batch.OrSubSearchBatch.Add(orBatch);

		//         orBatch = new SearchBatch();
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "cooperate", WithinWords = 0 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "cooperates", WithinWords = 0 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "cooperating", WithinWords = 0 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "cooperation", WithinWords = 1 }); 
		//         notInBatch = new SearchBatch();
		//andBatch.BatchSearchWords.Add(new SearchWord() { Word = "teams we co-operate with", WithinWords = 0 });
		//andBatch.BatchSearchWords.Add(new SearchWord() { Word = "people who cooperate", WithinWords = 0 });
		//andBatch.BatchSearchWords.Add(new SearchWord() { Word = "the cooperating local leader", WithinWords = 0 });
		//         orBatch.NotInWords.Add(notInBatch);
		//         batch.OrSubSearchBatch.Add(orBatch);


		//         orBatch = new SearchBatch();
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "coordinate", WithinWords = 0 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "coordinates", WithinWords = 0 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "coordinating", WithinWords = 0 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "coordination", WithinWords = 1 });
		//         andBatch = new SearchBatch();
		//andBatch.BatchSearchWords.Add(new SearchWord() { Word = "with", WithinWords = 0 });
		//andBatch.BatchSearchWords.Add(new SearchWord() { Word = "together", WithinWords = 0 });
		//         orBatch.AndMustMatchWords.Add(andBatch);
		//         batch.OrSubSearchBatch.Add(orBatch);



		//         orBatch = new SearchBatch();
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "between bu", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "between business unit", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "between team", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "between department", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "between division", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "between function", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "between site", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "between group", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "between entity", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "between bus", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "between business units", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "between teams", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "between departments", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "between divisions", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "between functions", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "between sites", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "between groups", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "between entities", WithinWords = 3 }); 
		//         notInBatch = new SearchBatch();
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "move between", WithinWords = 0 });
		//         orBatch.NotInWords.Add(notInBatch);
		//         batch.OrSubSearchBatch.Add(orBatch);


		//         orBatch = new SearchBatch();
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "between and us", WithinWords = 2 });
		//         notInBatch = new SearchBatch();
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "between us and competition", WithinWords = 0 });
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "between us and our competitors", WithinWords = 0 });
		//         orBatch.NotInWords.Add(notInBatch);
		//         batch.OrSubSearchBatch.Add(orBatch);



		//         orBatch = new SearchBatch();
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "across bu", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "across business unit", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "across team", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "across department", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "across division", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "across function", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "across site", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "across group", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "across entity", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "across bus", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "across business units", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "across teams", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "across departments", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "across divisions", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "across functions", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "across sites", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "across groups", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "across entities", WithinWords = 3 }); 
		//         notInBatch = new SearchBatch();
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "move across", WithinWords = 0 });
		//         orBatch.NotInWords.Add(notInBatch);
		//         batch.OrSubSearchBatch.Add(orBatch);



		//         orBatch = new SearchBatch();
		//         orBatch.BatchSearchWords.Add(new SearchWord() { Word = "avoid isolation", WithinWords = 0 });
		//         orBatch.BatchSearchWords.Add(new SearchWord() { Word = "creates isolation", WithinWords = 0 });
		//         orBatch.BatchSearchWords.Add(new SearchWord() { Word = "working in isolation", WithinWords = 0 });
		//         orBatch.BatchSearchWords.Add(new SearchWord() { Word = "isolated from", WithinWords = 0 });
		//         var ororBatch = new SearchBatch();
		//         ororBatch.BatchSearchWords.Add(new SearchWord() { Word = "feel", WithinWords = 0 });
		//         var orAndBatch = new SearchBatch();
		//         orAndBatch.BatchSearchWords.Add(new SearchWord() { Word = "isolated", WithinWords = 0 });
		//         ororBatch.AndMustMatchWords.Add(orAndBatch);
		//         orBatch.OrSubSearchBatch.Add(ororBatch);
		//         notInBatch = new SearchBatch();
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "isolated issues", WithinWords = 0 });
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "isolated incident", WithinWords = 0 });
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "single isolated department", WithinWords = 0 });
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "isolate the sounds", WithinWords = 0 });
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "isolated place", WithinWords = 0 });
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "Looking isolated at", WithinWords = 0 });
		//         orBatch.NotInWords.Add(notInBatch);
		//         batch.OrSubSearchBatch.Add(orBatch);



		//         orBatch = new SearchBatch();
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "improve relations", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "improvement relations", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "improved relations", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "relations improving", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "increase relations", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "increased relations", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "encourage relations", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "enhance relations", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "develop relations", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "greater relations", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "good relations", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "better relations", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "proper relations", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "stronger relations", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "open relations", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "global relations", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "bilateral relations", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "effective relations", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "poor relations", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "lack relations", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "lacking relations", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "weak relations", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "inadequate relations", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "little relations", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "employees relations", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "employee relations", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "relations between", WithinWords = 0 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "problem relations", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "problems relations", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "topdown relations", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "relations bu", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "relations business unit", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "relations team", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "relations department", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "relations division", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "relations group", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "relations entity", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "relations bus", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "relations business units", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "relations teams", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "relations departments", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "relations divisions", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "relations functions", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "relations groups", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "relations entities", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "relations staff", WithinWords = 3 }); 
		//         notInBatch = new SearchBatch();
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "public relations department", WithinWords = 0 });
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "better family relations", WithinWords = 0 });
		//         orBatch.NotInWords.Add(notInBatch);
		//         batch.OrSubSearchBatch.Add(orBatch);


		//         orBatch = new SearchBatch();
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "no help from", WithinWords = 0 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "no support from", WithinWords = 0 }); 
		//         notInBatch = new SearchBatch();
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "manager", WithinWords = 0 });
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "management", WithinWords = 0 });
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "leader", WithinWords = 0 });
		//         orBatch.NotInWords.Add(notInBatch);
		//         batch.OrSubSearchBatch.Add(orBatch);


		//         orBatch = new SearchBatch();
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "my team", WithinWords = 0 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "great team", WithinWords = 0 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "good team", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "fantastic team", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "committed team", WithinWords = 5 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "team driven", WithinWords = 0 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "more team", WithinWords = 0 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "grow team", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "strong team", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "team player", WithinWords = 2 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "amazing team", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "knowledgeable team", WithinWords = 3 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "our team", WithinWords = 0 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "positive team", WithinWords = 0 });
		//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "negative team", WithinWords = 0 }); 
		//         notInBatch = new SearchBatch();
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "our team leader", WithinWords = 0 });
		//         orBatch.NotInWords.Add(notInBatch);
		//         batch.OrSubSearchBatch.Add(orBatch);




		//         cat.Batches.Add(batch);
		//         return cat;
		//     }
	}
}
