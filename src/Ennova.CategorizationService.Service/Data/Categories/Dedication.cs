﻿using Ennova.CategorizationService.Service.Model;

namespace Ennova.CategorizationService.Service.Data.Categories
{
    public class Dedication
    {
        public Category GetCategory()
        {
            var cat = new Category();
            cat.Name = "Dedication";
            cat.Id = "c119548";
            cat.MinimumMatches = 2;

            var batch = new SearchBatch();

            string[] wordWithInWordsZero =
            {
                "my commitment", "our commitment", "committed employee", "committed employees", "work hard",
                "hard work", "hard working", "hardworking", "hard worker", "hard workers", "we put in ", "we put into",
                "dedication * ", "dedicating", "enthusi * ", "passion * ", "devoted his", "devoted their",
                "devoted themselves", "devote ourselves"
            };
            string[] wordWithInWordsTwo =
            {
                "commit lego", "commit company", "commit team", "commit job", "commit work", "my engagement",
                "employee engagement", "employees engagement", "try hard", "tries hard", "tried hard", "trying hard"
            };

            string[] wordWithInWordsThree =
            {
                "i commitment", "i committed", "i commit", "commitment lego", "commitment company", "commitment team",
                "commitment job", "commitment work", "commitments lego", "commitments company", "commitments team",
                "commitments job", "commitments work", "committed lego", "committed company", "committed team",
                "committed job", "committed work", "willing work", "devote time", "devote work", "devote company",
                "contribute to company", "contributes to company", "contributing to company",
                "contributings to company", "contribution to company", "contributions to company", "bring value to",
                "brings value to", "brought value to", "bringing value to"
            };
            string[] wordWithInWordsFour =
            {
                "dedicate job", "dedicated job", "dedication job", "dedicated employees", "dedicated employee",
                "dedicate work", "dedicated work", "dedication work", "dedicate company", "dedicated company",
                "dedication company", "dedicate i", "dedicated i", "dedication i"
            };

            string[] wordWithInWordsFive =
            {
                "put in effort", "put in efforts", "put in time", "put into effort", "put into efforts", "put into time"
            };

            batch.AddRangeSearchWords(wordWithInWordsZero, 0);
            batch.AddRangeSearchWords(wordWithInWordsTwo, 2);
            batch.AddRangeSearchWords(wordWithInWordsThree, 3);
            batch.AddRangeSearchWords(wordWithInWordsFour, 4);
            batch.AddRangeSearchWords(wordWithInWordsFive, 5);

            var orBatch = new SearchBatch();

            string[] orWordWithInWordsTwo =
            {
                "make effort", "makes effort", "making effort", "made effort", "make efforts", "makes efforts", "making efforts", "made efforts"
            };

            orBatch.AddRangeSearchWords(orWordWithInWordsTwo, 2);


            var notInBatch = new SearchBatch();
            notInBatch.AddSearchWord("decision-making", 0 );
            orBatch.NotInWords.Add(notInBatch);
            batch.OrSubSearchBatch.Add(orBatch);

            orBatch = new SearchBatch();

            string[] orWordWithInWordsFive =
            {
                "employees contributed","employee contributed", "weve contributed", "we contributed", "they contributed", "team contributed", "staff contributed",
                "colleagues contributed", "people contributed", "I contributed"
            };

            orBatch.AddRangeSearchWords(orWordWithInWordsFive, 5);


            notInBatch = new SearchBatch();
            notInBatch.AddSearchWord("contributed to the results", 0);
            notInBatch.AddSearchWord("contributed in staff feeling", 0);
            orBatch.NotInWords.Add(notInBatch);
            batch.OrSubSearchBatch.Add(orBatch);


            cat.Batches.Add(batch);
            return cat;

        }
    }
    //     public Category GetCategory()
    //     {
    //         var cat = new Category();
    //         cat.Name = "Dedication";
    //         cat.Id = "c119548";
    //         cat.MinimumMatches = 2;

    //var batch = new SearchBatch();

    //batch.BatchSearchWords.Add(new SearchWord() { Word = "my commitment", WithinWords = 0 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "our commitment", WithinWords = 0 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "i commitment", WithinWords = 3 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "i committed", WithinWords = 3 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "i commit", WithinWords = 3 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "commitment lego", WithinWords = 3 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "commitment company", WithinWords = 3 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "commitment team", WithinWords = 3 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "commitment job", WithinWords = 3 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "commitment work", WithinWords = 3 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "commitments lego", WithinWords = 3 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "commitments company", WithinWords = 3 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "commitments team", WithinWords = 3 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "commitments job", WithinWords = 3 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "commitments work", WithinWords = 3 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "commit lego", WithinWords = 2 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "commit company", WithinWords = 2 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "commit team", WithinWords = 2 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "commit job", WithinWords = 2 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "commit work", WithinWords = 2 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "committed lego", WithinWords = 3 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "committed company", WithinWords = 3 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "committed team", WithinWords = 3 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "committed job", WithinWords = 3 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "committed work", WithinWords = 3 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "committed employee", WithinWords = 0 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "committed employees", WithinWords = 0 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "my engagement", WithinWords = 2 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "employee engagement", WithinWords = 2 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "employees engagement", WithinWords = 2 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "dedicate job", WithinWords = 4 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "dedicated job", WithinWords = 4 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "dedication job", WithinWords = 4 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "dedicated employees", WithinWords = 4 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "dedicated employee", WithinWords = 4 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "dedicate work", WithinWords = 4 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "dedicated work", WithinWords = 4 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "dedication work", WithinWords = 4 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "dedicate company", WithinWords = 4 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "dedicated company", WithinWords = 4 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "dedication company", WithinWords = 4 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "dedicate i", WithinWords = 4 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "dedicated i", WithinWords = 4 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "dedication i", WithinWords = 4 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "willing work", WithinWords = 3 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "work hard", WithinWords = 0 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "hard work", WithinWords = 0 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "hard working", WithinWords = 0 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "hardworking", WithinWords = 0 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "hard worker", WithinWords = 0 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "hard workers", WithinWords = 0 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "put in effort", WithinWords = 5 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "put in efforts", WithinWords = 5 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "put in time", WithinWords = 5 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "put into effort", WithinWords = 5 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "put into efforts", WithinWords = 5 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "put into time", WithinWords = 5 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "we put in", WithinWords = 0 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "we put into", WithinWords = 0 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "dedication*", WithinWords = 0 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "dedicating", WithinWords = 0 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "try hard", WithinWords = 2 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "tries hard", WithinWords = 2 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "tried hard", WithinWords = 2 });
    //batch.BatchSearchWords.Add(new SearchWord() { Word = "trying hard", WithinWords = 2 });
    //         batch.BatchSearchWords.Add(new SearchWord() { Word = "enthusi*", WithinWords = 0 });
    //         batch.BatchSearchWords.Add(new SearchWord() { Word = "passion*", WithinWords = 0 });
    //         batch.BatchSearchWords.Add(new SearchWord() { Word = "devoted his", WithinWords = 0 });
    //         batch.BatchSearchWords.Add(new SearchWord() { Word = "devoted their", WithinWords = 0 });
    //         batch.BatchSearchWords.Add(new SearchWord() { Word = "devoted themselves", WithinWords = 0 });
    //         batch.BatchSearchWords.Add(new SearchWord() { Word = "devote ourselves", WithinWords = 0 });
    //         batch.BatchSearchWords.Add(new SearchWord() { Word = "devote time", WithinWords = 3 });
    //         batch.BatchSearchWords.Add(new SearchWord() { Word = "devote work", WithinWords = 3 });
    //         batch.BatchSearchWords.Add(new SearchWord() { Word = "devote company", WithinWords = 3 });
    //         batch.BatchSearchWords.Add(new SearchWord() { Word = "contribute to company", WithinWords = 3 });
    //         batch.BatchSearchWords.Add(new SearchWord() { Word = "contributes to company", WithinWords = 3 });
    //         batch.BatchSearchWords.Add(new SearchWord() { Word = "contributing to company", WithinWords = 3 });
    //         batch.BatchSearchWords.Add(new SearchWord() { Word = "contributings to company", WithinWords = 3 });
    //         batch.BatchSearchWords.Add(new SearchWord() { Word = "contribution to company", WithinWords = 3 });
    //         batch.BatchSearchWords.Add(new SearchWord() { Word = "contributions to company", WithinWords = 3 });
    //         batch.BatchSearchWords.Add(new SearchWord() { Word = "bring value to", WithinWords = 3 });
    //         batch.BatchSearchWords.Add(new SearchWord() { Word = "brings value to", WithinWords = 3 });
    //         batch.BatchSearchWords.Add(new SearchWord() { Word = "brought value to", WithinWords = 3 });
    //         batch.BatchSearchWords.Add(new SearchWord() { Word = "bringing value to", WithinWords = 3 });

    //         var orBatch = new SearchBatch();
    //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "make effort", WithinWords = 2 });
    //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "makes effort", WithinWords = 2 });
    //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "making effort", WithinWords = 2 });
    //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "made effort", WithinWords = 2 });
    //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "make efforts", WithinWords = 2 });
    //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "makes efforts", WithinWords = 2 });
    //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "making efforts", WithinWords = 2 });
    //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "made efforts", WithinWords = 2 }); 
    //         var notInBatch = new SearchBatch();
    //notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "decision-making", WithinWords = 0 });
    //         orBatch.NotInWords.Add(notInBatch);
    //         batch.OrSubSearchBatch.Add(orBatch);

    //         orBatch = new SearchBatch();
    //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "employees contributed", WithinWords = 5 });
    //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "employee contributed", WithinWords = 5 });
    //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "weve contributed", WithinWords = 5 });
    //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "we contributed", WithinWords = 5 });
    //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "they contributed", WithinWords = 5 });
    //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "team contributed", WithinWords = 5 });
    //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "staff contributed", WithinWords = 5 });
    //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "colleagues contributed", WithinWords = 5 });
    //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "people contributed", WithinWords = 5 });
    //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "I contributed", WithinWords = 5 }); 

    //         notInBatch = new SearchBatch();
    //notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "contributed to the results", WithinWords = 0 });
    //notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "contributed in staff feeling", WithinWords = 0 });
    //         orBatch.NotInWords.Add(notInBatch);
    //         batch.OrSubSearchBatch.Add(orBatch);


    //         cat.Batches.Add(batch);
    //         return cat;

    //     }
}


