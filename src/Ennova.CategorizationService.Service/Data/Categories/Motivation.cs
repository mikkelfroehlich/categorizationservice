﻿using Ennova.CategorizationService.Service.Model;

namespace Ennova.CategorizationService.Service.Data.Categories
{
    public class Motivation
    {
        public Category GetCategory()
        {
            var cat = new Category();
            cat.Name = "Motivation";
            cat.Id = "c119545";
            cat.MinimumMatches = 2;

            var batch = new SearchBatch();

            string[] wordWithInWordsZero =
            {
                "motivation*", "motivating", "motivate", "motivates", "motivated", "demotivation*", "demotivating",
                "demotivate", "demotivates", "demotivated", "*motivat*", "encouragement*", "encourages", "encouraging",
                "inspire", "inspires", "inspiring", "inspired", "inspiration*", "reassure", "reassures", "reassuring",
                "reassured", "reassurement*", "morale", "moral", "spirit"
            };


            batch.AddSearchWord("motivation", 1);
            batch.AddRangeSearchWords(wordWithInWordsZero, 0);


            var orBatch = new SearchBatch();
            orBatch.AddSearchWord("encourage", 0);
            var notInBatch = new SearchBatch();
            notInBatch.AddSearchWord("i encourage", 3);
            orBatch.NotInWords.Add(notInBatch);
            batch.OrSubSearchBatch.Add(orBatch);


            orBatch = new SearchBatch();
            orBatch.AddSearchWord("encouraged", 0);
            notInBatch = new SearchBatch();
            notInBatch.AddSearchWord("little girls are encouraged", 0);
            orBatch.NotInWords.Add(notInBatch);
            batch.OrSubSearchBatch.Add(orBatch);


            orBatch = new SearchBatch();
            string[] orWordWithInWordsZero =
            {
                "engage", "engages", "engaging", "engaged", "engagement"
            };

            orBatch.AddRangeSearchWords(orWordWithInWordsZero, 0);


            notInBatch = new SearchBatch();
            string[] notInWordWithInWordsTwo =
            {
                "engage customer", "engages customer", "engaging customer", "engaged customer", "engage customers", "engages customers", "engaging customers", "engaged customer"
            };

            notInBatch.AddRangeSearchWords(notInWordWithInWordsTwo, 2);

            orBatch.NotInWords.Add(notInBatch);
            batch.OrSubSearchBatch.Add(orBatch);


            cat.Batches.Add(batch);
            return cat;

        }


        //     public Category GetCategory()
        //     {
        //         var cat = new Category();
        //         cat.Name = "Motivation";
        //         cat.Id = "c119545";
        //         cat.MinimumMatches = 2;

        //         var batch = new SearchBatch();
        //         batch.BatchSearchWords.Add(new SearchWord() { Word = "motivation*", WithinWords = 0 });
        //batch.BatchSearchWords.Add(new SearchWord() { Word = "motivation", WithinWords = 1 });
        //batch.BatchSearchWords.Add(new SearchWord() { Word = "motivating", WithinWords = 0 });
        //batch.BatchSearchWords.Add(new SearchWord() { Word = "motivate", WithinWords = 0 });
        //batch.BatchSearchWords.Add(new SearchWord() { Word = "motivates", WithinWords = 0 });
        //batch.BatchSearchWords.Add(new SearchWord() { Word = "motivated", WithinWords = 0 });
        //batch.BatchSearchWords.Add(new SearchWord() { Word = "demotivation*", WithinWords = 0 });
        //batch.BatchSearchWords.Add(new SearchWord() { Word = "demotivating", WithinWords = 0 });
        //batch.BatchSearchWords.Add(new SearchWord() { Word = "demotivate", WithinWords = 0 });
        //batch.BatchSearchWords.Add(new SearchWord() { Word = "demotivates", WithinWords = 0 });
        //batch.BatchSearchWords.Add(new SearchWord() { Word = "demotivated", WithinWords = 0 });
        //batch.BatchSearchWords.Add(new SearchWord() { Word = "*motivat*", WithinWords = 0 });
        //batch.BatchSearchWords.Add(new SearchWord() { Word = "encouragement*", WithinWords = 0 });
        //         batch.BatchSearchWords.Add(new SearchWord() { Word = "encourages", WithinWords = 0 });
        //         batch.BatchSearchWords.Add(new SearchWord() { Word = "encouraging", WithinWords = 0 });
        //         batch.BatchSearchWords.Add(new SearchWord() { Word = "inspire", WithinWords = 0 });
        //         batch.BatchSearchWords.Add(new SearchWord() { Word = "inspires", WithinWords = 0 });
        //         batch.BatchSearchWords.Add(new SearchWord() { Word = "inspiring", WithinWords = 0 });
        //         batch.BatchSearchWords.Add(new SearchWord() { Word = "inspired", WithinWords = 0 });
        //         batch.BatchSearchWords.Add(new SearchWord() { Word = "inspiration*", WithinWords = 0 });
        //         batch.BatchSearchWords.Add(new SearchWord() { Word = "reassure", WithinWords = 0 });
        //         batch.BatchSearchWords.Add(new SearchWord() { Word = "reassures", WithinWords = 0 });
        //         batch.BatchSearchWords.Add(new SearchWord() { Word = "reassuring", WithinWords = 0 });
        //         batch.BatchSearchWords.Add(new SearchWord() { Word = "reassured", WithinWords = 0 });
        //         batch.BatchSearchWords.Add(new SearchWord() { Word = "reassurement*", WithinWords = 0 });
        //         batch.BatchSearchWords.Add(new SearchWord() { Word = "morale", WithinWords = 0 });
        //         batch.BatchSearchWords.Add(new SearchWord() { Word = "moral", WithinWords = 0 });
        //         batch.BatchSearchWords.Add(new SearchWord() { Word = "spirit", WithinWords = 0 });

        //var orBatch = new SearchBatch();
        //         orBatch.BatchSearchWords.Add(new SearchWord() { Word = "encourage", WithinWords = 0 });
        //         var notInBatch = new SearchBatch();
        // 		notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "i encourage", WithinWords = 3 });
        //orBatch.NotInWords.Add(notInBatch);
        //         batch.OrSubSearchBatch.Add(orBatch);


        //         orBatch = new SearchBatch();
        //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "encouraged", WithinWords = 0 });
        //         notInBatch = new SearchBatch();
        // 		notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "little girls are encouraged", WithinWords = 0 });
        //         orBatch.NotInWords.Add(notInBatch);
        //         batch.OrSubSearchBatch.Add(orBatch);


        //         orBatch = new SearchBatch();
        //         orBatch.BatchSearchWords.Add(new SearchWord() { Word = "engage", WithinWords = 0 });
        //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "engages", WithinWords = 0 });
        //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "engaging", WithinWords = 0 });
        //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "engaged", WithinWords = 0 });
        //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "engagement", WithinWords = 0 });
        //         notInBatch = new SearchBatch();
        //notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "engage customer", WithinWords = 2 });
        //notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "engages customer", WithinWords = 2 });
        //notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "engaging customer", WithinWords = 2 });
        //notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "engaged customer", WithinWords = 2 });
        //notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "engage customers", WithinWords = 2 });
        //notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "engages customers", WithinWords = 2 });
        //notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "engaging customers", WithinWords = 2 });
        //notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "engaged customer", WithinWords = 2 });
        //         orBatch.NotInWords.Add(notInBatch);
        //         batch.OrSubSearchBatch.Add(orBatch);


        //         cat.Batches.Add(batch);
        //         return cat;

        //     }
    }
}
