﻿using Ennova.CategorizationService.Service.Model;

namespace Ennova.CategorizationService.Service.Data.Categories
{
    public class ImmediateManager
    {
        public Category GetCategory()
        {
            var cat = new Category();
            cat.Name = "ImmediateManager";
            cat.Id = "c119537";
            cat.MinimumMatches = 2;

			var batch = new SearchBatch();

            string[] wordWithInWordsZero =
            {
                "leader", "leaders", "manager", "managers", "mgmt", "management", "managements", "director",
                "directors", "head", "boss", "bosses", "supervisor", "supervisors"
            };


            var orbatch = new SearchBatch();
			orbatch.AddSearchWord("superior",  0 );
			orbatch.AddSearchWord("superiors", 0 );

            var notbatch = new SearchBatch();
            notbatch.AddSearchWord("superior products", 0);
            notbatch.AddSearchWord("superior product",  0);
            orbatch.NotInWords.Add(notbatch);
            batch.OrSubSearchBatch.Add(orbatch);

            var notinbatch = new SearchBatch();

            string[] noInWordWithInWordsZero =
            {
                "senior leader", "senior leaders", "senior officials", "senior managers", "senior manager",
                "senior mgmt", "senior managements", "senior management", "sr leader", "sr leaders", "sr officials",
                "sr managers", "sr manager", "sr mgmt", "sr managements", "sr management", "upper leader",
                "upper leaders", "upper officials", "upper managers", "upper manager", "upper mgmt",
                "upper managements", "upper management", "top leader", "top leaders", "top officials", "top managers",
                "top manager", "top mgmt", "top managements", "top management", "corporate leader", "corporate leaders",
                "corporate officials", "corporate managers", "corporate manager", "corporate mgmt",
                "corporate managements", "corporate management", "corp leader", "corp leaders", "corp officials",
                "corp managers", "corp manager", "corp mgmt", "corp managements", "corp management", "higher leader",
                "higher leaders", "higher officials", "higher managers", "higher manager", "higher mgmt",
                "higher managements", "higher management", "company leader", "company leaders", "company officials",
                "company managers", "company manager", "company mgmt", "company managements", "company management",
                "exec", "vp", "vice president", "svp", "evp", "senior level", "executive*", "C?O", "CM"
            };
            notinbatch.AddRangeSearchWords(noInWordWithInWordsZero, 0);

			notbatch = new SearchBatch();

            string[] notWordWithInWordsZero =
            {
                "below vp", "executive capabilities", "below director", "area director", "regional director", "department director"
            };

			notbatch.AddRangeSearchWords(notWordWithInWordsZero,0);

			notinbatch.NotInWords.Add(notbatch);
            batch.NotInWords.Add(notinbatch);

			cat.Batches.Add(batch);
            return cat;

        }
        
   //     public Category GetCategory()
   //     {
   //         var cat = new Category();
   //         cat.Name = "ImmediateManager";
   //         cat.Id = "c119537";
   //         cat.MinimumMatches = 2;

			//var batch = new SearchBatch();
   //         batch.BatchSearchWords.Add(new SearchWord() { Word = "leader", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "leaders", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "manager", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "managers", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "mgmt", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "management", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "managements", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "director", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "directors", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "head", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "boss", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "bosses", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "supervisor", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "supervisors", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "", WithinWords = 0 });


   //         var orbatch = new SearchBatch();
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "superior", WithinWords = 0 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "superiors", WithinWords = 0 });

   //         var notbatch = new SearchBatch();
   //         notbatch.BatchSearchWords.Add(new SearchWord() { Word = "superior products", WithinWords = 0 });
   //         notbatch.BatchSearchWords.Add(new SearchWord() { Word = "superior product", WithinWords = 0 });
   //         orbatch.NotInWords.Add(notbatch);
   //         batch.OrSubSearchBatch.Add(orbatch);

   //         var notinbatch = new SearchBatch();
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "senior leader", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "senior leaders", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "senior officials", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "senior managers", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "senior manager", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "senior mgmt", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "senior managements", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "senior management", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "sr leader", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "sr leaders", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "sr officials", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "sr managers", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "sr manager", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "sr mgmt", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "sr managements", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "sr management", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "upper leader", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "upper leaders", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "upper officials", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "upper managers", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "upper manager", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "upper mgmt", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "upper managements", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "upper management", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "top leader", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "top leaders", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "top officials", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "top managers", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "top manager", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "top mgmt", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "top managements", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "top management", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "corporate leader", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "corporate leaders", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "corporate officials", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "corporate managers", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "corporate manager", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "corporate mgmt", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "corporate managements", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "corporate management", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "corp leader", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "corp leaders", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "corp officials", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "corp managers", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "corp manager", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "corp mgmt", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "corp managements", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "corp management", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "higher leader", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "higher leaders", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "higher officials", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "higher managers", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "higher manager", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "higher mgmt", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "higher managements", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "higher management", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "company leader", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "company leaders", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "company officials", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "company managers", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "company manager", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "company mgmt", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "company managements", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "company management", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "exec", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "vp", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "vice president", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "svp", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "evp", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "senior level", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "executive*", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "C?O", WithinWords = 0 });
			//notinbatch.BatchSearchWords.Add(new SearchWord() { Word = "CM", WithinWords = 0 });

			//notbatch = new SearchBatch();
   //         notbatch.BatchSearchWords.Add(new SearchWord() { Word = "below vp", WithinWords = 0 });
   //         notbatch.BatchSearchWords.Add(new SearchWord() { Word = "executive capabilities", WithinWords = 0 });
   //         notbatch.BatchSearchWords.Add(new SearchWord() { Word = "below director", WithinWords = 0 });
   //         notbatch.BatchSearchWords.Add(new SearchWord() { Word = "area director", WithinWords = 0 });
   //         notbatch.BatchSearchWords.Add(new SearchWord() { Word = "regional director", WithinWords = 0 });
   //         notbatch.BatchSearchWords.Add(new SearchWord() { Word = "department director", WithinWords = 0 });
   //         notinbatch.NotInWords.Add(notbatch);
   //         batch.NotInWords.Add(notinbatch);

			//cat.Batches.Add(batch);
   //         return cat;

   //     }
    }
}
