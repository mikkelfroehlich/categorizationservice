﻿using Ennova.CategorizationService.Service.Model;

namespace Ennova.CategorizationService.Service.Data.Categories
{
    public class Nonsense
    {
        public Category GetCategory()
        {
            var cat = new Category();
            cat.Name = "Nonsence";
            cat.Id = "c149341";
            cat.MinimumMatches = 0;
            return cat;
        }
    }
}