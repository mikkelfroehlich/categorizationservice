﻿using Ennova.CategorizationService.Service.Model;

namespace Ennova.CategorizationService.Service.Data.Categories
{
    public class Reputation
    {
        public Category GetCategory()
        {
            var cat = new Category();
			cat.Name = "Reputation";
			cat.Id = "c119535";
			cat.MinimumMatches = 2;

			var batch = new SearchBatch();
			string[] wordWithInZeroWords =
			{
				"proud", "honor", "renown", "renowned", "well known", "notor*", "recognized name", "household name", "our name", "respected name", "brand name",
				"name speaks", "publicity", "advertising", "advertisement", "quality product", "quality products", "work for", "privilege to work", "reputed name"
			};
            string[] wordWithInTwoWords = { "good name", "bad name", "name brand", "name reputations" };

			batch.AddRangeSearchWords(wordWithInZeroWords, 0);
            batch.AddRangeSearchWords(wordWithInTwoWords, 2);
			batch.AddSearchWord("privilege be part", 5);
			
			var orBatch = new SearchBatch();
			string[] orWordWithInZeroWords =
			{
				"image", "images", "reputation*", "reputab*", "prestig*", "brand", "brands", "branding", "fame", "famous"
			};
			orBatch.AddRangeSearchWords(orWordWithInZeroWords, 0);

			var notInBatch = new SearchBatch();
			string[] notInWordZero =
			{
				"personal reputation", "my reputation", "his reputation", "own reputation", "brand retail", "brand new"
			};

			notInBatch.AddRangeSearchWords(notInWordZero, 0);
			notInBatch.AddSearchWord("brand team", 1);
			orBatch.NotInWords.Add(notInBatch);
			
            batch.OrSubSearchBatch.Add(orBatch);
			
            orBatch = new SearchBatch();
			orBatch.AddSearchWord("pride", 0);

			notInBatch = new SearchBatch();
			notInBatch.AddSearchWord("his pride", 0);
			orBatch.NotInWords.Add(notInBatch);
			
            batch.OrSubSearchBatch.Add(orBatch);

			orBatch = new SearchBatch();
			orBatch.AddSearchWord("known", 0);
			orBatch.AddSearchWord("respected", 0);
			var andBatch = new SearchBatch();

			string[] andWordWithInZeroWords =
			{
				"company", "organization", "organisation", "name", "companies", "organizations", "organisations", "names"
			};

			andBatch.AddRangeSearchWords(andWordWithInZeroWords, 0);
			orBatch.AndMustMatchWords.Add(andBatch);
			notInBatch = new SearchBatch();

			string[] notInWordWithInZeroWords =
			{
				"managers","manager", "mgmt","managements", "officials", "official", "officer", "officers", "director", "directors", "head", "boss", "bosses", "supervisor",
				"supervisors", "exec", "president", "vp", "vice president", "svp", "evp", "senior level", "executive*", "C?O", "CMOR strateg*", "known markets"
			};
			notInBatch.AddRangeSearchWords(notInWordWithInZeroWords, 0);
			notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "management", WithinWords = 1 });

			orBatch.NotInWords.Add(notInBatch);
			batch.OrSubSearchBatch.Add(orBatch);


			orBatch = new SearchBatch();
			orBatch.AddSearchWord("company name", 3);
			notInBatch = new SearchBatch();
			notInBatch.AddSearchWord("change the company name", 0);
			orBatch.NotInWords.Add(notInBatch);
			batch.OrSubSearchBatch.Add(orBatch);

			orBatch = new SearchBatch();
			
			string[] orWordWithInTwoWords =
			{
				"good company", "bad company", "recognized company", "quality company", "trustworthy company", "green company", "prestigious company",
				"renown company", "reputed company", "respected company", "household company", "well known company", "strong company", "innovative company",
				"inovative company", "recognised company"
			};
            orBatch.AddSearchWord("solid company", 0);
			orBatch.AddRangeSearchWords(orWordWithInTwoWords, 2);


			notInBatch = new SearchBatch();
			notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "good for company", WithinWords = 2 });
			orBatch.NotInWords.Add(notInBatch);
			batch.OrSubSearchBatch.Add(orBatch);


			cat.Batches.Add(batch);
			return cat;

		}

        //public Category GetCategory()
        //{
            //         var cat = new Category();
            //         cat.Name = "Reputation";
            //         cat.Id = "c119535";
            //         cat.MinimumMatches = 2;

            //var batch = new SearchBatch();
            //batch.BatchSearchWords.Add(new SearchWord() { Word = "proud", WithinWords = 0 });
            //batch.BatchSearchWords.Add(new SearchWord() { Word = "honor", WithinWords = 0 });
            //batch.BatchSearchWords.Add(new SearchWord() { Word = "renown", WithinWords = 0 });
            //batch.BatchSearchWords.Add(new SearchWord() { Word = "renowned", WithinWords = 0 });
            //batch.BatchSearchWords.Add(new SearchWord() { Word = "well known", WithinWords = 0 });
            //batch.BatchSearchWords.Add(new SearchWord() { Word = "notor*", WithinWords = 0 });
            //batch.BatchSearchWords.Add(new SearchWord() { Word = "good name", WithinWords = 2 });
            //batch.BatchSearchWords.Add(new SearchWord() { Word = "bad name", WithinWords = 2 });
            //batch.BatchSearchWords.Add(new SearchWord() { Word = "recognized name", WithinWords = 0 });
            //batch.BatchSearchWords.Add(new SearchWord() { Word = "household name", WithinWords = 0 });
            //batch.BatchSearchWords.Add(new SearchWord() { Word = "our name", WithinWords = 0 });
            //batch.BatchSearchWords.Add(new SearchWord() { Word = "name brand", WithinWords = 2 });
            //batch.BatchSearchWords.Add(new SearchWord() { Word = "name reputations", WithinWords = 2 });
            //batch.BatchSearchWords.Add(new SearchWord() { Word = "excellent name", WithinWords = 2 });
            //batch.BatchSearchWords.Add(new SearchWord() { Word = "reputed name", WithinWords = 0 });
            //batch.BatchSearchWords.Add(new SearchWord() { Word = "respected name", WithinWords = 0 });
            //batch.BatchSearchWords.Add(new SearchWord() { Word = "brand name", WithinWords = 0 });
            //batch.BatchSearchWords.Add(new SearchWord() { Word = "name speaks", WithinWords = 0 });
            //batch.BatchSearchWords.Add(new SearchWord() { Word = "publicity", WithinWords = 0 });
            //batch.BatchSearchWords.Add(new SearchWord() { Word = "advertising", WithinWords = 0 });
            //batch.BatchSearchWords.Add(new SearchWord() { Word = "advertisement", WithinWords = 0 });
            //batch.BatchSearchWords.Add(new SearchWord() { Word = "quality product", WithinWords = 0 });
            //batch.BatchSearchWords.Add(new SearchWord() { Word = "quality products", WithinWords = 0 });
            //batch.BatchSearchWords.Add(new SearchWord() { Word = "work for", WithinWords = 0 });
            //batch.BatchSearchWords.Add(new SearchWord() { Word = "privilege to work", WithinWords = 0 });
            //batch.BatchSearchWords.Add(new SearchWord() { Word = "privilege be part", WithinWords = 5 });

            //         var orBatch = new SearchBatch();
            //         orBatch.BatchSearchWords.Add(new SearchWord() { Word = "image", WithinWords = 0 });
            //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "images", WithinWords = 0 });
            //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "reputation*", WithinWords = 0 });
            //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "reputab*", WithinWords = 0 });
            //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "prestig*", WithinWords = 0 });
            //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "brand", WithinWords = 0 });
            //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "brands", WithinWords = 0 });
            //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "branding", WithinWords = 0 });
            //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "fame", WithinWords = 0 });
            //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "famous", WithinWords = 0 }); 
            //         var notInBatch = new SearchBatch();
            //notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "personal reputation", WithinWords = 0 });
            //notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "my reputation", WithinWords = 0 });
            //notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "his reputation", WithinWords = 0 });
            //notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "own reputation", WithinWords = 0 });
            //notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "brand team", WithinWords = 1 });
            //notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "brand retail", WithinWords = 0 });
            //notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "brand new", WithinWords = 0 });
            //         orBatch.NotInWords.Add(notInBatch);
            //         batch.OrSubSearchBatch.Add(orBatch);


            //         orBatch = new SearchBatch();
            //         orBatch.BatchSearchWords.Add(new SearchWord() { Word = "pride", WithinWords = 0 });
            //         notInBatch = new SearchBatch();
            //   notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "his pride", WithinWords = 0 });
            //         orBatch.NotInWords.Add(notInBatch);
            //         batch.OrSubSearchBatch.Add(orBatch);

            //         orBatch = new SearchBatch();
            //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "known", WithinWords = 0 });
            //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "respected", WithinWords = 0 }); 
            //         var andBatch = new SearchBatch();
            //andBatch.BatchSearchWords.Add(new SearchWord() { Word = "company", WithinWords = 0 });
            //andBatch.BatchSearchWords.Add(new SearchWord() { Word = "organization", WithinWords = 0 });
            //andBatch.BatchSearchWords.Add(new SearchWord() { Word = "organisation", WithinWords = 0 });
            //andBatch.BatchSearchWords.Add(new SearchWord() { Word = "name", WithinWords = 0 });
            //andBatch.BatchSearchWords.Add(new SearchWord() { Word = "companies", WithinWords = 0 });
            //andBatch.BatchSearchWords.Add(new SearchWord() { Word = "organizations", WithinWords = 0 });
            //andBatch.BatchSearchWords.Add(new SearchWord() { Word = "organisations", WithinWords = 0 });
            //andBatch.BatchSearchWords.Add(new SearchWord() { Word = "names", WithinWords = 0 });
            //         orBatch.AndMustMatchWords.Add(andBatch);
            //         notInBatch = new SearchBatch();
            //notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "manager", WithinWords = 0 });
            //notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "managers", WithinWords = 0 });
            //notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "mgmt", WithinWords = 0 });
            //notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "management", WithinWords = 1 });
            //notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "managements", WithinWords = 0 });
            //notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "officials", WithinWords = 0 });
            //notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "official", WithinWords = 0 });
            //notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "officer", WithinWords = 0 });
            //notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "officers", WithinWords = 0 });
            //notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "director", WithinWords = 0 });
            //notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "directors", WithinWords = 0 });
            //notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "head", WithinWords = 0 });
            //notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "boss", WithinWords = 0 });
            //notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "bosses", WithinWords = 0 });
            //notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "supervisor", WithinWords = 0 });
            //notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "supervisors", WithinWords = 0 });
            //notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "exec", WithinWords = 0 });
            //notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "president", WithinWords = 0 });
            //notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "vp", WithinWords = 0 });
            //notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "vice president", WithinWords = 0 });
            //notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "svp", WithinWords = 0 });
            //notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "evp", WithinWords = 0 });
            //notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "senior level", WithinWords = 0 });
            //notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "executive*", WithinWords = 0 });
            //notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "C?O", WithinWords = 0 });
            //notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "CMOR strateg*", WithinWords = 0 });
            //notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "known markets", WithinWords = 0 });
            //         orBatch.NotInWords.Add(notInBatch);
            //         batch.OrSubSearchBatch.Add(orBatch);


            //         orBatch = new SearchBatch();
            // 		orBatch.BatchSearchWords.Add(new SearchWord() { Word = "company name", WithinWords = 3 });
            //         notInBatch = new SearchBatch();
            //  	notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "change the company name", WithinWords = 0 });
            //         orBatch.NotInWords.Add(notInBatch);
            //         batch.OrSubSearchBatch.Add(orBatch);


            //         orBatch = new SearchBatch();
            //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "solid company", WithinWords = 0 });
            //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "good company", WithinWords = 2 });
            //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "bad company", WithinWords = 2 });
            //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "recognized company", WithinWords = 2 });
            //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "quality company", WithinWords = 2 });
            //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "trustworthy company", WithinWords = 2 });
            //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "green company", WithinWords = 2 });
            //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "prestigious company", WithinWords = 2 });
            //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "renown company", WithinWords = 2 });
            //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "reputed company", WithinWords = 2 });
            //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "respected company", WithinWords = 2 });
            //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "household company", WithinWords = 2 });
            //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "well known company", WithinWords = 2 });
            //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "strong company", WithinWords = 2 });
            //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "innovative company", WithinWords = 2 });
            //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "inovative company", WithinWords = 2 });
            //orBatch.BatchSearchWords.Add(new SearchWord() { Word = "recognised company", WithinWords = 2 }); 
            //         notInBatch = new SearchBatch();
            //notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "good for company", WithinWords = 2 });
            //         orBatch.NotInWords.Add(notInBatch);
            //         batch.OrSubSearchBatch.Add(orBatch);


            //         cat.Batches.Add(batch);
            //return cat;
        //}
    }
}
