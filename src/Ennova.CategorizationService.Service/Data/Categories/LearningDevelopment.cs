﻿using Ennova.CategorizationService.Service.Model;

namespace Ennova.CategorizationService.Service.Data.Categories
{
    public class LearningDevelopment
    {
        public Category GetCategory()
        {
            var cat = new Category();
            cat.Name = "LearningDevelopment";
            cat.Id = "c119542";
            cat.MinimumMatches = 2;

			var batch = new SearchBatch();


			var orBatch = new SearchBatch();

            string[] wordWithInWordsZero =
            {
                "promotions", "promoted", "promotional opportunities", "promotional positions", "promote my self",
                "promoting his", "promote somebody", "promoting top performers", "promotes workers", "potential in me",
                "their potential", "growth potential", "potential to grow", "potential for advancement",
                "potential opportunities", "development potential", "develop potentials", "careers", "move up",
                "moving up", "moved up", "moves up", "move upward", "moving upward", "moved upward", "moves upward",
                "upward growth", "upward movement", "grow within", "grow alongside", "grow in", "grow with",
                "opportunity", "opportunities", "chance", "chances", "possibilities", "possibility", "position opens",
                "positions open", "position opening", "positions opening", "job opens", "jobs open", "job opening",
                "jobs opening", "new challenge", "new challenges", "developmental", "career progression", "mentor*",
                "training", "trainings", "train", "trained", "learning", "learnings", "learn", "learned", "courses",
                "guidance", "guidances", "guideline", "guidelines", "tutorial", "tutorials", "manuals", "educate",
                "educates", "educating", "coached", "coachings", "coaching", "constructive criticism",
                "individual evaluations", "employee evaluations", "staff evaluations", "performance evaluations",
                "status evaluations", "performance evaluation"
            };

            string[] wordWithInWordsTwo =
            {
                "promote internally", "promote internal", "promote them", "promote him", "promote her", "promote you",
                "promote within", "promote staff", "promote some one", "promote those", "promote potential",
                "promote personnel", "promote employees", "promote employee", "promote people", "promote person",
                "promotes internally", "promotes internal", "promotes them", "promotes him", "promotes her",
                "promotes you", "promotes within", "promotes staff", "promotes some one", "promotes those",
                "promotes potential", "promotes personnel", "promotes employees", "promotes employee",
                "promotes people", "promotes person", "promoting internally", "promoting internal", "promoting them",
                "promoting him", "promoting her", "promoting you", "promoting within", "promoting staff",
                "promoting some one", "promoting those", "promoting potential", "promoting personnel",
                "promoting employees", "promoting employee", "promoting people", "promoting person", "promote leaders",
                "promote individuals", "my potential", "position opened", "positions opened", "job opened",
                "jobs opened"
            };

            string[] wordWithInWordsThree =
            {
                "promote position", "potentials employees", "potential employees", "upward company", "progress job",
                "my future", "individual progress", "improve myself", "better myself", "focus people", "future progress"
            };

			batch.AddRangeSearchWords(wordWithInWordsZero,0);
			batch.AddSearchWord("promote the ones",  1);
			batch.AddSearchWord("promoting associates",  1);
            batch.AddRangeSearchWords(wordWithInWordsTwo, 2);
            batch.AddRangeSearchWords(wordWithInWordsThree, 3);
            batch.AddSearchWord("mistake learn", 5);
            batch.AddSearchWord("mistake help", 5);

            orBatch = new SearchBatch();
			orBatch.AddSearchWord("promotion", 0 );
            var notInBatch = new SearchBatch();
			notInBatch.AddSearchWord("promotion company",2 );
			notInBatch.AddSearchWord("promotion products", 2 );
			notInBatch.AddSearchWord("promotion customer", 3 );
            orBatch.NotInWords.Add(notInBatch);
            batch.OrSubSearchBatch.Add(orBatch);

            orBatch = new SearchBatch();
		    orBatch.AddSearchWord("career", 0 );
            notInBatch = new SearchBatch();
		    notInBatch.AddSearchWord("in career", 1 );
            orBatch.NotInWords.Add(notInBatch);
            batch.OrSubSearchBatch.Add(orBatch);

            orBatch = new SearchBatch();
    		orBatch.AddSearchWord("upward mobility", 0);
            notInBatch = new SearchBatch();
		    notInBatch.AddSearchWord("mobility in this department",  0 );
            orBatch.NotInWords.Add(notInBatch);
            batch.OrSubSearchBatch.Add(orBatch);

            orBatch = new SearchBatch();
			orBatch.AddSearchWord("develop",  0 );
			orBatch.AddSearchWord("developing", 0 );
            notInBatch = new SearchBatch();
            string[] notInWordWithInWordsThree =
            {
                "develop countries", "develop transport", "develop environment", "develop projects", "develop project",
                "develop trust", "develop strategies", "develop strategy", "develop system", "develop software",
                "develop businesses", "develop business",
                "develop communications", "develop communication", "develop products", "develop product",
                "developing countries", "developing transport", "developing environment",
                "developing projects", "developing project", "developing trust", "developing strategies",
                "developing strategy", "developing system", "developing software",
                "developing businesses", "developing business", "developing communications", "developing communication",
                "developing products", "developing product"
            };

            notInBatch.AddRangeSearchWords(notInWordWithInWordsThree,3);

			orBatch.NotInWords.Add(notInBatch);
            batch.OrSubSearchBatch.Add(orBatch);

            orBatch = new SearchBatch();
			orBatch.AddSearchWord("development",0);
			orBatch.AddSearchWord("developments",0 );
            notInBatch = new SearchBatch();
            string[] notInWordWithInOneWord =
            {
                "product developments", "product development", "products developments", "products development", "communication developments", "communication development",
                "communications developments", "communications development", "business developments", "business development", "businesses developments", "businesses development",
                "software developments", "software development", "system developments", "system development", "strategy developments", "strategy development",
                "strategies developments", "strategies development", "trust developments", "trust development", "project developments", "project development", "projects developments",
                "projects development", "environment developments", "environment development", "transport developments", "transport development", "countries developments",
                "countries development", "market developments", "market development"
            };
            notInBatch.AddRangeSearchWords(notInWordWithInOneWord,1);
			
            orBatch.NotInWords.Add(notInBatch);
            batch.OrSubSearchBatch.Add(orBatch);

            orBatch = new SearchBatch();
		    orBatch.BatchSearchWords.Add(new SearchWord() { Word = "course", WithinWords = 0 });
            notInBatch = new SearchBatch();
			notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "Of courseOR Of couse", WithinWords = 0 });
			notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "course of", WithinWords = 0 });
            orBatch.NotInWords.Add(notInBatch);
            batch.OrSubSearchBatch.Add(orBatch);

            orBatch = new SearchBatch();
		    orBatch.BatchSearchWords.Add(new SearchWord() { Word = "education", WithinWords = 0 });
            notInBatch = new SearchBatch();
            string[] notInWordWithInOneWord2nd =
            {
                "kid", "kids", "children", "childrens", "child", "no education", "education bonus", "education bonuses",
                "lack education", "work in education"
            };

		    notInBatch.AddSearchWord("education background", 1 );
            notInBatch.AddRangeSearchWords(notInWordWithInOneWord2nd, 0);
			
            orBatch.NotInWords.Add(notInBatch);
            batch.OrSubSearchBatch.Add(orBatch);

            orBatch = new SearchBatch();
    		orBatch.AddSearchWord("feedback", 0);
            notInBatch = new SearchBatch();
            notInBatch.AddSearchWord("customer feedback", 5);
			notInBatch.AddSearchWord("customers feedback", 0);
            orBatch.NotInWords.Add(notInBatch);
            batch.OrSubSearchBatch.Add(orBatch);

            orBatch = new SearchBatch();
    		orBatch.AddSearchWord("feed back", 0);
            notInBatch = new SearchBatch();
			notInBatch.AddSearchWord("customers feed back", 0);
			notInBatch.AddSearchWord("customer feed back", 0);
            orBatch.NotInWords.Add(notInBatch);
            batch.OrSubSearchBatch.Add(orBatch);


			cat.Batches.Add(batch);
            return cat;

        }
        
   //     public Category GetCategory()
   //     {
   //         var cat = new Category();
   //         cat.Name = "LearningDevelopment";
   //         cat.Id = "c119542";
   //         cat.MinimumMatches = 2;

			//var batch = new SearchBatch();


			//var orBatch = new SearchBatch();
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promotions", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promoted", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promote internally", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promote internal", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promote them", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promote him", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promote her", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promote you", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promote within", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promote staff", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promote some one", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promote those", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promote potential", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promote personnel", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promote employees", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promote employee", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promote people", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promote person", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promotes internally", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promotes internal", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promotes them", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promotes him", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promotes her", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promotes you", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promotes within", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promotes staff", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promotes some one", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promotes those", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promotes potential", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promotes personnel", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promotes employees", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promotes employee", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promotes people", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promotes person", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promoting internally", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promoting internal", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promoting them", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promoting him", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promoting her", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promoting you", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promoting within", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promoting staff", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promoting some one", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promoting those", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promoting potential", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promoting personnel", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promoting employees", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promoting employee", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promoting people", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promoting person", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promotional opportunities", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promotional positions", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promote my self", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promoting his", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promote the ones", WithinWords = 1 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promote person", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promote somebody", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promote position", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promote leaders", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promote individuals", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promoting associates", WithinWords = 1 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promoting top performers", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "promotes workers", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "my potential", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "potential in me", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "their potential", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "growth potential", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "potential to grow", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "potential for advancement", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "potential opportunities", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "development potential", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "develop potentials", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "potentials employees", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "potential employees", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "careers", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "move up", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "moving up", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "moved up", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "moves up", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "move upward", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "moving upward", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "moved upward", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "moves upward", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "upward growth", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "upward company", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "upward movement", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "grow within", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "grow alongside", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "grow in", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "grow with", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "opportunity", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "opportunities", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "chance", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "chances", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "possibilities", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "possibility", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "position opens", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "positions open", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "position opening", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "positions opening", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "position opened", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "positions opened", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "job opens", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "jobs open", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "job opening", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "jobs opening", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "job opened", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "jobs opened", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "progress job", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "my future", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "new challenge", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "new challenges", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "developmental", WithinWords = 0 });
   //         batch.BatchSearchWords.Add(new SearchWord() { Word = "career progression", WithinWords = 0 });
   //         batch.BatchSearchWords.Add(new SearchWord() { Word = "individual progress", WithinWords = 3 });
   //         batch.BatchSearchWords.Add(new SearchWord() { Word = "improve myself", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "better myself", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "focus people", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "future progress", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "mentor*", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "training", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "trainings", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "train", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "trained", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "learning", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "learnings", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "learn", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "learned", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "courses", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "guidance", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "guidances", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "guideline", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "guidelines", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "tutorial", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "tutorials", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "manuals", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "educate", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "educates", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "educating", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "coached", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "coachings", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "coaching", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "mistake feedback", WithinWords = 6 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "mistake learn", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "mistake help", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "constructive criticism", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "individual evaluations", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "employee evaluations", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "staff evaluations", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "performance evaluations", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "status evaluations", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "performance evaluation", WithinWords = 0 });


   //         orBatch = new SearchBatch();
			//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "promotion", WithinWords = 0 });
   //         var notInBatch = new SearchBatch();
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "promotion company", WithinWords = 2 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "promotion products", WithinWords = 2 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "promotion customer", WithinWords = 3 });
   //         orBatch.NotInWords.Add(notInBatch);
   //         batch.OrSubSearchBatch.Add(orBatch);

   //         orBatch = new SearchBatch();
		 //   orBatch.BatchSearchWords.Add(new SearchWord() { Word = "career", WithinWords = 0 });
   //         notInBatch = new SearchBatch();
		 //   notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "in career", WithinWords = 1 });
   //         orBatch.NotInWords.Add(notInBatch);
   //         batch.OrSubSearchBatch.Add(orBatch);

   //         orBatch = new SearchBatch();
   // 		orBatch.BatchSearchWords.Add(new SearchWord() { Word = "upward mobility", WithinWords = 0 });
   //         notInBatch = new SearchBatch();
		 //   notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "mobility in this department", WithinWords = 0 });
   //         orBatch.NotInWords.Add(notInBatch);
   //         batch.OrSubSearchBatch.Add(orBatch);

   //         orBatch = new SearchBatch();
			//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "develop", WithinWords = 0 });
			//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "developing", WithinWords = 0 });
   //         notInBatch = new SearchBatch();
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "develop countries", WithinWords = 3 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "develop transport", WithinWords = 3 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "develop environment", WithinWords = 3 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "develop projects", WithinWords = 3 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "develop project", WithinWords = 3 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "develop trust", WithinWords = 3 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "develop strategies", WithinWords = 3 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "develop strategy", WithinWords = 3 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "develop system", WithinWords = 3 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "develop software", WithinWords = 3 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "develop businesses", WithinWords = 3 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "develop business", WithinWords = 3 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "develop communications", WithinWords = 3 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "develop communication", WithinWords = 3 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "develop products", WithinWords = 3 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "develop product", WithinWords = 3 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "developing countries", WithinWords = 3 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "developing transport", WithinWords = 3 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "developing environment", WithinWords = 3 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "developing projects", WithinWords = 3 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "developing project", WithinWords = 3 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "developing trust", WithinWords = 3 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "developing strategies", WithinWords = 3 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "developing strategy", WithinWords = 3 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "developing system", WithinWords = 3 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "developing software", WithinWords = 3 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "developing businesses", WithinWords = 3 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "developing business", WithinWords = 3 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "developing communications", WithinWords = 3 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "developing communication", WithinWords = 3 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "developing products", WithinWords = 3 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "developing product", WithinWords = 3 });
   //         orBatch.NotInWords.Add(notInBatch);
   //         batch.OrSubSearchBatch.Add(orBatch);

   //         orBatch = new SearchBatch();
			//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "development", WithinWords = 0 });
			//orBatch.BatchSearchWords.Add(new SearchWord() { Word = "developments", WithinWords = 0 });
   //         notInBatch = new SearchBatch();
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "product developments", WithinWords = 1 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "product development", WithinWords = 1 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "products developments", WithinWords = 1 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "products development", WithinWords = 1 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "communication developments", WithinWords = 1 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "communication development", WithinWords = 1 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "communications developments", WithinWords = 1 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "communications development", WithinWords = 1 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "business developments", WithinWords = 1 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "business development", WithinWords = 1 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "businesses developments", WithinWords = 1 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "businesses development", WithinWords = 1 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "software developments", WithinWords = 1 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "software development", WithinWords = 1 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "system developments", WithinWords = 1 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "system development", WithinWords = 1 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "strategy developments", WithinWords = 1 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "strategy development", WithinWords = 1 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "strategies developments", WithinWords = 1 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "strategies development", WithinWords = 1 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "trust developments", WithinWords = 1 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "trust development", WithinWords = 1 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "project developments", WithinWords = 1 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "project development", WithinWords = 1 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "projects developments", WithinWords = 1 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "projects development", WithinWords = 1 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "environment developments", WithinWords = 1 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "environment development", WithinWords = 1 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "transport developments", WithinWords = 1 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "transport development", WithinWords = 1 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "countries developments", WithinWords = 1 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "countries development", WithinWords = 1 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "market developments", WithinWords = 1 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "market development", WithinWords = 1 });
   //         orBatch.NotInWords.Add(notInBatch);
   //         batch.OrSubSearchBatch.Add(orBatch);

   //         orBatch = new SearchBatch();
		 //   orBatch.BatchSearchWords.Add(new SearchWord() { Word = "course", WithinWords = 0 });
   //         notInBatch = new SearchBatch();
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "Of courseOR Of couse", WithinWords = 0 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "course of", WithinWords = 0 });
   //         orBatch.NotInWords.Add(notInBatch);
   //         batch.OrSubSearchBatch.Add(orBatch);

   //         orBatch = new SearchBatch();
		 //   orBatch.BatchSearchWords.Add(new SearchWord() { Word = "education", WithinWords = 0 });
   //         notInBatch = new SearchBatch();
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "kid", WithinWords = 0 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "kids", WithinWords = 0 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "children", WithinWords = 0 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "childrens", WithinWords = 0 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "child", WithinWords = 0 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "no education", WithinWords = 0 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "education background", WithinWords = 1 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "education bonus", WithinWords = 0 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "education bonuses", WithinWords = 0 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "lack education", WithinWords = 0 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "work in education", WithinWords = 0 });
   //         orBatch.NotInWords.Add(notInBatch);
   //         batch.OrSubSearchBatch.Add(orBatch);

   //         orBatch = new SearchBatch();
   // 		orBatch.BatchSearchWords.Add(new SearchWord() { Word = "feedback", WithinWords = 0 });
   //         notInBatch = new SearchBatch();
   //         notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "customer feedback", WithinWords = 5 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "customers feedback", WithinWords = 0 });
   //         orBatch.NotInWords.Add(notInBatch);
   //         batch.OrSubSearchBatch.Add(orBatch);

   //         orBatch = new SearchBatch();
   // 		orBatch.BatchSearchWords.Add(new SearchWord() { Word = "feed back", WithinWords = 0 });
   //         notInBatch = new SearchBatch();
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "customers feed back", WithinWords = 0 });
			//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "customer feed back", WithinWords = 0 });
   //         orBatch.NotInWords.Add(notInBatch);
   //         batch.OrSubSearchBatch.Add(orBatch);


			//cat.Batches.Add(batch);
   //         return cat;

   //     }
    }
}
