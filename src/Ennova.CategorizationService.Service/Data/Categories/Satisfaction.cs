﻿using Ennova.CategorizationService.Service.Model;

namespace Ennova.CategorizationService.Service.Data.Categories
{
    public class Satisfaction
    {
		public Category GetCategory()
		{
			var cat = new Category();
			cat.Name = "Satisfaction";
			cat.Id = "c119544";
			cat.MinimumMatches = 2;


			var globalBatch = new SearchBatch();
			var globalNotBatch = new SearchBatch();
			string[] wordWithInWordsZero =
			{
				"customer satisfaction","leadership satisfaction","satisfied customers","happy customers",
			};

			globalNotBatch.AddRangeSearchWords(wordWithInWordsZero, 0);
			globalBatch.NotInWords.Add(globalNotBatch);
			cat.GlobalBatch = globalBatch;


			var batch = new SearchBatch();
			string[] wordWithInWordsZero2nd =
			{
				"dissatisfaction*", "dissatisfied", "dissatisfies", "dissatisfying", "unhappy", "wonder company",
				"my experience as", "my experience with", "my experience in", "place to work", "employee focused",
				"like my job", "like the job", "like this job", "like my work", "like the work", "like this work",
				"like working", "valued", "respected", "more recognition", "better recognition", "show recognition",
				"supportive*", "ignore me", "information to employees", "information to employee", "transparency",
				"more openness", "greater openness", "employment survey", "sense of belonging", "inclusi*"
			};
			string[] wordWithInWordsOne2nd =
			{
				"best company", "greatest company", "nice company","great company"
			};
			string[] wordWithInWordsTwo2nd =
			{
				"excellent company", "decent company", "to proud", "employee recognition", "employee appreciation",
				"recognition quality", "recognition work", "involve employee", "involve employees", "involve workers",
				"employee survey", "employee opinions", "peoples opinion", "people opinions", "peoples opinions",
				"excellent company"
			};
			string[] wordWithInWordsThree2nd =
			{
				"thanks company", "super company", "company treat", "impressed company", "feel good", "felt good",
				"feel pride", "feel fortunate", "feel great", "feel honored", "feel respect", "feel respected",
				"feel bad", "felt bad", "feel worse", "felt worse", "feel thankful", "felt thankful",
				"feel discouraged", "felt discouraged", "feel discourage", "felt discourage", "feel encouraged",
				"feel encourage", "felt encouraged", "felt encourage", "felt pride", "felt fortunate", "felt great",
				"felt honored", "felt respect", "felt respected", "belong company", "belong organisation",
				"belong organization", "belonging company", "belonging organisation", "belonging organization",
				"belonging work", "recognition job", "listens employees", "listens employee", "listen people",
				"listens people", "listen us", "listens us", "listen workers", "listens workers", "listening employee",
				"listening employees", "listening people", "listening us", "listening workers", "employee involvement",
				"inform employee", "inform employees", "informs employee", "informs employees", "informing employee",
				"informing employees", "informed employee", "informed employees", "inform people", "informs people",
				"informing people", "informed people", "inform us", "informs us", "informing us", "informed us",
				"inform workers", "informs workers", "informing workers", "informed workers", "trust employees",
				"trust employee", "trusts employees", "trusts employee", "trust people", "trust us", "trusts people",
				"trusts us", "trusts workers", "trusting employees", "trusting employee", "trusting people",
				"trusting us", "trusting workers", "communication employees", "information employee", "input employees",
				"input employee", "inputs employee", "inputs employees", "input people", "inputs people",
				"input workers", "inputs workers", "suggestion employees", "suggestion employee",
				"suggestions employee", "suggestions employees", "suggestion people", "suggestion workers",
				"suggestions workers", "suggestions people", "employee say", "employees say", "transparent employees",
				"transparent employee", "open employees"
			};
			string[] wordWithInWordsFour2nd =
			{
				"great job", "hate job", "boring job", "best job", "good company", "bad company", "great company",
				"enjoy job", "enjoy work", "enjoy working", "enjoy company", "interesting job", "interesting work",
				"interesting working", "exciting job", "exciting work", "exciting working", "honoured work",
				"honored work", "love job", "love work", "love working", "love company", "love position", "i proud",
				"im proud", "me proud", "and proud", "we proud", "listen employee", "listen employees",
				"employees involvement", "employee opinion", "employees opinions", "worker opinion", "workers opinion",
				"worker opinions", "workers opinions"
			};

			batch.AddRangeSearchWords(wordWithInWordsZero2nd, 0);
			batch.AddRangeSearchWords(wordWithInWordsOne2nd, 1);
			batch.AddRangeSearchWords(wordWithInWordsTwo2nd, 2);
			batch.AddRangeSearchWords(wordWithInWordsThree2nd, 3);
			batch.AddRangeSearchWords(wordWithInWordsFour2nd, 4);



			var orbatch = new SearchBatch();
			orbatch.BatchSearchWords.Add(new SearchWord() { Word = "satisfaction", WithinWords = 1 });

			var notInBatch = new SearchBatch();

			string[] notInWordWithInWordsTwo =
			{
				"customer satisfaction", "customers satisfaction", "consumer satisfaction", "consumers satisfaction",
			};
			notInBatch.AddRangeSearchWords(notInWordWithInWordsTwo, 2);

			orbatch.NotInWords.Add(notInBatch);
			batch.OrSubSearchBatch.Add(orbatch);

			orbatch = new SearchBatch();
			batch.AddSearchWord("satisfactions", 0);

			notInBatch = new SearchBatch();

			string[] notInWordWithInWordsTwo2nd =
			{
				"customer satisfactions",
				"customers satisfactions",
				"consumer satisfactions",
				"consumers satisfactions",
			};

			notInBatch.AddRangeSearchWords(notInWordWithInWordsTwo2nd, 2);

			orbatch.NotInWords.Add(notInBatch);
			batch.OrSubSearchBatch.Add(orbatch);

			orbatch = new SearchBatch();
			batch.AddSearchWord("satisfied", 0);

			notInBatch = new SearchBatch();
			notInBatch.AddSearchWord("satisfied customer", 0);
			notInBatch.AddSearchWord("satisfied customers", 0);
			orbatch.NotInWords.Add(notInBatch);
			batch.OrSubSearchBatch.Add(orbatch);

			orbatch = new SearchBatch();
			batch.AddSearchWord("satisfying", 0);

			notInBatch = new SearchBatch();
			notInBatch.AddSearchWord("customer", 0);
			notInBatch.AddSearchWord("customers", 0);
			orbatch.NotInWords.Add(notInBatch);
			batch.OrSubSearchBatch.Add(orbatch);

			orbatch = new SearchBatch();
			batch.AddSearchWord("happy", 0);

			notInBatch = new SearchBatch();

			string[] notInWordWithInWordsTin =
			{
				"happy customer", "happy customers", "happy consumer", "happy consumers"
			};

			notInBatch.AddRangeSearchWords(notInWordWithInWordsTin, 10);

			orbatch.NotInWords.Add(notInBatch);
			batch.OrSubSearchBatch.Add(orbatch);

			orbatch = new SearchBatch();
			batch.AddSearchWord("like the company", 0);

			notInBatch = new SearchBatch();
			notInBatch.AddSearchWord("like the company to", 0);
			orbatch.NotInWords.Add(notInBatch);
			batch.OrSubSearchBatch.Add(orbatch);

			orbatch = new SearchBatch();
			batch.AddSearchWord("i part of", 6);
			batch.AddSearchWord("we part of", 3);
			batch.AddSearchWord("be part of", 1);

			notInBatch = new SearchBatch();
			notInBatch.AddSearchWord("own part", 2);
			notInBatch.AddSearchWord("part cultural", 3);
			notInBatch.AddSearchWord("understand part", 3);
			notInBatch.AddSearchWord("part job", 3);
			notInBatch.AddSearchWord("part changes", 4);

			string[] notInWordWithInWordsZero =
			{
				"as part of", "as part of", "part time", "big part", "part of myself", "part of themselves", "part of this is",
			};

			notInBatch.AddRangeSearchWords(notInWordWithInWordsZero, 0);

			orbatch.NotInWords.Add(notInBatch);
			batch.OrSubSearchBatch.Add(orbatch);


			orbatch = new SearchBatch();
			orbatch.AddSearchWord("trust workers", 4);

			notInBatch = new SearchBatch();
			notInBatch.AddSearchWord("co workers", 0);
			orbatch.NotInWords.Add(notInBatch);
			batch.OrSubSearchBatch.Add(orbatch);

			orbatch = new SearchBatch();
			orbatch.AddSearchWord("people opinion", 3);

			notInBatch = new SearchBatch();
			notInBatch.AddSearchWord("in my opinion", 0);
			orbatch.NotInWords.Add(notInBatch);
			batch.OrSubSearchBatch.Add(orbatch);

			notInBatch = new SearchBatch();
			notInBatch.AddSearchWord("customer satisfaction", 0);
			notInBatch.AddSearchWord("leadership satisfaction", 0);
			notInBatch.AddSearchWord("satisfied customers", 0);
			notInBatch.AddSearchWord("happy customers", 0);
			batch.NotInWords.Add(notInBatch);

			cat.Batches.Add(batch);
			return cat;

		}
		//     public Category GetCategory()
		//     {
		//         var cat = new Category();
		//         cat.Name = "Satisfaction";
		//         cat.Id = "c119544";
		//         cat.MinimumMatches = 2;


		//         var globalBatch = new SearchBatch();
		//         var globalNotBatch = new SearchBatch();
		//         globalNotBatch.BatchSearchWords.Add(new SearchWord() { Word = "customer satisfaction", WithinWords = 0 });
		//         globalNotBatch.BatchSearchWords.Add(new SearchWord() { Word = "leadership satisfaction", WithinWords = 0 });
		//         globalNotBatch.BatchSearchWords.Add(new SearchWord() { Word = "satisfied customers", WithinWords = 0 });
		//         globalNotBatch.BatchSearchWords.Add(new SearchWord() { Word = "happy customers", WithinWords = 0 });
		//         globalBatch.NotInWords.Add(globalNotBatch);
		//         cat.GlobalBatch = globalBatch;


		//var batch = new SearchBatch();
		//         batch.BatchSearchWords.Add(new SearchWord() { Word = "dissatisfaction*", WithinWords = 0 });
		//         batch.BatchSearchWords.Add(new SearchWord() { Word = "dissatisfied", WithinWords = 0 });
		//         batch.BatchSearchWords.Add(new SearchWord() { Word = "dissatisfies", WithinWords = 0 });
		//         batch.BatchSearchWords.Add(new SearchWord() { Word = "dissatisfying", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "unhappy", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "great job", WithinWords = 4 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "hate job", WithinWords = 4 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "boring job", WithinWords = 4 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "best job", WithinWords = 4 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "good company", WithinWords = 4 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "bad company", WithinWords = 4 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "great company", WithinWords = 4 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "thanks company", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "excellent company", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "wonder company", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "super company", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "my experience as", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "my experience with", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "my experience in", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "place to work", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "enjoy job", WithinWords = 4 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "enjoy work", WithinWords = 4 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "enjoy working", WithinWords = 4 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "enjoy company", WithinWords = 4 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "interesting job", WithinWords = 4 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "interesting work", WithinWords = 4 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "interesting working", WithinWords = 4 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "exciting job", WithinWords = 4 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "exciting work", WithinWords = 4 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "exciting working", WithinWords = 4 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "employee focused", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "honoured work", WithinWords = 4 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "honored work", WithinWords = 4 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "like my job", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "like the job", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "like this job", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "like my work", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "like the work", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "like this work", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "like working", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "love job", WithinWords = 4 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "love work", WithinWords = 4 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "love working", WithinWords = 4 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "love company", WithinWords = 4 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "love position", WithinWords = 4 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "best company", WithinWords = 1 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "great company", WithinWords = 1 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "greatest company", WithinWords = 1 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "nice company", WithinWords = 1 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "company treat", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "company treats", WithinWords = 6 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "company treated", WithinWords = 6 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "decent company", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "excellent company", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "impressed company", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "feel good", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "felt good", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "feel pride", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "feel fortunate", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "feel great", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "feel honored", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "feel respect", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "feel respected", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "feel bad", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "felt bad", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "feel worse", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "felt worse", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "feel thankful", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "felt thankful", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "feel discouraged", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "felt discouraged", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "feel discourage", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "felt discourage", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "feel encouraged", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "feel encourage", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "felt encouraged", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "felt encourage", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "felt pride", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "felt fortunate", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "felt great", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "felt honored", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "felt respect", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "felt respected", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "i proud", WithinWords = 4 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "im proud", WithinWords = 4 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "me proud", WithinWords = 4 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "and proud", WithinWords = 4 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "we proud", WithinWords = 4 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "to proud", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "valued", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "respected", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "belong company", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "belong organisation", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "belong organization", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "belonging company", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "belonging organisation", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "belonging organization", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "belonging work", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "employee recognition", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "employee appreciation", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "more recognition", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "better recognition", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "recognition quality", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "recognition work", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "recognition job", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "show recognition", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "supportive*", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "listen employee", WithinWords = 4 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "listen employees", WithinWords = 4 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "listens employees", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "listens employee", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "listen people", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "listens people", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "listen us", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "listens us", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "listen workers", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "listens workers", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "listening employee", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "listening employees", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "listening people", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "listening us", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "listening workers", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "employee involvement", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "employees involvement", WithinWords = 4 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "ignore me", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "involve employee", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "involve employees", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "involve workers", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "inform employee", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "inform employees", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "informs employee", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "informs employees", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "informing employee", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "informing employees", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "informed employee", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "informed employees", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "inform people", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "informs people", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "informing people", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "informed people", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "inform us", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "informs us", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "informing us", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "informed us", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "inform workers", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "informs workers", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "informing workers", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "informed workers", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "trust employees", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "trust employee", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "trusts employees", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "trusts employee", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "trust people", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "trust us", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "trusts people", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "trusts us", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "trusts workers", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "trusting employees", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "trusting employee", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "trusting people", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "trusting us", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "trusting workers", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "communication employees", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "information employee", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "information to employees", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "information to employee", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "input employees", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "input employee", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "inputs employee", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "inputs employees", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "input people", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "inputs people", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "input workers", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "inputs workers", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "suggestion employees", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "suggestion employee", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "suggestions employee", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "suggestions employees", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "suggestion people", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "suggestion workers", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "suggestions workers", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "suggestions people", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "employee say", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "employees say", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "transparent employees", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "transparent employee", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "transparency", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "open employees", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "more openness", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "greater openness", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "employee survey", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "employment survey", WithinWords = 0 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "employee opinion", WithinWords = 4 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "employees opinions", WithinWords = 4 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "employee opinions", WithinWords = 2 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "worker opinion", WithinWords = 4 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "workers opinion", WithinWords = 4 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "worker opinions", WithinWords = 4 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "workers opinions", WithinWords = 4 });
		//         batch.BatchSearchWords.Add(new SearchWord() { Word = "peoples opinion", WithinWords = 2 });
		//         batch.BatchSearchWords.Add(new SearchWord() { Word = "people opinions", WithinWords = 2 });
		//         batch.BatchSearchWords.Add(new SearchWord() { Word = "peoples opinions", WithinWords = 2 });
		//         batch.BatchSearchWords.Add(new SearchWord() { Word = "sense of belonging", WithinWords = 0 });
		//         batch.BatchSearchWords.Add(new SearchWord() { Word = "inclusi*", WithinWords = 0 });


		//var orbatch = new SearchBatch();
		//         orbatch.BatchSearchWords.Add(new SearchWord() { Word = "satisfaction", WithinWords = 1 });

		//         var notInBatch = new SearchBatch();
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "customer satisfaction", WithinWords = 2 });
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "customers satisfaction", WithinWords = 2 });
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "consumer satisfaction", WithinWords = 2 });
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "consumers satisfaction", WithinWords = 2 });
		//         orbatch.NotInWords.Add(notInBatch);
		//batch.OrSubSearchBatch.Add(orbatch);

		//         orbatch = new SearchBatch();
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "satisfactions", WithinWords = 0 });

		//         notInBatch = new SearchBatch();
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "customer satisfactions", WithinWords = 2 });
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "customers satisfactions", WithinWords = 2 });
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "consumer satisfactions", WithinWords = 2 });
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "consumers satisfactions", WithinWords = 2 });
		//         orbatch.NotInWords.Add(notInBatch);
		//         batch.OrSubSearchBatch.Add(orbatch);

		//         orbatch = new SearchBatch();
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "satisfied", WithinWords = 0 });

		//notInBatch = new SearchBatch();
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "satisfied customer", WithinWords = 0 });
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "satisfied customers", WithinWords = 0 });
		//         orbatch.NotInWords.Add(notInBatch);
		//         batch.OrSubSearchBatch.Add(orbatch);

		//         orbatch = new SearchBatch();
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "satisfying", WithinWords = 0 });

		//         notInBatch = new SearchBatch();
		//         notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "customer", WithinWords = 0 });
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "customers", WithinWords = 0 });
		//         orbatch.NotInWords.Add(notInBatch);
		//         batch.OrSubSearchBatch.Add(orbatch);

		//         orbatch = new SearchBatch();
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "happy", WithinWords = 0 });

		//         notInBatch = new SearchBatch();
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "happy customer", WithinWords = 10 });
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "happy customers", WithinWords = 10 });
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "happy consumer", WithinWords = 10 });
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "happy consumers", WithinWords = 10 });
		//         orbatch.NotInWords.Add(notInBatch);
		//         batch.OrSubSearchBatch.Add(orbatch);

		//         orbatch = new SearchBatch();
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "like the company", WithinWords = 0 });

		//         notInBatch = new SearchBatch();
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "like the company to", WithinWords = 0 });
		//         orbatch.NotInWords.Add(notInBatch);
		//         batch.OrSubSearchBatch.Add(orbatch);

		//         orbatch = new SearchBatch();
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "i part of", WithinWords = 6 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "we part of", WithinWords = 3 });
		//batch.BatchSearchWords.Add(new SearchWord() { Word = "be part of", WithinWords = 1 });

		//         notInBatch = new SearchBatch();
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "as part of", WithinWords = 0 });
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "part time", WithinWords = 0 });
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "big part", WithinWords = 0 });
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "part of myself", WithinWords = 0 });
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "part of themselves", WithinWords = 0 });
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "own part", WithinWords = 2 });
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "part of this is", WithinWords = 0 });
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "part cultural", WithinWords = 3 });
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "understand part", WithinWords = 3 });
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "part job", WithinWords = 3 });
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "part changes", WithinWords = 4 });
		//         orbatch.NotInWords.Add(notInBatch);
		//         batch.OrSubSearchBatch.Add(orbatch);


		//         orbatch = new SearchBatch();
		//         orbatch.BatchSearchWords.Add(new SearchWord() { Word = "trust workers", WithinWords = 4 });

		//         notInBatch = new SearchBatch();
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "co workers", WithinWords = 0 });
		//         orbatch.NotInWords.Add(notInBatch);
		//         batch.OrSubSearchBatch.Add(orbatch);

		//         orbatch = new SearchBatch();
		//         orbatch.BatchSearchWords.Add(new SearchWord() { Word = "people opinion", WithinWords = 3 });

		//         notInBatch = new SearchBatch();
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "in my opinion", WithinWords = 0 });
		//         orbatch.NotInWords.Add(notInBatch);
		//         batch.OrSubSearchBatch.Add(orbatch);

		//         notInBatch = new SearchBatch();
		//   notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "customer satisfaction", WithinWords = 0 });
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "leadership satisfaction", WithinWords = 0 });
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "satisfied customers", WithinWords = 0 });
		//notInBatch.BatchSearchWords.Add(new SearchWord() { Word = "happy customers", WithinWords = 0 });
		//         batch.NotInWords.Add(notInBatch);

		//         cat.Batches.Add(batch);
		//         return cat;

		//     }
	}
}
