﻿using Ennova.CategorizationService.Service.Model;

namespace Ennova.CategorizationService.Service.Data.Categories
{
    public class WorkingConditions
    {
        public Category GetCategory()
        {
            var cat = new Category();
            cat.Name = "WorkingConditions";
            cat.Id = "c119539";
            cat.MinimumMatches = 2;

			var batch = new SearchBatch();
            string[] wordWithInWordsZero =
            {
                "workspace", "office conditions", "the office", "open office", "bathroom", "bathrooms", "restroom",
                "restrooms", "physical room", "stock room", "smoking room", "shower", "walls", "cooler", "heater",
                "heating", "warm", "cold", "air condition", "air conditioning", "air vent", "air vents", "ventilation",
                "smell", "parking lot", "parking deck", "conference room", "meeting room", "desks", "equipment",
                "tools", "noise", "toilet", "more storage", "storage area", "break room", "table tennis", "pool table",
                "PlayStation", "xbox", "relax room", "faster network", "reliable network", "network speed",
                "network issues", "better network", "it system", "it systems", "it support", "it software",
                "effective it", "improved it", "it issues", "quicker it", "enhanced it", "it capabilities",
                "it capability", "it department", "it departments", "it dept", "it service", "it services",
                "it solution", "it resource", "it resources", "information systems", "computer", "laptop*", "hardware",
                "software", "wifi", "wi fi", "scanner*", "printer*", "SAP", "work safety", "health safety",
                "safety issues", "safety gear", "safety and health", "health and safety", "safety policy",
                "safety rules", "safety reason", "safety reasons", "our safety", "safety procedures",
                "safety procedure", "workload", "overtime", "overwork", "overload", "work load", "work loads",
                "extra work", "jobs after hours", "work long", "working long", "schedules", "work life balance",
                "worklife balance", "stress*", "working hours", "work hours", "workweek", "work from home",
                "home office", "effectiveness", "supply chain", "lean process", "lean processes", "food", "canteen",
                "cantine", "catering", "dining hall", "lunch", "breakfast", "coffee", "cafeteria", "dinner", "dining",
                "smoke*", "smoking", "*ethic*", "environment friendly", "emission", "emissions", "electricity usage",
                "energy efficient", "clean energy", "renewable energy", "abuse", "threats", "abusive", "abusing",
                "abused", "harass", "harassed", "harassment", "threatening", "bully*", "corruption", "violence",
                "intimidation", "Humiliation", "humiliated", "Racism", "Racist", "discriminat*", "treat equally",
                "treated equally", "treat unequally", "treated unequally", "treat fairly", "treated fairly",
                "fair employees", "Equal opportunities", "favoritism", "favouritism","clear processes", "bully*"
			};

            string[] wordWithInWordsTwo =
            {
                "safety first", "safety work", "work over", "working over", "over worked", "flexible hours",
                "job effectively", "job efficiently", "work process", "work processes", "working process",
                "working processes", "good process", "good processes", "manage process", "manage processes"
            };
            string[] wordWithInWordsThree =
            {
                "work conditions", "working conditions", "working condition", "work condition", "work environment",
                "working environment", "work enviroment", "working enviroment", "office environment",
                "office enviroment", "physical environment", "improve office", "office space", "nice office",
                "good office", "modern office", "bigger office", "big office", "size office", "meeting rooms",
                "old machine", "old machines", "older machine", "older machines", "new machine", "new machines",
                "newer machine", "newer machines", "better machines", "good machines", "improve machines",
                "machines work", "slow network", "network down", "network speeds", "work effectively",
                "work efficiently", "work effective", "work efficient", "work effectiveness", "work efficiency",
                "clear processes", "clear process", "clearly processes", "clearly process", "better processes",
                "better process", "simple process", "simple processes", "clearer processes", "align processes",
                "clearance processes", "clearance process", "consistent processes", "consistent process",
                "Cut processes", "Cut process", "efficient processes", "efficient process", "flexibility processes",
                "flexibility process", "flexible processes", "flexible process", "improve processes", "improve process",
                "improves processes", "improves process", "improving processes", "improving process",
                "inefficient processes", "inefficient process", "less processes", "less process", "necessary processes",
                "necessary process", "needless processes", "needless process", "obstructive processes",
                "obstructive process", "reduce processes", "reduce process", "Reduced processes", "Reduced process",
                "reduction processes", "reduction process", "removal processes", "removal process",
                "removing processes", "removing process", "seamless processes", "seamless process", "simpler processes",
                "simpler process", "simplification processes", "simplification process", "lacking processes",
                "simplified processes", "simplified process", "simplify processes", "simplify process",
                "Sleeker processes", "Sleeker process", "slicker processes", "slicker process", "stream processes",
                "stream process", "Streamline processes", "Streamline process", "Streamlined processes",
                "Streamlined process", "Streamlining processes", "Streamlining process", "too many processes",
                "too many process", "unnecessary processes", "unnecessary process", "useless processes",
                "useless process", "well-thought processes", "well-thought process", "effective process",
                "effective processes", "ineffective process", "ineffective processes", "know process", "long process",
                "process improvement", "internal process", "internal processes", "inappropriate behaviour",
                "inappropriate behavior", "inappropriate behavor", "behaviour inappropriate", "behaviur inappropriate",
                "behavior inappropriate", "improper behaviour", "improper behavior", "behaviour improper",
                "equal employee", "equal employees", "equally employees", "employees equals", "treat equal",
                "treat equaly", "treat equals", "unequal employee", "unequal employees", "unequally employees",
                "treat unequal", "treat unequals", "treat fair", "staff fairly", "care employee", "care employees"
            };

            string[] wordWithInWordsFive =
            {
                "working space", "work space", "treat same", "treating same", "treated same", "treats same",
                "treatment same", "treat different", "treating different", "treated different", "treats different",
                "treatment different", "preferential treat", "preferential treated", "preferential treating",
                "preferential treats", "preferential treatment", "prefferential treatment", "prefferential treat",
                "prefferential treated", "prefferential treats", "prefferential treating", "treat better",
                "treats better", "treated better", "treating better", "treatment better"
            };

			batch.AddRangeSearchWords(wordWithInWordsZero,0);
			batch.AddSearchWord("bully*",1);
			batch.AddRangeSearchWords(wordWithInWordsTwo,2);
			batch.AddRangeSearchWords(wordWithInWordsThree,3);
			batch.AddSearchWord("works over", 4);
			batch.AddRangeSearchWords(wordWithInWordsFive,5);


			

			var orbatch = new SearchBatch();
			orbatch.AddSearchWord("fairness",0 );
            var notinBatch = new SearchBatch();
            notinBatch.AddSearchWord("salary fairness", 0 );
            orbatch.NotInWords.Add(notinBatch);
			batch.OrSubSearchBatch.Add(orbatch);



            orbatch = new SearchBatch();
            orbatch.AddSearchWord("favorites", 0 );
            notinBatch = new SearchBatch();
            notinBatch.AddSearchWord("my favorites", 0 );
            notinBatch.AddSearchWord("the favorites", 0 );
            orbatch.NotInWords.Add(notinBatch);
            batch.OrSubSearchBatch.Add(orbatch);


            orbatch = new SearchBatch();
            string[] orWordWithInWordsZero =
            {
                "in favor of", "favor one", "favor certain"
			};
            string[] orWordWithInWordsTwo =
            {
                "favor employee", "favor employees", "not objective", "favor people", "favor staff"

            };
            string[] orWordWithInWordsThree =
            {
                "favor over", "she favor"
			};
			
            orbatch.AddRangeSearchWords(orWordWithInWordsZero, 0);
            orbatch.AddRangeSearchWords(orWordWithInWordsTwo, 2);
            orbatch.AddRangeSearchWords(orWordWithInWordsThree, 3);
			
            notinBatch = new SearchBatch();
            string[] notInWordWithInWordsTwo =
            {
                "a favor", "do favor", "did favor"
            };
			notinBatch.AddRangeSearchWords(notInWordWithInWordsTwo, 2 );
			notinBatch.AddSearchWord("doing favor", 3 );
            
            orbatch.NotInWords.Add(notinBatch);
            batch.OrSubSearchBatch.Add(orbatch);


            orbatch = new SearchBatch();
            orbatch.AddSearchWord("facilit*", 0 );

            notinBatch = new SearchBatch();

            string[] notInWordWithInWordsZero =
            {
                "facilitator", "facilitating", "facilitate*"
            };
			notinBatch.AddRangeSearchWords(notInWordWithInWordsZero, 0 );
            orbatch.NotInWords.Add(notinBatch);
            batch.OrSubSearchBatch.Add(orbatch);		

            orbatch = new SearchBatch();
            orbatch.AddSearchWord("storage", 0 );

            var andBatch = new SearchBatch();

            string[] andWordWithInWordsZero =
            {
			   "space", "bigger", "larger", "room", 
			};
			andBatch.AddRangeSearchWords(andWordWithInWordsZero,0);

            orbatch.AndMustMatchWords.Add(notinBatch);
            batch.OrSubSearchBatch.Add(orbatch);		

            orbatch = new SearchBatch();
            orbatch.AddSearchWord("inventory", 0 );

            andBatch = new SearchBatch();
            andBatch.AddSearchWord("capacity", 0 );
            andBatch.AddSearchWord("room",  0 );
            orbatch.AndMustMatchWords.Add(andBatch);
            batch.OrSubSearchBatch.Add(orbatch);		

            orbatch = new SearchBatch();
            orbatch.AddSearchWord("tables", 0 );
            notinBatch = new SearchBatch();
            notinBatch.AddSearchWord("excel tables", 0 );
            notinBatch.AddSearchWord("play tables", 0 );
            orbatch.NotInWords.Add(notinBatch);
            batch.OrSubSearchBatch.Add(orbatch);		


            orbatch = new SearchBatch();
            orbatch.AddSearchWord("faster it", 0 );

            notinBatch = new SearchBatch();
            orbatch.AddSearchWord("fast it takes", 0 );
            orbatch.AddSearchWord("faster it shouldn't", 0 );
            orbatch.NotInWords.Add(notinBatch);
            batch.OrSubSearchBatch.Add(orbatch);		


            orbatch = new SearchBatch();
            orbatch.AddSearchWord("better it", 0 );

            notinBatch = new SearchBatch();
            notinBatch.AddSearchWord("better it can",0 );
            notinBatch.AddSearchWord("better it would", 0 );
            orbatch.NotInWords.Add(notinBatch);
            batch.OrSubSearchBatch.Add(orbatch);		

            
            orbatch = new SearchBatch();
            orbatch.AddSearchWord("pc", 0 );
            notinBatch = new SearchBatch();
            notinBatch.AddSearchWord("p&c", 0 );
            notinBatch.AddSearchWord("pc training", 0 );
            orbatch.NotInWords.Add(notinBatch);
            batch.OrSubSearchBatch.Add(orbatch);		


            orbatch = new SearchBatch();
            orbatch.AddSearchWord("internet", 0);

            notinBatch = new SearchBatch();


            string[] notInWordWithInWordsZero2nd =
            {
				"internet sites", "from Internet", "on internet", "Internet explorer"
			};

			notinBatch.AddRangeSearchWords(notInWordWithInWordsZero2nd,0);
            orbatch.NotInWords.Add(notinBatch);
            batch.OrSubSearchBatch.Add(orbatch);		

            orbatch = new SearchBatch();
            orbatch.AddSearchWord("security", 0 );

            notinBatch = new SearchBatch();
            string[] notInWordWithInWordsZero3nd =
            {
				"social security", "sense of security", "concept of security", "source of security",
			}; 
            string[] notInWordWithInWordsThree3nd =
            {
                "data security", "income security", "security role", "information security", "occupational security", "task security", 
			};
            
            notinBatch.AddRangeSearchWords(notInWordWithInWordsZero3nd, 0);
			notinBatch.AddSearchWord("security employment", 2 );
            notinBatch.AddRangeSearchWords(notInWordWithInWordsThree3nd,3);
            notinBatch.AddSearchWord("security job",  4 );
            notinBatch.AddSearchWord("future security",  4 ); 
			notinBatch.AddSearchWord("security employees", 5 );

			orbatch.NotInWords.Add(notinBatch);
            batch.OrSubSearchBatch.Add(orbatch);		

            
            orbatch = new SearchBatch();
            orbatch.AddSearchWord("schedule", 0 );

            notinBatch = new SearchBatch();
            notinBatch.AddSearchWord("to schedule", 0 );
            orbatch.NotInWords.Add(notinBatch);
            batch.OrSubSearchBatch.Add(orbatch);	

            
            orbatch = new SearchBatch();
            orbatch.AddSearchWord("the hours", 0 );

            notinBatch = new SearchBatch();
            notinBatch.AddSearchWord("the hours to", 0 );
            orbatch.NotInWords.Add(notinBatch);
            batch.OrSubSearchBatch.Add(orbatch);	

            
            orbatch = new SearchBatch();
            orbatch.AddSearchWord("*efficien*", 0 );
            orbatch.AddSearchWord("efficient", 1 ); 

            notinBatch = new SearchBatch();

            string[] notInWordWithInWordsZero4nd =
            {
				"tool*", "system*", "software", "machine", "PC", 
			};

			notinBatch.AddRangeSearchWords(notInWordWithInWordsZero4nd, 0 );
            
            orbatch.NotInWords.Add(notinBatch);
            batch.OrSubSearchBatch.Add(orbatch);		

                
            orbatch = new SearchBatch();
            orbatch.AddSearchWord("effectiveness", 0 );

            notinBatch = new SearchBatch();
            notinBatch.AddSearchWord("cost effectiveness", 0 );
            orbatch.NotInWords.Add(notinBatch);
            batch.OrSubSearchBatch.Add(orbatch);	

            orbatch = new SearchBatch();

            string[] orWordWithInWordsZero2nd =
            {
				"atmosphere", "morals", "climate", "work culture", "working culture", "culture", "employee relationship"
			};
			orbatch.AddRangeSearchWords(orWordWithInWordsZero2nd, 0 );
            
            orbatch.AddSearchWord("employee friendly",  3 ); 

            notinBatch = new SearchBatch();
            notinBatch.AddSearchWord("indoor climate", 0 );
            orbatch.NotInWords.Add(notinBatch);
            batch.OrSubSearchBatch.Add(orbatch);		


            orbatch = new SearchBatch();
            orbatch.AddSearchWord("integrity", 0 );
            notinBatch = new SearchBatch();
            notinBatch.AddSearchWord("data integrity", 0 );
            orbatch.NotInWords.Add(notinBatch);
            batch.OrSubSearchBatch.Add(orbatch);	

            orbatch = new SearchBatch();
            orbatch.AddSearchWord("honest", 0 );
            notinBatch = new SearchBatch();
            notinBatch.AddSearchWord("open honest", 3 );
            notinBatch.AddSearchWord("honest communication", 2);
            orbatch.NotInWords.Add(notinBatch);
            batch.OrSubSearchBatch.Add(orbatch);		

            orbatch = new SearchBatch();
            notinBatch.AddSearchWord("honesty", 3 );

            notinBatch = new SearchBatch();
            notinBatch.AddSearchWord("openness honesty", 3 );
            notinBatch.AddSearchWord("honesty communication",  2 );
            orbatch.NotInWords.Add(notinBatch);
            batch.OrSubSearchBatch.Add(orbatch);		


            orbatch = new SearchBatch();
            orbatch.AddSearchWord("diversity", 0 );

            notinBatch = new SearchBatch();
            string[] notInWordWithInWordsZero5nd =
            {
                "job diversity", "work diversity", "diversity snacks",
			};
            string[] notInWordWithInWordsTwo5nd =
            {
                "diversity products", "diversity work", "diversity projects", "diversity problems", "diversity activities", "diversity tasks", 
			};
			notinBatch.AddRangeSearchWords(notInWordWithInWordsZero5nd, 0 );
			notinBatch.AddRangeSearchWords(notInWordWithInWordsTwo5nd, 2 );
            notinBatch.AddSearchWord("diversity job", 3);
			
            orbatch.NotInWords.Add(notinBatch);
            batch.OrSubSearchBatch.Add(orbatch);		

            orbatch = new SearchBatch();
            orbatch.AddSearchWord("diverse", 0 );

            notinBatch = new SearchBatch();

			string[] notInWordWithInWordsZero6nd =
            {
				 "job diverse", "diverse snacks", "work diverse",
			};
            string[] notInWordWithInWordsTwo6nd =
            {
                "diverse products", "diverse work", "diverse projects", "diverse problems", "diverse activities", "diverse tasks",
			};

            notinBatch.AddRangeSearchWords(notInWordWithInWordsZero6nd, 0);
            notinBatch.AddRangeSearchWords(notInWordWithInWordsTwo6nd, 2);
			notinBatch.AddSearchWord("diverse job", 3);

			orbatch.NotInWords.Add(notinBatch);
            batch.OrSubSearchBatch.Add(orbatch);		

            
            orbatch = new SearchBatch();
            orbatch.AddSearchWord("diversified",0 );
            notinBatch = new SearchBatch();

			string[] notInWordWithInWordsZero7nd =
            {
				"job diversified", "diversified snacks","work diversified",
			};
            string[] notInWordWithInWordsTwo7nd =
            {
				"diversified projects", "diversified problems", "diversified activities", "diversified tasks", "diversified products", "diversified work"
			};

            notinBatch.AddRangeSearchWords(notInWordWithInWordsZero7nd, 0);
            notinBatch.AddRangeSearchWords(notInWordWithInWordsTwo7nd, 2);
			notinBatch.AddSearchWord("diversified job", 3 );

			orbatch.NotInWords.Add(notinBatch);
            batch.OrSubSearchBatch.Add(orbatch);		

            cat.Batches.Add(batch);
			return cat;

        }
        
   //     public Category GetCategory()
   //     {
   //         var cat = new Category();
   //         cat.Name = "WorkingConditions";
   //         cat.Id = "c119539";
   //         cat.MinimumMatches = 2;

			//var batch = new SearchBatch();

			//batch.BatchSearchWords.Add(new SearchWord() { Word = "work conditions", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "working conditions", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "working condition", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "work condition", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "work environment", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "working environment", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "work enviroment", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "working enviroment", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "office environment", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "office enviroment", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "physical environment", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "working space", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "work space", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "workspace", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "office conditions", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "the office", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "open office", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "improve office", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "office space", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "nice office", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "good office", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "modern office", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "bigger office", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "big office", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "size office", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "bathroom", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "bathrooms", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "restroom", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "restrooms", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "physical room", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "stock room", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "smoking room", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "shower", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "walls", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "cooler", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "heater", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "heating", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "warm", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "cold", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "air condition", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "air conditioning", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "air vent", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "air vents", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "ventilation", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "smell", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "parking lot", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "parking deck", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "conference room", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "meeting room", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "meeting rooms", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "desks", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "equipment", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "tools", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "noise", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "toilet", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "more storage", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "storage area", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "break room", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "table tennis", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "pool table", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "PlayStation", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "xbox", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "relax room", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "old machine", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "old machines", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "older machine", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "older machines", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "new machine", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "new machines", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "newer machine", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "newer machines", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "better machines", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "good machines", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "improve machines", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "machines work", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "faster network", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "reliable network", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "network speed", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "network issues", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "better network", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "slow network", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "network down", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "network speed", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "network speeds", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "it system", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "it systems", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "it support", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "it software", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "effective it", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "improved it", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "it issues", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "it challenges issues", WithinWords = 1 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "quicker it", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "enhanced it", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "it capabilities", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "it capability", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "it department", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "it departments", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "it dept", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "it service", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "it services", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "it solution", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "it resource", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "it resources", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "information systems", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "computer", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "laptop*", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "hardware", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "software", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "wifi", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "wi fi", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "scanner*", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "printer*", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "SAP", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "work safety", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "health safety", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "safety issues", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "safety gear", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "safety and health", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "health and safety", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "safety policy", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "safety rules", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "safety reason", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "safety reasons", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "our safety", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "safety first", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "safety work", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "safety procedures", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "safety procedure", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "workload", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "overtime", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "overwork", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "overload", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "work load", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "work loads", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "work over", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "working over", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "works over", WithinWords = 4 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "over worked", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "extra work", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "jobs after hours", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "work long", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "working long", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "schedules", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "work life balance", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "worklife balance", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "stress*", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "flexible hours", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "working hours", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "work hours", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "workweek", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "work from home", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "home office", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "work effectively", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "work efficiently", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "work effective", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "work efficient", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "work effectiveness", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "work efficiency", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "effectiveness", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "clear processes", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "clear process", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "clearly processes", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "clearly process", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "job effectively", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "job efficiently", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "supply chain", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "better processes", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "better process", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "simple process", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "simple processes", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "lean process", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "lean processes", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "clear processes", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "clear process", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "clear processes", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "clearer processes", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "align processes", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "clearance processes", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "clearance process", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "consistent processes", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "consistent process", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "Cut processes", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "Cut process", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "efficient processes", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "efficient process", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "flexibility processes", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "flexibility process", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "flexible processes", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "flexible process", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "improve processes", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "improve process", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "improves processes", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "improves process", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "improving processes", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "improving process", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "inefficient processes", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "inefficient process", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "less processes", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "less process", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "necessary processes", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "necessary process", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "needless processes", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "needless process", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "obstructive processes", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "obstructive process", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "reduce processes", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "reduce process", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "Reduced processes", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "Reduced process", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "reduction processes", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "reduction process", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "removal processes", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "removal process", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "removing processes", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "removing process", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "seamless processes", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "seamless process", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "simpler processes", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "simpler process", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "simplification processes", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "simplification process", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "lacking processes", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "simplified processes", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "simplified process", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "simplify processes", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "simplify process", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "Sleeker processes", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "Sleeker process", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "slicker processes", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "slicker process", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "stream processes", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "stream process", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "Streamline processes", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "Streamline process", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "Streamlined processes", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "Streamlined process", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "Streamlining processes", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "Streamlining process", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "too many processes", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "too many process", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "unnecessary processes", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "unnecessary process", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "useless processes", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "useless process", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "well-thought processes", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "well-thought process", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "effective process", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "effective processes", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "ineffective process", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "ineffective processes", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "know process", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "long process", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "process improvement", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "internal process", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "internal processes", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "work process", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "work processes", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "working process", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "working processes", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "good process", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "good processes", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "manage process", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "manage processes", WithinWords = 2 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "food", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "canteen", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "cantine", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "catering", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "dining hall", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "lunch", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "breakfast", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "coffee", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "cafeteria", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "dinner", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "dining", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "smoke*", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "smoking", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "*ethic*", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "environment friendly", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "emission", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "emissions", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "electricity usage", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "energy efficient", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "clean energy", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "renewable energy", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "abuse", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "threats", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "abusive", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "abusing", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "abused", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "harass", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "harassed", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "harassment", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "inappropriate behaviour", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "inappropriate behavior", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "inappropriate behavor", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "behaviour inappropriate", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "behaviur inappropriate", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "behavior inappropriate", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "behaviour inappropriate", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "improper behaviour", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "improper behavior", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "behaviour improper", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "threatening", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "bully*", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "corruption", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "violence", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "intimidation", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "Humiliation", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "humiliated", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "Racism", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "Racist", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "discriminat*", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "equal employee", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "equal employees", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "equally employees", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "employees equals", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "treat equally", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "treated equally", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "treat equal", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "treat equaly", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "treat equals", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "unequal employee", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "unequal employees", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "unequally employees", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "treat unequally", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "treated unequally", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "treat unequal", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "treat unequals", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "treat fair", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "treat fairly", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "treated fairly", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "fair employees", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "staff fairly", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "Equal opportunities", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "treat same", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "treating same", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "treated same", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "treats same", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "treatment same", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "treat different", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "treating different", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "treated different", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "treats different", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "treatment different", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "preferential treat", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "preferential treated", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "preferential treating", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "preferential treats", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "preferential treatment", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "prefferential treatment", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "prefferential treat", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "prefferential treated", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "prefferential treats", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "prefferential treating", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "treat better", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "treats better", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "treated better", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "treating better", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "treatment better", WithinWords = 5 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "favoritism", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "favouritism", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "bully*", WithinWords = 0 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "care employee", WithinWords = 3 });
			//batch.BatchSearchWords.Add(new SearchWord() { Word = "care employees", WithinWords = 3 });

			//var orbatch = new SearchBatch();
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "fairness", WithinWords = 0 });
   //         var notinBatch = new SearchBatch();
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "salary fairness", WithinWords = 0 });
   //         orbatch.NotInWords.Add(notinBatch);
			//batch.OrSubSearchBatch.Add(orbatch);



   //         orbatch = new SearchBatch();
   //         orbatch.BatchSearchWords.Add(new SearchWord() { Word = "favorites", WithinWords = 0 });
   //         notinBatch = new SearchBatch();
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "my favorites", WithinWords = 0 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "the favorites", WithinWords = 0 });
   //         orbatch.NotInWords.Add(notinBatch);
   //         batch.OrSubSearchBatch.Add(orbatch);


   //         orbatch = new SearchBatch();
   //         orbatch.BatchSearchWords.Add(new SearchWord() { Word = "favor employee", WithinWords = 2 });
			//orbatch.BatchSearchWords.Add(new SearchWord() { Word = "favor employees", WithinWords = 2 });
   //         orbatch.BatchSearchWords.Add(new SearchWord() { Word = "not objective", WithinWords = 2 });
   //         orbatch.BatchSearchWords.Add(new SearchWord() { Word = "favor people", WithinWords = 3 });
   //         orbatch.BatchSearchWords.Add(new SearchWord() { Word = "favor over", WithinWords = 3 });
   //         orbatch.BatchSearchWords.Add(new SearchWord() { Word = "favor one", WithinWords = 0 });
   //         orbatch.BatchSearchWords.Add(new SearchWord() { Word = "favor certain", WithinWords = 0 });
   //         orbatch.BatchSearchWords.Add(new SearchWord() { Word = "she favor", WithinWords = 3 });
   //         orbatch.BatchSearchWords.Add(new SearchWord() { Word = "in favor of", WithinWords = 0 });
   //         orbatch.BatchSearchWords.Add(new SearchWord() { Word = "favor staff", WithinWords = 2 }); 

   //         notinBatch = new SearchBatch();
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "a favor", WithinWords = 2 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "doing favor", WithinWords = 3 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "do favor", WithinWords = 2 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "did favor", WithinWords = 2 });

   //         orbatch.NotInWords.Add(notinBatch);
   //         batch.OrSubSearchBatch.Add(orbatch);


   //         orbatch = new SearchBatch();
   //         orbatch.BatchSearchWords.Add(new SearchWord() { Word = "facilit*", WithinWords = 0 });

   //         notinBatch = new SearchBatch();
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "facilitator", WithinWords = 0 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "facilitating", WithinWords = 0 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "facilitate*", WithinWords = 0 });
   //         orbatch.NotInWords.Add(notinBatch);
   //         batch.OrSubSearchBatch.Add(orbatch);		

   //         orbatch = new SearchBatch();
   //         orbatch.BatchSearchWords.Add(new SearchWord() { Word = "storage", WithinWords = 0 });

   //         var andBatch = new SearchBatch();
   //         andBatch.BatchSearchWords.Add(new SearchWord() { Word = "space", WithinWords = 0 });
   //         andBatch.BatchSearchWords.Add(new SearchWord() { Word = "bigger", WithinWords = 0 });
   //         andBatch.BatchSearchWords.Add(new SearchWord() { Word = "larger", WithinWords = 0 });
   //         andBatch.BatchSearchWords.Add(new SearchWord() { Word = "room", WithinWords = 0 });
   //         orbatch.AndMustMatchWords.Add(notinBatch);
   //         batch.OrSubSearchBatch.Add(orbatch);		

   //         orbatch = new SearchBatch();
   //         orbatch.BatchSearchWords.Add(new SearchWord() { Word = "inventory", WithinWords = 0 });

   //         andBatch = new SearchBatch();
   //         andBatch.BatchSearchWords.Add(new SearchWord() { Word = "capacity", WithinWords = 0 });
   //         andBatch.BatchSearchWords.Add(new SearchWord() { Word = "room", WithinWords = 0 });
   //         orbatch.AndMustMatchWords.Add(andBatch);
   //         batch.OrSubSearchBatch.Add(orbatch);		

   //         orbatch = new SearchBatch();
   //         orbatch.BatchSearchWords.Add(new SearchWord() { Word = "tables", WithinWords = 0 });
   //         notinBatch = new SearchBatch();
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "excel tables", WithinWords = 0 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "play tables", WithinWords = 0 });
   //         orbatch.NotInWords.Add(notinBatch);
   //         batch.OrSubSearchBatch.Add(orbatch);		


   //         orbatch = new SearchBatch();
   //         orbatch.BatchSearchWords.Add(new SearchWord() { Word = "faster it", WithinWords = 0 });

   //         notinBatch = new SearchBatch();
   //         orbatch.BatchSearchWords.Add(new SearchWord() { Word = "fast it takes", WithinWords = 0 });
   //         orbatch.BatchSearchWords.Add(new SearchWord() { Word = "faster it shouldn't", WithinWords = 0 });
   //         orbatch.NotInWords.Add(notinBatch);
   //         batch.OrSubSearchBatch.Add(orbatch);		


   //         orbatch = new SearchBatch();
   //         orbatch.BatchSearchWords.Add(new SearchWord() { Word = "better it", WithinWords = 0 });

   //         notinBatch = new SearchBatch();
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "better it can", WithinWords = 0 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "better it would", WithinWords = 0 });
   //         orbatch.NotInWords.Add(notinBatch);
   //         batch.OrSubSearchBatch.Add(orbatch);		

            
   //         orbatch = new SearchBatch();
   //         orbatch.BatchSearchWords.Add(new SearchWord() { Word = "pc", WithinWords = 0 });
   //         notinBatch = new SearchBatch();
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "p&c", WithinWords = 0 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "pc training", WithinWords = 0 });
   //         orbatch.NotInWords.Add(notinBatch);
   //         batch.OrSubSearchBatch.Add(orbatch);		


   //         orbatch = new SearchBatch();
   //         orbatch.BatchSearchWords.Add(new SearchWord() { Word = "internet", WithinWords = 0 });

   //         notinBatch = new SearchBatch();
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "internet sites", WithinWords = 0 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "from Internet", WithinWords = 0 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "on internet", WithinWords = 0 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "Internet explorer", WithinWords = 0 });
   //         orbatch.NotInWords.Add(notinBatch);
   //         batch.OrSubSearchBatch.Add(orbatch);		

   //         orbatch = new SearchBatch();
   //         orbatch.BatchSearchWords.Add(new SearchWord() { Word = "security", WithinWords = 0 });

   //         notinBatch = new SearchBatch();
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "security job", WithinWords = 4 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "security employment", WithinWords = 2 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "security employees", WithinWords = 5 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "social security", WithinWords = 0 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "data security", WithinWords = 3 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "income security", WithinWords = 3 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "future security", WithinWords = 4 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "security role", WithinWords = 3 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "information security", WithinWords = 3 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "occupational security", WithinWords = 3 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "task security", WithinWords = 3 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "sense of security", WithinWords = 0 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "concept of security", WithinWords = 0 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "source of security", WithinWords = 0 });
   //         orbatch.NotInWords.Add(notinBatch);
   //         batch.OrSubSearchBatch.Add(orbatch);		

            
   //         orbatch = new SearchBatch();
   //         orbatch.BatchSearchWords.Add(new SearchWord() { Word = "schedule", WithinWords = 0 });

   //         notinBatch = new SearchBatch();
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "to schedule", WithinWords = 0 });
   //         orbatch.NotInWords.Add(notinBatch);
   //         batch.OrSubSearchBatch.Add(orbatch);	

            
   //         orbatch = new SearchBatch();
   //         orbatch.BatchSearchWords.Add(new SearchWord() { Word = "the hours", WithinWords = 0 });

   //         notinBatch = new SearchBatch();
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "the hours to", WithinWords = 0 });
   //         orbatch.NotInWords.Add(notinBatch);
   //         batch.OrSubSearchBatch.Add(orbatch);	

            
   //         orbatch = new SearchBatch();
   //         orbatch.BatchSearchWords.Add(new SearchWord() { Word = "*efficien*", WithinWords = 0 });
   //         orbatch.BatchSearchWords.Add(new SearchWord() { Word = "efficient", WithinWords = 1 }); 

   //         notinBatch = new SearchBatch();
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "tool*", WithinWords = 0 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "system*", WithinWords = 0 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "software", WithinWords = 0 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "machine", WithinWords = 0 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "PC", WithinWords = 0 });
   //         orbatch.NotInWords.Add(notinBatch);
   //         batch.OrSubSearchBatch.Add(orbatch);		

                
   //         orbatch = new SearchBatch();
   //         orbatch.BatchSearchWords.Add(new SearchWord() { Word = "effectiveness", WithinWords = 0 });

   //         notinBatch = new SearchBatch();
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "cost effectiveness", WithinWords = 0 });
   //         orbatch.NotInWords.Add(notinBatch);
   //         batch.OrSubSearchBatch.Add(orbatch);	

   //         orbatch = new SearchBatch();
   //         orbatch.BatchSearchWords.Add(new SearchWord() { Word = "atmosphere", WithinWords = 0 });
   //         orbatch.BatchSearchWords.Add(new SearchWord() { Word = "morals", WithinWords = 0 });
   //         orbatch.BatchSearchWords.Add(new SearchWord() { Word = "climate", WithinWords = 0 });
   //         orbatch.BatchSearchWords.Add(new SearchWord() { Word = "work culture", WithinWords = 0 });
   //         orbatch.BatchSearchWords.Add(new SearchWord() { Word = "working culture", WithinWords = 0 });
   //         orbatch.BatchSearchWords.Add(new SearchWord() { Word = "culture", WithinWords = 0 });
   //         orbatch.BatchSearchWords.Add(new SearchWord() { Word = "employee relationship", WithinWords = 0 });
   //         orbatch.BatchSearchWords.Add(new SearchWord() { Word = "employee friendly", WithinWords = 3 }); 

   //         notinBatch = new SearchBatch();
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "indoor climate", WithinWords = 0 });
   //         orbatch.NotInWords.Add(notinBatch);
   //         batch.OrSubSearchBatch.Add(orbatch);		


   //         orbatch = new SearchBatch();
   //         orbatch.BatchSearchWords.Add(new SearchWord() { Word = "integrity", WithinWords = 0 });
   //         notinBatch = new SearchBatch();
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "data integrity", WithinWords = 0 });
   //         orbatch.NotInWords.Add(notinBatch);
   //         batch.OrSubSearchBatch.Add(orbatch);	

   //         orbatch = new SearchBatch();
   //         orbatch.BatchSearchWords.Add(new SearchWord() { Word = "honest", WithinWords = 0 });
   //         notinBatch = new SearchBatch();
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "open honest", WithinWords = 3 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "honest communication", WithinWords = 2 });
   //         orbatch.NotInWords.Add(notinBatch);
   //         batch.OrSubSearchBatch.Add(orbatch);		

   //         orbatch = new SearchBatch();
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "honesty", WithinWords = 3 });

   //         notinBatch = new SearchBatch();
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "openness honesty", WithinWords = 3 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "honesty communication", WithinWords = 2 });
   //         orbatch.NotInWords.Add(notinBatch);
   //         batch.OrSubSearchBatch.Add(orbatch);		


   //         orbatch = new SearchBatch();
   //         orbatch.BatchSearchWords.Add(new SearchWord() { Word = "diversity", WithinWords = 0 });

   //         notinBatch = new SearchBatch();
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "diversity products", WithinWords = 2 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "diversity work", WithinWords = 2 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "job diversity", WithinWords = 0 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "diversity job", WithinWords = 3 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "work diversity", WithinWords = 0 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "diversity projects", WithinWords = 2 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "diversity problems", WithinWords = 2 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "diversity snacks", WithinWords = 0 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "diversity activities", WithinWords = 2 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "diversity tasks", WithinWords = 2 });
   //         orbatch.NotInWords.Add(notinBatch);
   //         batch.OrSubSearchBatch.Add(orbatch);		

   //         orbatch = new SearchBatch();
   //         orbatch.BatchSearchWords.Add(new SearchWord() { Word = "diverse", WithinWords = 0 });

   //         notinBatch = new SearchBatch();
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "diverse products", WithinWords = 2 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "diverse work", WithinWords = 2 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "job diverse", WithinWords = 0 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "diverse job", WithinWords = 3 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "work diverse", WithinWords = 0 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "diverse projects", WithinWords = 2 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "diverse problems", WithinWords = 2 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "diverse snacks", WithinWords = 0 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "diverse activities", WithinWords = 2 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "diverse tasks", WithinWords = 2 });
   //         orbatch.NotInWords.Add(notinBatch);
   //         batch.OrSubSearchBatch.Add(orbatch);		

            
   //         orbatch = new SearchBatch();
   //         orbatch.BatchSearchWords.Add(new SearchWord() { Word = "diversified", WithinWords = 0 });
   //         notinBatch = new SearchBatch();
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "diversified products", WithinWords = 2 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "diversified work", WithinWords = 2 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "job diversified", WithinWords = 0 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "diversified job", WithinWords = 3 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "work diversified", WithinWords = 0 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "diversified projects", WithinWords = 2 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "diversified problems", WithinWords = 2 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "diversified snacks", WithinWords = 0 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "diversified activities", WithinWords = 2 });
   //         notinBatch.BatchSearchWords.Add(new SearchWord() { Word = "diversified tasks", WithinWords = 2 });
   //         orbatch.NotInWords.Add(notinBatch);
   //         batch.OrSubSearchBatch.Add(orbatch);		

   //         cat.Batches.Add(batch);
			//return cat;

   //     }
    }
}
