﻿namespace Ennova.CategorizationService.Service.Services
{
    public interface ITableService
    {
        void Insert(Comment.Model.Comment model);
        int GetAmountOfRequestToTextAnalyseService();
        void AddRequestThatTextAnalyseServiceHasBeenUsed();
    }
}