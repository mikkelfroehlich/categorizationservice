﻿using System;
using System.Collections.Generic;
using System.Linq;
using Azure;
using Azure.Data.Tables;
using Ennova.Comment.Model;


namespace Ennova.CategorizationService.Service.Services
{
    public class TableService : ITableService
    {
        private TableClient _tableClient;
        private readonly string partitionKey = "917E89C8-1293-4BA3-AB1E-C942844D2576";

        public TableService(TableClient tableClient)
        {
            _tableClient = tableClient;
        }

        public void Insert(Comment.Model.Comment model)
        {
            TableEntity entity = null;

            foreach (SentenceText sentenceText in model.Sentence)
            {
                entity = new TableEntity();
                entity.PartitionKey = model.Id.ToString();
                entity["sentence"] = sentenceText.Text;

                foreach (string category in sentenceText.Categories)
                {
                    entity.RowKey = Guid.NewGuid().ToString();
                    entity["category"] = category.Split(' ')[1].ToString();
                    entity["categoryID"] = category.Split(' ')[0].ToString();
                    entity["OverAllSentiment"] = sentenceText.Score.OverAllSentiment.ToString();
                    entity["PositiveScore"] = sentenceText.Score.PositiveScore.ToString();
                    entity["NeutralScore"] = sentenceText.Score.NeutralScore.ToString();
                    entity["NegativeScore"] = sentenceText.Score.NegativeScore.ToString();
                    Console.WriteLine("-------> Inserted id:" + model.Id.ToString());
                    _tableClient.AddEntity(entity);

                }
            }
        }

        public int GetAmountOfRequestToTextAnalyseService()
        {
            List<DateTimeOffset> requestList = new List<DateTimeOffset>();
            List<TableEntity> dbEntries = new List<TableEntity>();

            if (_tableClient != null)
            {
                Pageable<TableEntity> oDataQueryEntities = _tableClient.Query<TableEntity>(filter: TableClient.CreateQueryFilter($"PartitionKey eq {partitionKey}"));

                foreach (TableEntity entity in oDataQueryEntities)
                {
                    DateTimeOffset entryDatetime;
                    if (DateTimeOffset.TryParse(entity.GetDateTimeOffset("Timestamp").ToString(), out entryDatetime))
                    {
                        requestList.Add(entryDatetime);
                        dbEntries.Add(entity);
                    }
                }
            }
            
            CleanUp(dbEntries);

            return requestList.Count(x => x > DateTime.UtcNow.AddSeconds(-60));
        }

        public void AddRequestThatTextAnalyseServiceHasBeenUsed()
        {
            TableEntity entity = null;
            entity = new TableEntity();
            entity.PartitionKey = partitionKey;
            entity.RowKey = Guid.NewGuid().ToString();
            entity["Request"] = 1;
            _tableClient.AddEntity(entity);
        }


        public void CleanUp(List<TableEntity> dbEntities)
        {
            if (_tableClient != null)
            {
                foreach (TableEntity entity in dbEntities)
                {
                    DateTimeOffset entryDatetime;
                    if (DateTimeOffset.TryParse(entity.GetDateTimeOffset("Timestamp").ToString(), out entryDatetime))
                    {
                        if (entryDatetime > DateTimeOffset.UtcNow.AddMinutes(-5))
                        {
                            _tableClient.DeleteEntity(entity.PartitionKey, entity.RowKey);
                        }
                    }
                }
            }
        }
    }
}