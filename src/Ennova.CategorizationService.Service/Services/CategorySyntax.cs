﻿using System.Collections.Generic;
using Ennova.CategorizationService.Service.Data.Categories;
using Ennova.CategorizationService.Service.Model;

namespace Ennova.CategorizationService.Service.Services
{
    public interface ICategorySyntax
    {
        List<Category> GetAllCategories();
        Category CategoryOther();
        Category CategoryNonsence();
    }

    public class CategorySyntax : ICategorySyntax
    {

        public List<Category> GetAllCategories()
        {
            List<Category> cats = new List<Category>();
            
            var cat = new Cooperation();
            cats.Add(cat.GetCategory());

            var learningDevelopment = new LearningDevelopment();
            cats.Add(learningDevelopment.GetCategory());

            var immediateManager = new ImmediateManager();
            cats.Add(immediateManager.GetCategory());

            var remuneration = new Remuneration();
            cats.Add(remuneration.GetCategory());

            var reputation = new Reputation();
            cats.Add(reputation.GetCategory());

            var seniorManagement = new SeniorManagement();
            cats.Add(seniorManagement.GetCategory());

            var theJob = new TheJob();
            cats.Add(theJob.GetCategory());

            var workingConditions = new WorkingConditions();
            cats.Add(workingConditions.GetCategory());

            var competitors = new Competitors();
            cats.Add(competitors.GetCategory());

            var customers = new Customers();
            cats.Add(customers.GetCategory());

            var dedication = new Dedication();
            cats.Add(dedication.GetCategory());

            var faithfulness = new Faithfulness();
            cats.Add(faithfulness.GetCategory());

            var motivation = new Motivation();
            cats.Add(motivation.GetCategory());

            var satisfaction = new Satisfaction();
            cats.Add(satisfaction.GetCategory());


            return cats;
        }

        public Category CategoryOther()
        {
            Other otherCategory = new Other();
            return otherCategory.GetCategory();
        }

        public Category CategoryNonsence()
        {
            Nonsense nonsenseCategoory = new Nonsense();
            return nonsenseCategoory.GetCategory();
        }


        public Category CategoryNonsense()
        {
            Nonsense categoryNonsense = new Nonsense();
            return categoryNonsense.GetCategory();
        }
    }
}