﻿using System.Collections.Generic;
using Ennova.CategorizationService.Service.Model;
using Ennova.Comment.Model;

namespace Ennova.CategorizationService.Service.Services
{
    public class CommentsService : ICommentsService
    {
        private readonly ICategorySyntax _categorySyntax;

        public CommentsService(ICategorySyntax categorySyntax)
        {
            _categorySyntax = categorySyntax;
        }

        public Comment.Model.Comment Categorize(Comment.Model.Comment comment)
        {
            List<SentenceText> sentenceList = new List<SentenceText>();

            foreach (SentenceText text in comment.Sentence)
            {
                sentenceList.Add(CategorizeSentence(text));
            }

            comment.Sentence = sentenceList;
            return comment;
        }

        private SentenceText CategorizeSentence(SentenceText text)
        {
            Category nonsense = _categorySyntax.CategoryOther();

            if (text.IsPhraseOfWords)
            {
                List<Category> categories = _categorySyntax.GetAllCategories();
                Category other = _categorySyntax.CategoryOther();

                foreach (Category category in categories)
                {
                    if (category.IsCategoryInText(text.ClearText))
                    {
                        text.Categories.Add(category.Id + " " + category.Name);
                    }
                }

                if (text.Categories.Count == 0)
                {
                    text.Categories.Add(other.Id + " " + other.Name);
                }
            }
            else
            {
                text.Categories.Add(nonsense.Id + " " + nonsense.Name);
            }

            return text;


        }
    }
}