﻿using Ennova.Comment.Model;

namespace Ennova.CategorizationService.Service.Services
{
    public interface ICommentsService
    {
        Comment.Model.Comment Categorize(Comment.Model.Comment comment);
    }
}