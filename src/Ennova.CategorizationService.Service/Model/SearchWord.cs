﻿using System;

namespace Ennova.CategorizationService.Service.Model
{
    public class SearchWord
    {
        public string Word { get; set; }
        public int WithinWords { get; set; }
        public bool HasWildCard => Word.Contains("*");
        public bool HasQuestionMark => Word.Contains("?");
        public bool StartWithWildCard => Word.StartsWith("*") && !Word.EndsWith("*");
        public bool EndWithWildCard => !Word.StartsWith("*") && Word.EndsWith("*");
        public bool StartAndEndWithWildCard => Word.StartsWith("*") && Word.EndsWith("*");
        public bool WithOutWildCardAndQuestionsMark => !HasWildCard && !HasQuestionMark;
        public string CleanWord => Word.Replace("*", "").Replace("?", "");

        public bool CompaireWithWord(string text)
        {
            return Word.Equals(text, StringComparison.InvariantCultureIgnoreCase);
        }

        public bool ContainsInClearWord(string text)
        {
            return CleanWord.Contains(text);
        }
    }
}