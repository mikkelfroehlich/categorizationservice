﻿using System.Collections.Generic;

namespace Ennova.CategorizationService.Service.Model
{
    public class SearchBatch
    {
        public SearchBatch()
        {
            BatchSearchWords = new List<SearchWord>();
            OrSubSearchBatch = new List<SearchBatch>();
            AndMustMatchWords = new List<SearchBatch>();
            NotInWords = new List<SearchBatch>();
        }

        public List<SearchWord> BatchSearchWords { get; set; }
        public List<SearchBatch> OrSubSearchBatch { get; set; }
        public List<SearchBatch> AndMustMatchWords { get; set; }
        public List<SearchBatch> NotInWords { get; set; }

        public void AddSearchWord(string word, int withinWords)
        {
            BatchSearchWords.Add(new SearchWord() { Word = word, WithinWords = withinWords });
        }

        public void AddRangeSearchWords(List<string> wordsList, int withInWords)
        {
            foreach (string zeroWordBetween in wordsList)
            {
                AddSearchWord(zeroWordBetween, withInWords);
            }
        }

        public void AddRangeSearchWords(string[] wordsList, int withInWords)
        {
            foreach (string zeroWordBetween in wordsList)
            {
                AddSearchWord(zeroWordBetween, withInWords);
            }
        }
    }
}