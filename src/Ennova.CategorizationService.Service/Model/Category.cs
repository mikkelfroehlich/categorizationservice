﻿using System;
using System.Collections.Generic;

namespace Ennova.CategorizationService.Service.Model
{
    public class Category 
    {
        public Category()
        {
            Batches = new List<SearchBatch>();
        }

        public string Name { get; set; }
        public string Id { get; set; }
        public int MinimumMatches { get; set; }
        public List<SearchBatch> Batches { get; set; }
        public SearchBatch GlobalBatch { get; set; }


        public bool IsCategoryInText(string text)
        {
            char[] delimiterChars = { ' ' };
            string[] singleWords = text.Split(delimiterChars, StringSplitOptions.RemoveEmptyEntries);

            if (GlobalBatch != null)
            {
                if (IsCommentInList(singleWords, GlobalBatch.NotInWords, false))
                {
                    return false;
                }
            }

            foreach (var batch in Batches)
            {
                if (IsCommentInBatchTrue(singleWords, batch))
                {
                    return true;
                }
            }

            return false;
        }

        private bool IsCommentInList(string[] singleWords, List<SearchBatch> listOfSearchBatches, bool defaultValueIfListIsEmpty)
        {
            if (listOfSearchBatches.Count == 0)
            {
                return defaultValueIfListIsEmpty;
            }

            foreach (SearchBatch searchBatch in listOfSearchBatches)
            {
                if (IsCommentInBatchTrue(singleWords, searchBatch))
                {
                    return true;
                }
            }

            return false;
        }

        private bool IsCommentInBatchTrue(string[] singleWords, SearchBatch searchBatch)
        {
            var wordsFound = IsSearchWordsFound(singleWords, searchBatch.BatchSearchWords);
            var orWordsFound = IsCommentInList(singleWords, searchBatch.OrSubSearchBatch, false);
            var andWords = IsCommentInList(singleWords, searchBatch.AndMustMatchWords, true);
            var notInWords = IsCommentInList(singleWords, searchBatch.NotInWords, false);

            if ((wordsFound || orWordsFound) && andWords && !notInWords)
            {
                return true;
            }

            return false;
        }

        private bool IsSearchWordsFound(string[] singleWords, List<SearchWord> listOfWords)
        {
            if (listOfWords.Count == 0)
            {
                return false;
            }

            foreach (SearchWord searchForWord in listOfWords)
            {
                if (searchForWord.Word.Length - searchForWord.Word.Replace(" ", "").Length == 0)
                {
                    // don't make sence. one word can't be within words. Reset within parametre    
                    searchForWord.WithinWords = 0;
                }

                if (searchForWord.WithinWords == 0 && searchForWord.Word.Length - searchForWord.Word.Replace(" ", "").Length == 0)
                {
                    //if (IndexOfTextContainedInArray(singleWords, searchForWord.Word, 0) > -1)
                    if (IndexOfTextContainedInArray(singleWords, searchForWord, 0) > -1)
                    {
                        return true;
                    }
                }
                else
                {
                    if (AreWordsWithinLimit(singleWords, searchForWord))
                    {
                        return true;
                    }

                    if (searchForWord.WithinWords > 0)
                    {
                        // ConfirmIT also search search words in reverse order with ~1
                        searchForWord.WithinWords = 1;
                        searchForWord.Word = ReserveWordOrder(searchForWord.Word);
                        if (AreWordsWithinLimit(singleWords, searchForWord))
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        private int IndexOfTextContainedInArray(string[] texts, SearchWord searchText, int searchAfterIndex)
        {
            int startIndex = searchAfterIndex > 0 ? searchAfterIndex : 0;

            for (int i = startIndex; i < texts.Length; i++)
            {
                if (!searchText.HasWildCard && !searchText.HasQuestionMark)
                {
                    if (searchText.CompaireWithWord(texts[i]))
                    {
                        return i;
                    }
                }

                if (searchText.HasWildCard)
                {
                    if (!searchText.StartWithWildCard && searchText.EndWithWildCard)
                    {
                        if (searchText.CompaireWithWord(texts[i]))
                        {
                            return i;
                        }
                    }

                    if (searchText.StartWithWildCard && !searchText.EndWithWildCard)
                    {
                        if (texts[i].EndsWith(searchText.CleanWord, StringComparison.InvariantCultureIgnoreCase))
                        {
                            return i;
                        }
                    }

                    if (searchText.StartWithWildCard && searchText.EndWithWildCard)
                    {
                        if (searchText.ContainsInClearWord(texts[i]))
                        {
                            return i;
                        }
                    }
                }

                if (searchText.HasQuestionMark)
                {
                    string firstString = searchText.Word.Substring(0, searchText.Word.IndexOf("?"));
                    string lastString = searchText.Word.Substring(searchText.Word.IndexOf("?") + 1, searchText.Word.Length - searchText.Word.IndexOf("?") - 1);

                    if (texts[i].StartsWith(firstString, StringComparison.InvariantCultureIgnoreCase) && texts[i].EndsWith(lastString, StringComparison.InvariantCultureIgnoreCase) && texts[i].Length == searchText.Word.Length)
                    {
                        return i;
                    }
                }

            }
            return -1;
        }

        public int IndexOfTextContainedInArray(string[] texts, string searchText, int searchAfterIndex)
        {
            int startIndex = searchAfterIndex > 0 ? searchAfterIndex : 0;
            bool hasStar = searchText.Contains("*");
            bool hasQuestionMark = searchText.Contains("?");

            var cleanText = searchText.Replace("*", "");


            for (int i = startIndex; i < texts.Length; i++)
            {
                if (!hasStar && !hasQuestionMark)
                {
                    if (texts[i].Equals(searchText, StringComparison.InvariantCultureIgnoreCase))
                    {
                        return i;
                    }
                }
                else
                {
                    if (hasStar)
                    {
                        if (!searchText.StartsWith("*") && searchText.EndsWith("*"))
                        {
                            if (texts[i].StartsWith(cleanText, StringComparison.InvariantCultureIgnoreCase))
                            {
                                return i;
                            }
                        }
                        else
                        {
                            if (searchText.StartsWith("*") && !searchText.EndsWith("*"))
                            {
                                if (texts[i].EndsWith(cleanText, StringComparison.InvariantCultureIgnoreCase))
                                {
                                    return i;
                                }
                            }
                            else
                            {
                                if (searchText.StartsWith("*") && searchText.EndsWith("*"))
                                {
                                    if (texts[i].Contains(cleanText))
                                    {
                                        return i;
                                    }
                                }
                            }
                        }
                    }

                    if (hasQuestionMark)
                    {
                        string firstString = searchText.Substring(0, searchText.IndexOf("?"));
                        string lastString = searchText.Substring(searchText.IndexOf("?") + 1, searchText.Length - searchText.IndexOf("?") - 1);

                        if (texts[i].StartsWith(firstString, StringComparison.InvariantCultureIgnoreCase) && texts[i].EndsWith(lastString, StringComparison.InvariantCultureIgnoreCase) && texts[i].Length == searchText.Length)
                        {
                            return i;
                        }
                    }
                }
            }
            return -1;
        }

        private bool AreWordsWithinLimit(string[] singleWords, SearchWord searchForWord)
        {
            char[] delimiterChars = { ' ' };
            string[] searchWords = searchForWord.Word.Split(delimiterChars);

            int index1 = IndexOfTextContainedInArray(singleWords, searchWords[0], 0);
            while (index1 > -1)
            {
                bool proceed = true;
                int i = 1;
                int currentIndex = index1;
                while (i <= searchWords.Length - 1 && proceed)
                {
                    int indexNext = IndexOfTextContainedInArray(singleWords, searchWords[i], currentIndex);
                    if (indexNext > -1)
                    {
                        if (!((indexNext - index1) - (searchWords.Length - 1) <= searchForWord.WithinWords))
                        {
                            proceed = false;
                        }
                        else
                        {
                            if (i == searchWords.Length - 1)
                            {
                                return true;
                            }
                        }
                    }
                    else
                    {
                        break;
                    }

                    currentIndex = indexNext;
                    i++;
                }

                index1 = IndexOfTextContainedInArray(singleWords, searchWords[0], index1 + 1);
            }

            return false;
        }

        private string ReserveWordOrder(string words)
        {
            string result = "";
            List<string> wordsList = new List<string>();
            wordsList.Reverse();
            foreach (var word in wordsList)
            {
                result += word;
            }

            //string[] arrayOfWords = words.Split(new[] { " " }, StringSplitOptions.None);
            //for (int i = arrayOfWords.Length - 1; i >= 0; i--)
            //{
            //    result += arrayOfWords[i] + " ";
            //}

            return result;
        }
    }
}