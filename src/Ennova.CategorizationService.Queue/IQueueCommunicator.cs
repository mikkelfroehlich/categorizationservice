﻿using System.Threading.Tasks;
using Ennova.CategorizationService.Queue.Message;

namespace Ennova.CategorizationService.Queue
{
    public interface IQueueCommunicator
    {
        T Read<T>(string message);
        Task SendAsync<T>(T tObject) where T: BaseQueueMessage;
    }
}