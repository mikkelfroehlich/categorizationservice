﻿using Ennova.CategorizationService.Queue.Config;
using Ennova.CategorizationService.Queue.Factory;
using Ennova.CategorizationService.Queue.Message.Serializer;
using Microsoft.Extensions.DependencyInjection;

namespace Ennova.CategorizationService.Queue
{
    public static class CommentQueueDependencyInjectionRegistry
    {
        public static IServiceCollection AddSentimentQueue(this IServiceCollection service, string sentimentQueueConnectionString)
        {
            service.AddSingleton(new QueueConfig(sentimentQueueConnectionString));
            service.AddServicesCollection();
            return service;
        }

        public static IServiceCollection AddTextQueue(this IServiceCollection service, string textQueueConnectionString)
        {
            service.AddSingleton(new QueueConfig(textQueueConnectionString));
            service.AddServicesCollection();
            return service;
        }

        private static IServiceCollection AddServicesCollection(this IServiceCollection service)
        {
            service.AddSingleton<IQueueClientFactory, QueueClientFactory>();
            service.AddSingleton<IQueueMessageSerializer, JsonSerializer>();
            service.AddTransient<IQueueCommunicator, QueueCommunicator>();
            return service;
        }
    }
}