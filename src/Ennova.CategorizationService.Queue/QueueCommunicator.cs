﻿using System.Threading.Tasks;
using Ennova.CategorizationService.Queue.Factory;
using Ennova.CategorizationService.Queue.Message;
using Ennova.CategorizationService.Queue.Message.Serializer;
using Microsoft.WindowsAzure.Storage.Queue;

namespace Ennova.CategorizationService.Queue
{
    public class QueueCommunicator : IQueueCommunicator
    {
        private readonly IQueueMessageSerializer _serializer;
        private readonly IQueueClientFactory _clientFactory;

        public QueueCommunicator(IQueueMessageSerializer serializer, IQueueClientFactory clientFactory)
        {
            _serializer = serializer;
            _clientFactory = clientFactory;
        }
        public T Read<T>(string message)
        {
            return _serializer.Deserializer<T>(message);
        }

        public async Task SendAsync<T>(T tObject) where T : BaseQueueMessage
        {
            var queueReference = _clientFactory.GetClient().GetQueueReference(tObject.Route);
            await queueReference.CreateIfNotExistsAsync();

            var serializeMessage = _serializer.Serializer(tObject);
            var queueMessage = new CloudQueueMessage(serializeMessage);

            await queueReference.AddMessageAsync(queueMessage);
        }
    }
}