﻿using Microsoft.WindowsAzure.Storage.Queue;

namespace Ennova.CategorizationService.Queue.Factory
{
    public interface IQueueClientFactory
    {
        CloudQueueClient GetClient();
    }
}