﻿using Ennova.CategorizationService.Queue.Config;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;

namespace Ennova.CategorizationService.Queue.Factory
{
    public class QueueClientFactory : IQueueClientFactory
    {

        private readonly QueueConfig _queueConfig;
        private CloudQueueClient _client;
        public QueueClientFactory(QueueConfig queueConfig)
        {
            _queueConfig = queueConfig;
        }
        public CloudQueueClient GetClient()
        {
            if (_client != null)
            {
                return _client;
            }

            var storeageAccount = CloudStorageAccount.Parse(_queueConfig.ConnectionString);
            _client = storeageAccount.CreateCloudQueueClient();
            return _client;
        }
    }


    //public class TextQueueClientFactory : IQueueClientFactory
    //{

    //    private readonly TextQueueConfig _textQueueConfig;
    //    private CloudQueueClient _client;
    //    public TextQueueClientFactory(TextQueueConfig textQueueConfig)
    //    {
    //        _textQueueConfig = textQueueConfig;
    //    }
    //    public CloudQueueClient GetClient()
    //    {
    //        if (_client != null)
    //        {
    //            return _client;
    //        }

    //        var storeageAccount = CloudStorageAccount.Parse(_textQueueConfig.ConnectionString);
    //        _client = storeageAccount.CreateCloudQueueClient();
    //        return _client;
    //    }
    //}
}