﻿using Newtonsoft.Json;

namespace Ennova.CategorizationService.Queue.Message.Serializer
{
    public interface IQueueMessageSerializer
    {
        T Deserializer<T>(string message);
        string Serializer(object objectMessage);
    }


    public class JsonSerializer : IQueueMessageSerializer
    {
        public T Deserializer<T>(string message)
        {
            var messageDeserialized = JsonConvert.DeserializeObject<T>(message);
            return messageDeserialized;
        }

        public string Serializer(object objectMessage)
        {
            var json = JsonConvert.SerializeObject(objectMessage);
            return json;
        }
    }
}