﻿namespace Ennova.CategorizationService.Queue.Message
{
    public abstract class BaseQueueMessage
    {
        public BaseQueueMessage(string route)
        {
            Route = route;
        }

        public string Route { get; set; }
    }
}