﻿using System;
using Ennova.CategorizationService.Queue.Config;

namespace Ennova.CategorizationService.Queue.Message
{
    public class CommentMessageCommand : BaseQueueMessage
    {
        public CommentMessageCommand() : base(RouteConfig.RouteSentimentQueueName)
        {

        }


        public string Comment{ get; set; }
        public Guid Id { get; set; }

    }

    public class TextMessageCommand : BaseQueueMessage
    {
        public TextMessageCommand() : base(RouteConfig.RouteTextQueueName)
        {

        }


        public string Comment { get; set; }
        public Guid Id { get; set; }

    }



    public class SentimentMessageCommand : BaseQueueMessage
    {
        public SentimentMessageCommand() : base(RouteConfig.RouteSentimentQueueName)
        {

        }


        public Comment.Model.Comment Comment { get; set; }

    }
}