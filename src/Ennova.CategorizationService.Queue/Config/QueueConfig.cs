﻿namespace Ennova.CategorizationService.Queue.Config
{
    public class QueueConfig
    {
        public QueueConfig(string connectionString)
        {
            ConnectionString = connectionString;
        }

        public string ConnectionString { get; set; }

    }

}