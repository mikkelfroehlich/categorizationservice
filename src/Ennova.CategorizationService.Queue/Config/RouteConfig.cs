﻿namespace Ennova.CategorizationService.Queue.Config
{
    public class RouteConfig
    {

        public const string RouteTextQueueName = "text-queue-ready-for-sentiment";
        public const string RouteSentimentQueueName = "sentiment-queue-ready-for-category-analyze";
    }
}