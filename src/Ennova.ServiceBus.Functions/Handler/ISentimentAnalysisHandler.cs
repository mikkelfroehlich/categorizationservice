﻿using System.Threading.Tasks;
using Azure.Messaging.ServiceBus;

namespace Ennova.ServiceBus.Functions.Handler
{
    public interface ISentimentAnalysisHandler
    {
        Task<ServiceBusReceivedMessage> AnalysisText(ServiceBusReceivedMessage message);
        Task<ServiceBusReceivedMessage[]> AnalysisTextBatch(ServiceBusReceivedMessage[] messages);
    }
}