﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Azure.Messaging.ServiceBus;
using Ennova.CategorizationService.Queue;
using Ennova.CategorizationService.Queue.Message;
using Ennova.CategorizationService.Queue.Message.Serializer;
using Ennova.Comment.Dto;
using Ennova.Comment.Model;
using Ennova.SentimentService.Service.Services;

namespace Ennova.ServiceBus.Functions.Handler
{
    public class SentimentAnalysisHandler : ISentimentAnalysisHandler
    {
        private readonly ISentimentAnalysisService _sentimentAnalysisService;
        private readonly IQueueMessageSerializer _queueMessageSerializer;
        private readonly IQueueCommunicator _queueCommunicator;
        private const int BatchSize = 10;

        public SentimentAnalysisHandler(ISentimentAnalysisService sentimentAnalysisService, IQueueMessageSerializer queueMessageSerializer ,IQueueCommunicator queueCommunicator)
        {
            _sentimentAnalysisService = sentimentAnalysisService;
            _queueMessageSerializer = queueMessageSerializer;
            _queueCommunicator = queueCommunicator;
        }


        public async Task<ServiceBusReceivedMessage> AnalysisText(ServiceBusReceivedMessage message)
        {
            var simpleComment = _queueMessageSerializer.Deserializer<SimpleComment>(message.Body.ToString());
            SimpleCommentMessageDto simpleCommentDto = new SimpleCommentMessageDto
            {
                ServiceBusReceivedMessageId = message.MessageId,
                SimpleComment = simpleComment
            };

            CommentMessageDto commentDto = await _sentimentAnalysisService.AnalyzeSentiment(simpleCommentDto);

            if (commentDto == null)
            {
                return null;
            }

            await AddItemToQueue(commentDto.Comment);
            
            return message;
        }

        public async Task<ServiceBusReceivedMessage[]> AnalysisTextBatch(ServiceBusReceivedMessage[] messages)
        {
            List<SimpleCommentMessageDto> simpleCommentDtoList = MapToCommentList(messages);

            List<CommentMessageDto> commentList = await RunBatchSentimentAnalysic(simpleCommentDtoList);

            await AddBatchToQueue(commentList);

            List<ServiceBusReceivedMessage> serviceBusReceivedMessages = FindTheMessagesThatHasBeenAnalysed(commentList, messages);
            
            return serviceBusReceivedMessages.ToArray();
        }

        private List<ServiceBusReceivedMessage> FindTheMessagesThatHasBeenAnalysed(List<CommentMessageDto> comments, ServiceBusReceivedMessage[] messages)
        {
            List<ServiceBusReceivedMessage> serviceBusReceivedMessages = new List<ServiceBusReceivedMessage>();

            foreach (var commet in comments)
            {
                var messageToComplete = messages.Where(x => x.MessageId == commet.ServiceBusReceivedMessageId);
                if (messageToComplete != null || messageToComplete.Any())
                {
                    serviceBusReceivedMessages.Add(messageToComplete.First());
                }
            }

            return serviceBusReceivedMessages;
        }

        private List<SimpleCommentMessageDto> MapToCommentList(ServiceBusReceivedMessage[] messages)
        {
            List<SimpleCommentMessageDto> simpleCommentList = new List<SimpleCommentMessageDto>();

            foreach (ServiceBusReceivedMessage message in messages)
            {
                var comment = _queueMessageSerializer.Deserializer<SimpleComment>(message.Body.ToString());
                SimpleCommentMessageDto simpleCommentDto = new SimpleCommentMessageDto()
                {
                    ServiceBusReceivedMessageId = message.MessageId,
                    SimpleComment = comment
                };
                simpleCommentList.Add(simpleCommentDto);
            }

            return simpleCommentList;
        }

        private async Task AddBatchToQueue(List<CommentMessageDto> commentList)
        {
            foreach (CommentMessageDto comment in commentList)
            {
                await AddItemToQueue(comment.Comment);
            }
        }
        
        private async Task AddItemToQueue(Comment.Model.Comment comment)
        {
            try
            {
                await _queueCommunicator.SendAsync(new SentimentMessageCommand { Comment = comment });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private async Task<List<CommentMessageDto>> RunBatchSentimentAnalysic(List<SimpleCommentMessageDto> simpleCommentDtoList)
        {
            List<SimpleCommentMessageDto> batchReadyForSentimentAnalyze = new List<SimpleCommentMessageDto>();
            List<CommentMessageDto> commenDtotList = new List<CommentMessageDto>();

            int countAll = 0;
            foreach (SimpleCommentMessageDto sC in simpleCommentDtoList)
            {
                countAll++;
                if (batchReadyForSentimentAnalyze.Count <= BatchSize)
                {
                    batchReadyForSentimentAnalyze.Add(sC);

                    if (batchReadyForSentimentAnalyze.Count == BatchSize)
                    {
                        var analyzedMessages = await _sentimentAnalysisService.AnalyzeSentimentBatch(batchReadyForSentimentAnalyze);
                        commenDtotList.AddRange(analyzedMessages);
                        batchReadyForSentimentAnalyze = new List<SimpleCommentMessageDto>();
                    }
                }

                if (countAll == simpleCommentDtoList.Count)
                {
                    var analyzedMessages = await _sentimentAnalysisService.AnalyzeSentimentBatch(batchReadyForSentimentAnalyze);
                    commenDtotList.AddRange(analyzedMessages);
                }
            }
            return commenDtotList;
        }
    }
}