using System.Linq;
using System.Threading.Tasks;
using Azure.Messaging.ServiceBus;
using Ennova.ServiceBus.Functions.Handler;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.ServiceBus;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Ennova.ServiceBus.Functions
{
    public static class Function1
    {
        [FunctionName("Function1")]
        public static async Task  Run([ServiceBusTrigger("text-content-servicebus-queue", Connection = "ConnectionStringSettingName")] ServiceBusReceivedMessage[] messages, ServiceBusMessageActions messageActions, ILogger logger)
        {
            var handler = SentimentAnalysisConfigureServices.Instance.GetService<ISentimentAnalysisHandler>();


            if (messages.Length == 1)
            {
                var message = await handler.AnalysisText(messages[0]);
                if (message != null)
                {
                    await messageActions.CompleteMessageAsync(message);
                }
                else
                {
                    await messageActions.DeadLetterMessageAsync(messages[0]);
                }
            }
            else
            {
                var messagesToComplete = await handler.AnalysisTextBatch(messages);
                if (messagesToComplete.Any())
                {
                    foreach (ServiceBusReceivedMessage message in messagesToComplete)
                    {
                        await messageActions.CompleteMessageAsync(message);
                    }
                }
            }
        }
    }
}
