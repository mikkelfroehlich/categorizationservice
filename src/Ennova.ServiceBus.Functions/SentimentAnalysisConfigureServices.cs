﻿using System;
using System.IO;
using Azure;
using Azure.Data.Tables;
using Ennova.CategorizationService.Queue;
using Ennova.CategorizationService.Queue.Message.Serializer;
using Ennova.CategorizationService.Service.Services;
using Ennova.SentimentService.Service.Services;
using Ennova.ServiceBus.Functions.Handler;
using Microsoft.Extensions.Azure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Ennova.ServiceBus.Functions
{
    public sealed class SentimentAnalysisConfigureServices
    {
        private static readonly IServiceProvider _serviceProvider = Build();
        public static IServiceProvider Instance => _serviceProvider;

        static SentimentAnalysisConfigureServices()
        {

        }

        private SentimentAnalysisConfigureServices()
        {

        }

        private static IServiceProvider Build()
        {
            var services = new ServiceCollection();
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("local.settings.json", optional: true, reloadOnChange: true)
                .Build();

            services.AddAzureClients(builder =>
                builder.AddTextAnalyticsClient(new Uri(configuration.GetSection("AzureTextAnalytics")["Endpoint"]), new AzureKeyCredential(configuration.GetSection("AzureTextAnalytics")["Key"]))
            );

            services.AddAzureClients(builder =>
            {
                builder.AddServiceBusClient(configuration.GetSection("ServiceBus")["ConnectionString"]);
            });

            services.AddSingleton<TableClient>(new TableClient(configuration.GetSection("AzureTablesService")["ConnectionString"], configuration.GetSection("AzureTablesService")["TablesName"]));


            services.AddTransient<ISentimentAnalysisService, SentimentAnalysisService>();
            services.AddTransient<ISentimentAnalysisHandler, SentimentAnalysisHandler>();

            services.AddSentimentQueue(configuration.GetSection("SentimentQueue")["ConnectionString"]);

            //services.AddTransient<IQueueMessageSerializer, JsonSerializer>();
            services.AddTransient<ITableService,TableService>();

            return services.BuildServiceProvider();

        }
    }
}