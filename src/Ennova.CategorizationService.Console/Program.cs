﻿using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace Ennova.CategorizationService.Console
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            // create service collection
            var services = new ServiceCollection();
            ConfigureServices(services);

            // create service provider
            var serviceProvider = services.BuildServiceProvider();

            // entry to run app
            await serviceProvider.GetService<StartApp>().Execute(args);

        }

        private static void ConfigureServices(IServiceCollection services)
        {
            services.RegisterServices();
        }
    }
}