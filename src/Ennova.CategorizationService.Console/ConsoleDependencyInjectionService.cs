﻿using System.IO;
using Ennova.CategorizationService.Queue;
using Ennova.CategorizationService.Queue.Message.Serializer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Ennova.CategorizationService.Console
{
    public static class ConsoleDependencyInjectionService
    {
        public static void RegisterServices(this IServiceCollection services)
        {
            services.AddSingleton<StartApp>();
            services.AddTextQueue("UseDevelopmentStorage=true");
            services.AddTransient<IQueueMessageSerializer, JsonSerializer>();
        }
    }
}